package routes;

/**
 * Created by Felix on 25.11.15.
 */
import backend.controller.BookingController;
import backend.service.DAOService;
import backend.service.FileService;
import backend.service.config.ConfigService;
import backend.service.mail.MailService;
import backend.service.routemapper.RouteMapper;
import backend.service.routemapper.RouteType;
import spark.servlet.SparkApplication;


import javax.servlet.http.HttpSession;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

import static spark.Spark.*;

public class MainRoute implements SparkApplication{

    private static boolean apache = false;

    public void init(){
        apache = true;
        ConfigService.runsInApplicationServer(apache);
        setupRoutes();
    }

    public static void main (String args[]){
        setupRoutes();
    }


    public static void setupRoutes(){

        Runnable l_expiredBookings = new BookingController().checkForExpiredBookings();
        Thread thread = new Thread(l_expiredBookings);
        thread.start();

        //Initialize Daos
        DAOService daoService = new DAOService();
        initStaticRoutes();
        initBeforeFilter();
        initBackendRoutes();
        initFrontendRoutes();

        initAfterFilter();
    }

    public static void initStaticRoutes(){

    }

    public static void initBeforeFilter(){
    before("/*",(req, res)->{
        req.session(true);
        if(req.session().isNew()) {
            int[] roles = {0}; // niederste Rolle, notwendig für Login
            req.session().attribute("Roles", roles);
        }
    });


    }





    public static void initFrontendRoutes(){

        get("/public/*", ((req, res) ->{



           String folder = req.pathInfo().split("/")[2];

            if(folder.equals("css")){
                String content = "";
                try {
                    content = new FileService().getResource(req.pathInfo());
                }catch(Exception e){
                    res.status(404);
                }
                res.header("Content-Type","text/css");
                return content;

            }else {
                byte[] rawData = new FileService().getRessourceRaw(req.pathInfo());
                res.header("Content-Type", "application");
                OutputStream stream = res.raw().getOutputStream();
                stream.write(rawData);
                stream.flush();
                return "";
            }




        }));


        get("/*", ((req, res) ->{





            String content = "";
            try {
                content = new FileService().getResource("/public/index.html");
            }catch(Exception e){
                res.status(404);
            }
            return content;


        }));




    }
    public static void initBackendRoutes(){


        get("/api/:controller/:action",(req,res) -> {
            RouteMapper mapper = new RouteMapper();
            String response_body = mapper.mapRoute(req,res,req.params("controller"),req.params("action"), RouteType.GET);
            return response_body;
        });

        get("/api/:controller/:action/:id",(req,res) -> {
            RouteMapper mapper = new RouteMapper();
            int id;
            try{
                id = Integer.valueOf(req.params("id"));
            }catch(Exception e){
                res.status(412);
                res.body( "/" + req.params("id") +" Has to be numeric ");
                return "";
            }


            String response_body = mapper.mapRouteWithId(req,res,req.params("controller"),req.params("action"),id,RouteType.GET);
            return response_body;
        });
        post("/api/:controller/:action",(req,res) -> {
            RouteMapper mapper = new RouteMapper();
            String response_body = mapper.mapRoute(req,res,req.params("controller"),req.params("action"), RouteType.POST);
            return response_body;
        });

        post("/api/:controller/:action/:id",(req,res) -> {
            RouteMapper mapper = new RouteMapper();
            int id;
            try{
                id = Integer.valueOf(req.params("id"));
            }catch(Exception e){
                res.status(412);
                res.body( "/" + req.params("id") +" Has to be numeric ");
                return "";
            }
            String response_body = mapper.mapRouteWithId(req,res,req.params("controller"),req.params("action"),id,RouteType.POST);
            return response_body;
        });


    }


    public static void initAfterFilter(){
        after((request, response) -> {



            if(apache) {
                response.cookie("/", "JSESSIONID", "" + request.session().id(), 999999999, false);
            }
            response.header("Content-Encoding", "gzip");
        });


    }

}
