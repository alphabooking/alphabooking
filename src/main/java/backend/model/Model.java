package backend.model;

import backend.service.DAOService;
import com.j256.ormlite.dao.Dao;

/**
 * Created by patrick on 30.11.15.
 */
public abstract class Model {
    public  Dao getDao(){
        //return DAO
        return new DAOService().createDAO(this.getClass());
    }

}
