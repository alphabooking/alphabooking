package backend.model.main;

/**
 * Created by patrick on 12.01.16.
 */
public class ReportModel {

    int id;
    String label;
    Object value;


    public ReportModel() { }

    public ReportModel(int id, String cla_roomName, Object value) {
        this.id = id;
        this.label = cla_roomName;
        this.value = value;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
