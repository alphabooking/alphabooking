package backend.model.main;

import backend.model.Model;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 03.12.15.
 */

@DatabaseTable(tableName = "Seating")
public class SeatingModel extends Model {

    private final static String CN_ID = "idSeating";
    private final static String CN_FORM = "form";
    private final static String CN_DESCRIPTION = "description";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_seatingId;

    @DatabaseField(columnName = CN_FORM)
    private String cla_name;

    @DatabaseField(columnName = CN_DESCRIPTION)
    private String cla_description;




    public SeatingModel() { }

    public SeatingModel(int cla_seatingId, String cla_name, String cla_description) {
        this.cla_seatingId = cla_seatingId;
        this.cla_name = cla_name;
        this.cla_description = cla_description;

    }

    public int getCla_seatingId() {
        return cla_seatingId;
    }

    public void setCla_seatingId(int cla_seatingId) {
        this.cla_seatingId = cla_seatingId;
    }

    public String getCla_name() {
        return cla_name;
    }

    public void setCla_name(String cla_name) {
        this.cla_name = cla_name;
    }

    public String getCla_description() {
        return cla_description;
    }

    public void setCla_description(String cla_description) {
        this.cla_description = cla_description;
    }


}
