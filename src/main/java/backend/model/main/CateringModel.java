package backend.model.main;

import backend.model.Model;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Felix on 07.01.16.
 */
@DatabaseTable(tableName = "Catering")
public class CateringModel extends Model {

    private static final String CN_ID = "idCatering";
    private static final String CN_DESCRIPTION = "description";


    @DatabaseField(generatedId = true, columnName = CN_ID)
    int id;
    @DatabaseField(columnName = CN_DESCRIPTION)
    String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
