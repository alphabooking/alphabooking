package backend.model.main;

import backend.model.Model;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Felix on 08.01.16.
 */
@DatabaseTable(tableName = "BookingStatus")
public class StatusModel extends Model {

    @DatabaseField(columnName = "idStatus" , id = true)
    private int id;

    @DatabaseField(columnName = "description" )
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
