package backend.model.main;

import backend.model.Model;
import backend.model.help.DepartmentModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Patrick on 25.11.15.
 */

@DatabaseTable(tableName = "User")
public class UserModel extends Model {

    private final static String CN_ID = "idUser";
    private final static String CN_FIRSTNAME = "firstname";
    private final static String CN_SURNAME = "surname";
    private final static String CN_GENDER = "gender";
    private final static String CN_MAIL = "mail";
    private final static String CN_PHONE = "phone";
    private final static String CN_PASSWORD = "password";
    private final static String CN_DEPARTMENT = "Department_idDepartment";


    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_userId;
    @DatabaseField(columnName = CN_PASSWORD)
    private String cla_password;

    @DatabaseField(columnName = CN_GENDER)
    private Gender cla_gender;
    @DatabaseField(columnName = CN_FIRSTNAME)
    private String cla_firstname;
    @DatabaseField(columnName = CN_SURNAME)
    private String cla_surname;

    @DatabaseField(columnName = CN_MAIL)
    private String cla_mail;
    @DatabaseField(columnName = CN_PHONE)
    private String cla_phone;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CN_DEPARTMENT)
    private DepartmentModel cla_department;






    public UserModel(){ }

    public UserModel(int cla_userId, String cla_password, String cla_firstname, String cla_surname, String cla_mail, String cla_phone, DepartmentModel cla_department, ForeignCollection<BookingModel> cla_bookingCollection, ForeignCollection<RoomModel> cla_roomCollection) {
        this.cla_userId = cla_userId;
        this.cla_password = cla_password;
        this.cla_firstname = cla_firstname;
        this.cla_surname = cla_surname;
        this.cla_mail = cla_mail;
        this.cla_phone = cla_phone;
        this.cla_department = cla_department;
    }

    private enum Gender {
        m,f
    }

    public int getCla_userId() {
        return cla_userId;
    }

    public void setCla_userId(int cla_userId) {
        this.cla_userId = cla_userId;
    }

    public String getCla_password() {
        return cla_password;
    }

    public void setCla_password(String cla_password) {
        this.cla_password = cla_password;
    }

    public String getCla_gender() {
        return cla_gender.toString();
    }

    public void setCla_gender(Gender cla_gender) {
        this.cla_gender = cla_gender;
    }

    public String getCla_firstname() {
        return cla_firstname;
    }

    public void setCla_firstname(String cla_firstname) {
        this.cla_firstname = cla_firstname;
    }

    public String getCla_surname() {
        return cla_surname;
    }

    public void setCla_surname(String cla_surname) {
        this.cla_surname = cla_surname;
    }

    public String getCla_mail() {
        return cla_mail;
    }

    public void setCla_mail(String cla_mail) {
        this.cla_mail = cla_mail;
    }

    public String getCla_phone() {
        return cla_phone;
    }

    public void setCla_phone(String cla_phone) {
        this.cla_phone = cla_phone;
    }

    public DepartmentModel getCla_department() {
        return cla_department;
    }

    public void setCla_department(DepartmentModel cla_department) {
        this.cla_department = cla_department;
    }



}
