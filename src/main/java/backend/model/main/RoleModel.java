package backend.model.main;

import backend.model.Model;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 03.12.15.
 */

@DatabaseTable(tableName = "Role")
public class RoleModel extends Model {

    private final static String CN_ID = "idRole";
    private final static String CN_NAME = "name";
    private final static String CN_DESCRIPTION = "description";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_roleId;

    @DatabaseField(columnName = CN_NAME)
    private String cla_name;

    @DatabaseField(columnName = CN_DESCRIPTION)
    private String cla_description;

    //@ForeignCollectionField
   // private ForeignCollection<UserModel> cla_user;


    public RoleModel() { }

    public RoleModel(int cla_roleId) {
        this.cla_roleId = cla_roleId;
    }


    public int getCla_roleId() {
        return cla_roleId;
    }

    public void setCla_roleId(int cla_roleId) {
        this.cla_roleId = cla_roleId;
    }

    public String getCla_name() {
        return cla_name;
    }

    public void setCla_name(String cla_name) {
        this.cla_name = cla_name;
    }

    public String getCla_description() {
        return cla_description;
    }

    public void setCla_description(String cla_description) {
        this.cla_description = cla_description;
    }

}
