package backend.model.main;

import backend.model.Model;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Booking")
public class BookingModel extends Model {

    public final static String CN_BOOINGID = "idBooking";
    public final static String CN_DESCRIPTION = "description";
    public final static String CN_BEGINDATE = "startTIme";
    public final static String CN_ENDDATE = "endTime";
    public final static String CN_EXTERN = "extern";
    public final static String CN_MEMBERCOUNT = "memberCount";
    public final static String CN_SEATING = "Seating_idSeating";
    public final static String CN_ROOM = "Room_idRoom";
    public final static String CN_USER = "User_idUser";
    public final static String CN_STATUS = "status";
    public final static String CN_CREATEDAT = "created_at";
    public final static String CN_APPROVEDDATE = "approvedDate";
    public final static String CN_SERVICE = "service";
    public final static String CN_COSTCENTER = "costCenter";
    public final static String CN_COMMENT = "comment";
    public final static String CN_BUFFERTIME = "bufferTime";

    @DatabaseField(generatedId = true, columnName = CN_BOOINGID)
    private int cla_bookingId;

    @DatabaseField(columnName = CN_DESCRIPTION)
    private String cla_description;

    @DatabaseField(columnName = CN_BEGINDATE, dataType = DataType.DATE_STRING)
    private Date cla_beginDate;


    @DatabaseField(columnName = CN_ENDDATE, dataType = DataType.DATE_STRING)
    private Date cla_endDate;


    @DatabaseField(columnName = CN_EXTERN)
    private Boolean cla_extern;

    @DatabaseField(columnName = CN_MEMBERCOUNT)
    private int cla_memberCount;

    @DatabaseField(columnName = CN_STATUS, foreign = true, foreignAutoRefresh = true)
    private StatusModel cla_status;

    @DatabaseField(columnName = CN_CREATEDAT)
    private Date cla_createdAt;

    @DatabaseField(columnName = CN_APPROVEDDATE)
    private Date cla_approvedDate;


    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CN_ROOM)
    private RoomModel cla_room;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CN_SEATING)
    private SeatingModel cla_seating;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CN_USER)
    private UserModel cla_user;

    @DatabaseField(foreign = false, columnName = CN_SERVICE)
    private boolean cla_service;

    @DatabaseField(columnName = CN_COMMENT)
    private String comment;

    @DatabaseField(columnName = CN_COSTCENTER)
    private int costCenter;
    @DatabaseField(columnName = CN_BUFFERTIME , defaultValue = "0")
    private int bufferTime;


    public BookingModel() { }

    public BookingModel(int cla_bookingId, String cla_description, Date cla_beginDate, Date cla_endDate, Boolean cla_extern, int cla_memberCount, StatusModel cla_status, Date cla_approvedDate, RoomModel cla_room, SeatingModel cla_seating, UserModel cla_user) {
        this.cla_bookingId = cla_bookingId;
        this.cla_description = cla_description;
        this.cla_beginDate = cla_beginDate;

        this.cla_endDate = cla_endDate;

        this.cla_extern = cla_extern;
        this.cla_memberCount = cla_memberCount;
        this.cla_status = cla_status;
        this.cla_approvedDate = cla_approvedDate;
        this.cla_room = cla_room;
        this.cla_seating = cla_seating;
        this.cla_user = cla_user;
    }


    public int getBufferTime() {
        return bufferTime;
    }

    public void setBufferTime(int bufferTime) {
        this.bufferTime = bufferTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(int costCenter) {
        this.costCenter = costCenter;
    }

    public boolean isCla_service() {
        return cla_service;
    }

    public Date getCla_createdAt() {
        return cla_createdAt;
    }

    public void setCla_createdAt(Date cla_createdAt) {
        this.cla_createdAt = cla_createdAt;
    }

    public int getCla_bookingId() {
        return cla_bookingId;
    }

    public void setCla_bookingId(int cla_bookingId) {
        this.cla_bookingId = cla_bookingId;
    }

    public String getCla_description() {
        return cla_description;
    }

    public void setCla_description(String cla_description) {
        this.cla_description = cla_description;
    }

    public Date getCla_beginDate() {
        return cla_beginDate;
    }

    public void setCla_beginDate(Date cla_beginDate) {
        this.cla_beginDate = cla_beginDate;
    }

    public Date getCla_endDate() {
        return cla_endDate;
    }

    public void setCla_endDate(Date cla_endDate) {
        this.cla_endDate = cla_endDate;
    }

    public Boolean getCla_extern() {
        return cla_extern;
    }

    public void setCla_extern(Boolean cla_extern) {
        this.cla_extern = cla_extern;
    }

    public int getCla_memberCount() {
        return cla_memberCount;
    }

    public void setCla_memberCount(int cla_memberCount) {
        this.cla_memberCount = cla_memberCount;
    }

    public RoomModel getCla_room() {
        return cla_room;
    }

    public void setCla_room(RoomModel cla_room) {
        this.cla_room = cla_room;
    }

    public SeatingModel getCla_seating() {
        return cla_seating;
    }

    public void setCla_seating(SeatingModel cla_seating) {
        this.cla_seating = cla_seating;
    }

    public UserModel getCla_user() {
        return cla_user;
    }

    public void setCla_user(UserModel cla_user) {
        this.cla_user = cla_user;
    }

    public StatusModel getCla_status() {
        return cla_status;
    }

    public void setCla_status(StatusModel cla_status) {
        this.cla_status = cla_status;
    }

    public Date getCla_approvedDate() {
        return cla_approvedDate;
    }

    public void setCla_approvedDate(Date cla_approvedDate) {
        this.cla_approvedDate = cla_approvedDate;
    }

    public boolean getCla_service(){
        return this.cla_service;
    }

    public void setCla_service(boolean p_service){
        this.cla_service = p_service;
    }



}