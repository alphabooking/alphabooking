package backend.model.main;

import backend.model.Model;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Felix on 25.11.15.
 */

@DatabaseTable(tableName = "Room")
public class RoomModel extends Model {

    public final static String CN_ID = "idRoom";
    public final static String CN_ROOMNAME = "roomName";
    public final static String CN_DESCRIPTION = "description";
    public final static String CN_CAPACITY = "capacity";
    public final static String CN_USER = "User_idUser";
    public final static String CN_ACTIVE = "active";


    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_roomId;

    @DatabaseField(columnName = CN_ROOMNAME)
    private String cla_name;
    @DatabaseField(columnName = CN_DESCRIPTION)
    private String cla_description;

    @DatabaseField(columnName = CN_CAPACITY)
    private int cla_capacity;

    @DatabaseField(columnName = CN_ACTIVE)
    private Boolean cla_active;


    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CN_USER)
    private UserModel cla_user;




    public RoomModel() { }

    public RoomModel(int cla_roomId, String cla_name, String cla_description, int cla_capacity, Boolean cla_active, UserModel cla_user) {
        this.cla_roomId = cla_roomId;
        this.cla_name = cla_name;
        this.cla_description = cla_description;
        this.cla_capacity = cla_capacity;
        this.cla_active = cla_active;
        this.cla_user = cla_user;
    }

    public int getCla_roomId() {
        return cla_roomId;
    }

    public void setCla_roomId(int cla_roomId) {
        this.cla_roomId = cla_roomId;
    }

    public String getCla_name() {
        return cla_name;
    }

    public void setCla_name(String cla_name) {
        this.cla_name = cla_name;
    }

    public String getCla_description() {
        return cla_description;
    }

    public void setCla_description(String cla_description) {
        this.cla_description = cla_description;
    }

    public int getCla_capacity() {
        return cla_capacity;
    }

    public void setCla_capacity(int cla_capacity) {
        this.cla_capacity = cla_capacity;
    }

    public UserModel getCla_user() {
        return cla_user;
    }

    public void setCla_user(UserModel cla_user) {
        this.cla_user = cla_user;
    }


    public Boolean getCla_active() {
        return cla_active;
    }

    public void setCla_active(Boolean cla_active) {
        this.cla_active = cla_active;
    }
}
