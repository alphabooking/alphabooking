package backend.model.help;

import backend.model.Model;
import backend.model.main.BookingModel;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by patrick on 07.12.15.
 */

@DatabaseTable(tableName = "BookingHasAdditionalEquipment")
public class BookingEquipmentModel extends Model {
    public final static String CN_ID = "idBookingHAE";
    public final static String CN_BOOKINGID = "Booking_idBooking";
    public final static String CN_AdditionalEquipmentID = "AdditionalEquipment_idAdditionalEquipment";
    public final static String CN_STARTTIME= "startTime";
    public final static String CN_ENDTIME = "endTime";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    int id ;

    @DatabaseField(foreign = true, columnName = CN_BOOKINGID)
    BookingModel cla_booking;

    @DatabaseField(foreign = true, columnName = CN_AdditionalEquipmentID)
    AdditionalEquipmentModel cla_additionalEquipment;

    @DatabaseField(columnName = CN_STARTTIME, dataType = DataType.DATE_STRING)
    Date cla_startTime;
    @DatabaseField(columnName = CN_ENDTIME, dataType = DataType.DATE_STRING)
    Date cla_endTime;



    public BookingEquipmentModel() { }

    public BookingEquipmentModel(BookingModel booking, AdditionalEquipmentModel additionalEquipment) {
        this.cla_booking = booking;
        this.cla_additionalEquipment = additionalEquipment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCla_startTime() {
        return cla_startTime;
    }

    public void setCla_startTime(Date cla_startTime) {
        this.cla_startTime = cla_startTime;
    }

    public Date getCla_endTime() {
        return cla_endTime;
    }

    public void setCla_endTime(Date cla_endTime) {
        this.cla_endTime = cla_endTime;
    }

    public BookingModel getCla_Booking() {
        return cla_booking;
    }

    public void setCla_Booking(BookingModel booking) {
        this.cla_booking = booking;
    }

    public AdditionalEquipmentModel getCla_AdditionalEquipment() {
        return cla_additionalEquipment;
    }

    public void setCla_AdditionalEquipment(AdditionalEquipmentModel additionalEquipment) {
        this.cla_additionalEquipment = additionalEquipment;
    }
}
