package backend.model.help;

import backend.model.Model;
import backend.model.main.BookingModel;
import backend.model.main.UserModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.sun.media.sound.ModelSource;

/**
 * Created by patrick on 06.01.16.
 */

@DatabaseTable(tableName = "BookingMembers")
public class BookingMemberModel extends Model{

    private final static String CN_BOOKINGMEMBERSID = "idBookingMembers";
    private final static String CN_BOOKINGID = "idBooking";
    private final static String CN_USERID = "idUser";


    @DatabaseField(generatedId = true, columnName = CN_BOOKINGMEMBERSID)
    private int cla_bookingMembersId;

    @DatabaseField(foreign = true, columnName = CN_BOOKINGID)
    private BookingModel cla_booking;

    @DatabaseField(foreign = true, columnName = CN_USERID)
    private UserModel cla_user;


    public BookingMemberModel() { }

    public BookingMemberModel(int cla_bookingMembersId, BookingModel cla_booking, UserModel cla_user) {
        this.cla_bookingMembersId = cla_bookingMembersId;
        this.cla_booking = cla_booking;
        this.cla_user = cla_user;
    }


    public int getCla_bookingMembersId() {
        return cla_bookingMembersId;
    }

    public void setCla_bookingMembersId(int cla_bookingMembersId) {
        this.cla_bookingMembersId = cla_bookingMembersId;
    }

    public BookingModel getCla_booking() {
        return cla_booking;
    }

    public void setCla_booking(BookingModel cla_booking) {
        this.cla_booking = cla_booking;
    }

    public UserModel getCla_user() {
        return cla_user;
    }

    public void setCla_user(UserModel cla_user) {
        this.cla_user = cla_user;
    }
}
