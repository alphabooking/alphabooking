package backend.model.help;

import backend.model.Model;
import backend.model.main.BookingModel;
import backend.model.main.UserModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 06.01.16.
 */

@DatabaseTable(tableName = "BookingExternalMembers")
public class BookingExternalMemberModel extends Model {


    private final static String CN_BOOKINGEXTMEMBERSID = "idBookingExternalMembers";
    private final static String CN_BOOKINGID = "idBooking";
    private final static String CN_MAIL= "email";


    @DatabaseField(generatedId = true, columnName = CN_BOOKINGEXTMEMBERSID)
    private int cla_bookingExtMemberId;

    @DatabaseField(foreign = true, columnName = CN_BOOKINGID)
    private BookingModel cla_booking;

    @DatabaseField(columnName = CN_MAIL)
    private String cla_mail;


    public BookingExternalMemberModel() { }
    public BookingExternalMemberModel(int cla_bookingExtMemberId, BookingModel cla_booking, String cla_mail) {
        this.cla_bookingExtMemberId = cla_bookingExtMemberId;
        this.cla_booking = cla_booking;
        this.cla_mail = cla_mail;
    }


    public int getCla_bookingExtMemberId() {
        return cla_bookingExtMemberId;
    }

    public void setCla_bookingExtMemberId(int cla_bookingExtMemberId) {
        this.cla_bookingExtMemberId = cla_bookingExtMemberId;
    }

    public BookingModel getCla_booking() {
        return cla_booking;
    }

    public void setCla_booking(BookingModel cla_booking) {
        this.cla_booking = cla_booking;
    }

    public String getCla_mail() {
        return cla_mail;
    }

    public void setCla_mail(String cla_mail) {
        this.cla_mail = cla_mail;
    }
}

