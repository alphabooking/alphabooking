package backend.model.help;

import backend.model.Model;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 04.12.15.
 */

@DatabaseTable(tableName = "AdditionalEquipment")
public class AdditionalEquipmentModel extends Model {

    private final static String CN_ID = "idAdditionalEquipment";
    private final static String CN_DESCRIPTION = "description";
    private final static String CN_Stock = "stock";
    public final static String CN_ACTIVE = "active";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_additionalEquipmentId;

    @DatabaseField(columnName = CN_DESCRIPTION)
    private String cla_description;

    @DatabaseField(columnName = CN_Stock)
    private int cla_stock;

    @DatabaseField(columnName = CN_ACTIVE)
    private Boolean cla_active;


    public AdditionalEquipmentModel() { }


    public String getCla_description() {
        return cla_description;
    }

    public void setCla_description(String cla_description) {
        this.cla_description = cla_description;
    }

    public int getCla_additionalEquipmentId() {
        return cla_additionalEquipmentId;
    }

    public void setCla_additionalEquipmentId(int cla_additionalEquipmentId) {
        this.cla_additionalEquipmentId = cla_additionalEquipmentId;
    }

    public int getCla_stock() {
        return cla_stock;
    }

    public void setCla_stock(int cla_stock) {
        this.cla_stock = cla_stock;
    }

    public Boolean getCla_active() {
        return cla_active;
    }

    public void setCla_active(Boolean cla_active) {
        this.cla_active = cla_active;
    }
}
