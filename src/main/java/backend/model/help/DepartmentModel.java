package backend.model.help;

import backend.model.Model;
import backend.model.main.UserModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 03.12.15.
 */

@DatabaseTable(tableName = "Department")
public class DepartmentModel extends Model {

    private final static String CN_ID = "idDepartment";
    private final static String CN_NAME = "departmentName";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_departmentId;

    @DatabaseField(columnName = CN_NAME)
    private String cla_name;




    public DepartmentModel() { }

    public DepartmentModel(int cla_departmentId) {
        this.cla_departmentId = cla_departmentId;
    }

    public int getCla_departmentId() {
        return cla_departmentId;
    }

    public void setCla_departmentId(int cla_departmentId) {
        this.cla_departmentId = cla_departmentId;
    }

    public String getCla_name() {
        return cla_name;
    }

    public void setCla_name(String cla_name) {
        this.cla_name = cla_name;
    }
}
