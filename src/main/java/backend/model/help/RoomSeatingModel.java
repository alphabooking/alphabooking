package backend.model.help;

import backend.model.Model;
import backend.model.main.RoomModel;
import backend.model.main.SeatingModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 07.12.15.
 */

@DatabaseTable(tableName = "RoomHasSeating")
public class RoomSeatingModel extends Model {

    public final static String CN_ID = "ID";
    public final static String CN_ROOMID = "Room_idRoom";
    public final static String CN_SEATINGID = "Seating_idSeating";


    @DatabaseField( generatedId = true , columnName = CN_ID)
    int id;

    @DatabaseField(foreign = true, columnName = CN_ROOMID)
    RoomModel cla_room;

    @DatabaseField(foreign = true, columnName = CN_SEATINGID)
    SeatingModel cla_seating;


    public RoomSeatingModel() { }

    public RoomSeatingModel(RoomModel room, SeatingModel seating) {
        this.cla_room = room;
        this.cla_seating = seating;
    }


    public RoomModel getCla_room() {
        return cla_room;
    }

    public void setCla_room(RoomModel cla_room) {
        this.cla_room = cla_room;
    }

    public SeatingModel getCla_seating() {
        return cla_seating;
    }

    public void setCla_seating(SeatingModel cla_seating) {
        this.cla_seating = cla_seating;
    }
}
