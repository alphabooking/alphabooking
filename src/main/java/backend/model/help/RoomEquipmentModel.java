package backend.model.help;

import backend.model.Model;
import backend.model.main.RoomModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 03.01.16.
 */


@DatabaseTable(tableName = "RoomHasFixEquipment")
public class RoomEquipmentModel extends Model{

    public final static String CN_ID = "id";
    public final static String CN_FIXEQUIPMENTID = "FixEquipment_idFixEquipment";
    public final static String CN_ROOMID = "Room_idRoom";


    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int id ;

    @DatabaseField(foreign = true, columnName = CN_FIXEQUIPMENTID)
    private FixEquipmentModel cla_fixEquipment;

    @DatabaseField(foreign = true, columnName = CN_ROOMID)
    private RoomModel cla_room;


    public RoomEquipmentModel() { }

    public RoomEquipmentModel(FixEquipmentModel cla_fixEquipment, RoomModel cla_room) {
        this.cla_fixEquipment = cla_fixEquipment;
        this.cla_room = cla_room;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FixEquipmentModel getCla_fixEquipment() {
        return cla_fixEquipment;
    }

    public void setCla_fixEquipment(FixEquipmentModel cla_fixEquipment) {
        this.cla_fixEquipment = cla_fixEquipment;
    }

    public RoomModel getCla_room() {
        return cla_room;
    }

    public void setCla_room(RoomModel cla_room) {
        this.cla_room = cla_room;
    }
}
