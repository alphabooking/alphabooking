package backend.model.help;

import backend.model.Model;
import backend.model.main.BookingModel;
import backend.model.main.StatusModel;
import backend.model.main.UserModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * Created by Felix on 08.01.16.
 */
@DatabaseTable(tableName = "BookingSeen")
public class BookingSeenModel extends Model {

    private static final String CN_ID = "idBookingSeen";
    private static final String CN_BOOKING = "idBooking";
    private static final String CN_OLD = "statusOld";
    private static final String CN_NEW = "statusNew";
    private static final String CN_SEEN = "seen";
    private static final String CN_USER = "idUser";




    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CN_BOOKING)
    private BookingModel booking;
    @DatabaseField(foreign = true, foreignAutoRefresh = true,columnName = CN_OLD)
    private StatusModel status_old;
    @DatabaseField(foreign = true, foreignAutoRefresh = true,columnName = CN_NEW)
    private StatusModel status_new;
    @DatabaseField(columnName = CN_SEEN)
    private boolean alreadyseen;

    @DatabaseField(foreign = true, foreignAutoRefresh = true,columnName = CN_USER)
    private UserModel user;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public boolean isAlreadyseen() {
        return alreadyseen;
    }

    public void setAlreadyseen(boolean alreadyseen) {
        this.alreadyseen = alreadyseen;
    }

    public BookingModel getBooking() {
        return booking;
    }

    public void setBooking(BookingModel booking) {
        this.booking = booking;
    }

    public StatusModel getStatus_old() {
        return status_old;
    }

    public void setStatus_old(StatusModel status_old) {
        this.status_old = status_old;
    }

    public StatusModel getStatus_new() {
        return status_new;
    }

    public void setStatus_new(StatusModel status_new) {
        this.status_new = status_new;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
