package backend.model.help;

import backend.model.Model;
import backend.model.main.BookingModel;
import backend.model.main.CateringModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Felix on 07.01.16.
 */
@DatabaseTable(tableName = "BookingHasCatering")
public class BookingHasCateringModel extends Model {

    private final static String CN_ID = "idBookingHasCatering";
    private final static String CN_BOOKING = "idBooking";
    private final static String CN_CATERING = "idCatering";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    int id;
    @DatabaseField(foreign = true, columnName = CN_BOOKING)
    BookingModel booking;
    @DatabaseField(foreign = true, columnName = CN_CATERING)
    CateringModel catering;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookingModel getBooking() {
        return booking;
    }

    public void setBooking(BookingModel booking) {
        this.booking = booking;
    }

    public CateringModel getCatering() {
        return catering;
    }

    public void setCatering(CateringModel catering) {
        this.catering = catering;
    }
}
