package backend.model.help;

import backend.model.Model;

import backend.model.main.RoleModel;
import backend.model.main.UserModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 18.12.15.
 */

@DatabaseTable(tableName = "UserHasRole")
public class UserRoleModel extends Model {

    private final static String CN_ROLEID = "Role_idRole";
    private final static String CN_USERID = "User_idUser";

    @DatabaseField(foreign = true, columnName = CN_ROLEID)
    RoleModel cla_role;

    @DatabaseField(foreign = true, columnName = CN_USERID)
    UserModel cla_user;


    public UserRoleModel() { }

    public UserRoleModel(RoleModel cla_role, UserModel cla_user) {
        this.cla_role = cla_role;
        this.cla_user = cla_user;
    }


    public RoleModel getCla_role() {
        return cla_role;
    }

    public void setCla_role(RoleModel cla_role) {
        this.cla_role = cla_role;
    }

    public UserModel getCla_user() {
        return cla_user;
    }

    public void setCla_user(UserModel cla_user) {
        this.cla_user = cla_user;
    }
}
