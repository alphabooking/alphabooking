package backend.model.help;

import backend.model.Model;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by patrick on 03.12.15.
 */

@DatabaseTable(tableName = "FixEquipment")
public class FixEquipmentModel extends Model {

    private final static String CN_ID = "idFixEquipment";
    private final static String CN_DESCRIPTION = "description";
    public final static String CN_ACTIVE = "active";

    @DatabaseField(generatedId = true, columnName = CN_ID)
    private int cla_idFixEquipment;

    @DatabaseField(columnName = CN_DESCRIPTION)
    private String cla_description;

    @DatabaseField(columnName = CN_ACTIVE)
    private Boolean cla_active;



    public FixEquipmentModel() { }


    public int getCla_idFixEquipment() {
        return cla_idFixEquipment;
    }

    public void setCla_idFixEquipment(int cla_idFixEquipment) {
        this.cla_idFixEquipment = cla_idFixEquipment;
    }

    public String getCla_description() {
        return cla_description;
    }

    public void setCla_description(String cla_description) {
        this.cla_description = cla_description;
    }

    public Boolean getCla_active() {
        return cla_active;
    }

    public void setCla_active(Boolean cla_active) {
        this.cla_active = cla_active;
    }
}
