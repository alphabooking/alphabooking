package backend.service;

/**
 * Created by Felix on 11.01.16.
 */
public class ValidateService {

    public boolean validateEmail(String p_email){
        boolean l_erg = true;

        if (p_email.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,63}")) {

            l_erg = true;
        } else {
            l_erg = false;
        }

        return l_erg;
    }


    public boolean validateCostcenter(String p_costcenter){
        return p_costcenter.matches("[1-9][0-9]{7}");
    }


    public boolean verifyFixedEquipmentGetParameter(String l_fixedEquipments){
        return l_fixedEquipments.matches("\\[(\\{id:[0-9]{1,}\\}\\,){0,}(\\{id:[0-9]{1,}\\})\\]");
    }

    public boolean verifyDate(String p_date) {
        boolean l_erg;

        if (p_date.matches("^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\\d\\d$")) {
            l_erg = true;
        } else {
            l_erg = false;
        }

        return l_erg;
    }

    public boolean verifyTime(String p_time) {
        boolean l_erg = true;

        if (p_time.matches("([0-2][0-9])(:)([0-5][0-9])")) {
            l_erg = true;
        } else {
            l_erg = false;
        }

        return l_erg;
    }

    public boolean canBeCastToNumeric(String[] p_array) {

        boolean l_erg = true;

        int[] l_array = new int[p_array.length];
        int i = 0;
        for (String str : p_array) {

            try {
                l_array[i] = Integer.parseInt(str);

            } catch (Exception ex) {
                l_erg = false;
            }


            i++;

        }

        return l_erg;
    }
    
}
