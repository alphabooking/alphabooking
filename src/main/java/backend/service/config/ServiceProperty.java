package backend.service.config;

/**
 * Created by Felix on 30.11.15.
 */
public class ServiceProperty {
    private String cla_id;
    private String cla_value;

    public ServiceProperty(String p_id, String p_value) {
        this.cla_id = p_id;
        this.cla_value = p_value;


    }

    public String getCla_id() {
        return cla_id;
    }

    public void setCla_id(String p_id) {
        this.cla_id = p_id;
    }

    public String getCla_value() {
        return cla_value;
    }

    public void setCla_value(String p_value) {
        this.cla_value = p_value;
    }
}
