package backend.service.config;

import java.net.URL;
import java.util.ArrayList;
import java.util.*;
import javax.xml.parsers.*;

import org.w3c.dom.Document;
import java.io.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

/**
 * Created by Felix on 30.11.15.
 */
import java.util.HashMap;
public class ConfigService {
private String path_standalone  = new String("private/Config.xml");
private String path_server = new String("/opt/config/Config.xml");
private String path_server_windows = new String("private/Config.xml");
    private Map<String, ArrayList<ServiceProperty>> cla_XML_Doc;
    private static ConfigService cla_service = null;
    private static boolean inApplicationServer = false;

    public static ConfigService getConfigService(){
        if(cla_service == null)
            cla_service = new ConfigService();

        return cla_service;

    }

    public static void runsInApplicationServer(boolean p_runsInApplicationServer){
        inApplicationServer = p_runsInApplicationServer;

    }

    private ConfigService(){
        try{
            cla_XML_Doc = new HashMap<String,ArrayList<ServiceProperty>>();
        init();}
        catch(Exception e){
            e.printStackTrace();
        }
    }


    private void init( ) throws Exception{
        File file;

        if(inApplicationServer) {
            String os = System.getProperty("os.name");

            if (os.toLowerCase().contains("windows"))
                file = new File(path_server_windows);
            else
                file = new File(path_server);
        }else{
            file = new File(path_standalone);
        }

        if(!file.exists()) {
            File dir = new File("private");
            dir.mkdir();
            file.createNewFile();
            File l_blueprint = new File(ConfigService.class.getResource("/private/Config.xml").toURI());
            BufferedReader read = new BufferedReader(new FileReader(l_blueprint));
            PrintWriter writer = new PrintWriter(file);

            String buffer;
            while((buffer=read.readLine())!=null){
                writer.write(buffer);
                writer.flush();
            }

        }

        DocumentBuilderFactory l_dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder l_dBuilder = l_dbFactory.newDocumentBuilder();
        Document l_doc = l_dBuilder.parse(file);

        NodeList l_nlist = l_doc.getElementsByTagName("service");

        for(int i = 0; i < l_nlist.getLength(); i++){
            Node l_node = l_nlist.item(i);
            NodeList l_snlist = l_node.getChildNodes();

            String l_key = l_node.getAttributes().getNamedItem("name").toString().replace("name=","").replace("\"","");

            ArrayList<ServiceProperty> l_list = new ArrayList<ServiceProperty>();
            cla_XML_Doc.put(l_key,l_list);
            for(int j = 0; j < l_snlist.getLength(); j++){
                Node sn = l_snlist.item(j);
                if(sn == null)
                    continue;
                if(sn.getNodeName().equals("#text"))
                    continue;

                l_list.add(new ServiceProperty(sn.getNodeName(),sn.getTextContent()));


        }





    }


}

    public ArrayList<ServiceProperty> getConfigForService(String p_serviceName){
        ArrayList<ServiceProperty> l_list = cla_XML_Doc.get(p_serviceName);
        return l_list;

    }

}
