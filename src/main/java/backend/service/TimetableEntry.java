package backend.service;

import backend.model.main.RoomModel;

import java.util.Date;

/**
 * Created by Felix on 12.01.16.
 */
public class TimetableEntry {
    private Date begin;
    private Date end;
    private int booking_id;
    private String booking_description;
    private RoomModel room;

    public TimetableEntry(Date begin, Date end, int booking_id, String booking_description, RoomModel room) {
        this.begin = begin;
        this.end = end;
        this.booking_id = booking_id;
        this.booking_description = booking_description;
        this.room = room;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_description() {
        return booking_description;
    }

    public void setBooking_description(String booking_description) {
        this.booking_description = booking_description;
    }

    public RoomModel getRoom() {
        return room;
    }

    public void setRoom(RoomModel room) {
        this.room = room;
    }
}
