package backend.service;

import spark.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Felix on 26.11.15.
 */
public class FileService {



    public FileService(){}
        public String getResource(String p_route) throws Exception{
                File l_resource = new File(FileService.class.getResource(p_route).toURI());
                String content = "";

                BufferedReader read = new BufferedReader(new FileReader(l_resource));
                String buffer;
                while((buffer = read.readLine())!=null) {
                    content = content + buffer;
                }
                 return content;

        }


        public byte[] getRessourceRaw(String p_route) throws Exception{
            byte [] bytes = Files.readAllBytes(Paths.get(FileService.class.getResource(p_route).toURI()));
            return bytes;
        }


    }

