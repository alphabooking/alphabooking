package backend.service.mail;

import backend.controller.help.BookingEquipmentController;
import backend.controller.help.BookingHasCateringController;
import backend.controller.help.RoomEquipmentController;
import backend.model.help.AdditionalEquipmentModel;
import backend.model.help.BookingHasCateringModel;
import backend.model.help.FixEquipmentModel;
import backend.model.main.BookingModel;
import backend.model.main.CateringModel;
import backend.model.main.UserModel;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by patrick on 29.12.15.
 */
public class TemplateService {

    private final String temp_path = "/private/templates/";

    private final String temp_header = "/private/templates/help/header.vm";
    private final String temp_footer = "/private/templates/help/footer.vm";

    private final String temp_OrganizerConfirmation = temp_path + "OrganizerConfirmation.vm";
    private final String temp_OrganizerModification = temp_path + "OrganizerModification.vm";
    private final String temp_OrganizerCancellation = temp_path + "OrganizerCancellation.vm";

    private final String temp_ExternalConfirmation = temp_path + "ExternalConfirmation.vm";
    private final String temp_ExternalModification = temp_path + "ExternalModification.vm";
    private final String temp_ExternalCancellation = temp_path + "ExternalCancellation.vm";

    private final String temp_CateringConfirmation = temp_path + "CateringConfirmation.vm";
    private final String temp_CateringModification = temp_path + "CateringModification.vm";
    private final String temp_CateringCancellation = temp_path + "CateringCancellation.vm";

    private final String temp_CaretakerConfirmation = temp_path + "CaretakerConfirmation.vm";
    private final String temp_CaretakerModification = temp_path + "CaretakerModification.vm";
    private final String temp_CaretakerCancellation = temp_path + "CaretakerCancellation.vm";

    private final SimpleDateFormat cla_dateformate = new SimpleDateFormat("dd.MM.yyyy");
    private final SimpleDateFormat cla_timeformate = new SimpleDateFormat("HH:mm");

    private static TemplateService cla_self = null;


    public static TemplateService getTemplateService(){
        if(cla_self == null){
            cla_self = new TemplateService();
        }
        return cla_self;
    }
    private TemplateService(){ }


    private Properties getVelocityProperties() {
        Properties l_properties = new Properties();
        l_properties.setProperty("resource.loader", "file");
        l_properties.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        return l_properties;
    }
    private Map getTemplateInformation() {

        Map l_templatemap = new HashMap();
        l_templatemap.put("header", temp_header);
        l_templatemap.put("footer", temp_footer);

        return l_templatemap;
    }

    private Map getBookingInformation(BookingModel p_bmodel) {

        Map l_bookingmap = new HashMap();

        switch (p_bmodel.getCla_user().getCla_gender()) {
            case "m":
                l_bookingmap.put("user_titelText", "Sehr geehrter");
                l_bookingmap.put("user_titel", "Herr");
                l_bookingmap.put("user_titel2", "Herrn");
                break;

            case "f":
                l_bookingmap.put("user_titelText", "Sehr geehrte");
                l_bookingmap.put("user_titel", "Frau");
                l_bookingmap.put("user_titel2", "Frau");
                break;
            default:
                break;
        }

        l_bookingmap.put("user_name", p_bmodel.getCla_user().getCla_surname());
        l_bookingmap.put("room_seating", p_bmodel.getCla_seating().getCla_name());
        l_bookingmap.put("room_name", p_bmodel.getCla_room().getCla_name());
        l_bookingmap.put("booking_nr", p_bmodel.getCla_bookingId());
        l_bookingmap.put("booking_description", p_bmodel.getCla_description());
        l_bookingmap.put("booking_begin", cla_dateformate.format(p_bmodel.getCla_beginDate()) +", " + cla_timeformate.format(p_bmodel.getCla_beginDate()) + " Uhr");
        l_bookingmap.put("booking_end", cla_dateformate.format(p_bmodel.getCla_endDate()) +", " + cla_timeformate.format(p_bmodel.getCla_endDate()) + " Uhr");
        l_bookingmap.put("booking_count", p_bmodel.getCla_memberCount());

        return l_bookingmap;
    }
    private ArrayList getEquipmentInformation(BookingModel p_bmodel) throws Exception {

        BookingEquipmentController l_becontroller = new BookingEquipmentController();
        ArrayList<AdditionalEquipmentModel> AdditionalEquipment = l_becontroller.getAdditionalEquipmentForBooking(p_bmodel);

        ArrayList l_equipmentList = new ArrayList();


        if (AdditionalEquipment.isEmpty()) {
            Map l_equipmentMap = new HashMap();
            l_equipmentMap.put("description", "Es wurde keine Zusatzausstattung gebucht.");
            l_equipmentList.add(l_equipmentMap);
        } else {
            for (AdditionalEquipmentModel temp : AdditionalEquipment) {
                Map l_equipmentMap = new HashMap();
                l_equipmentMap.put("description", temp.getCla_description());
                l_equipmentList.add( l_equipmentMap );
            }
        }

        return l_equipmentList;
    }
    private ArrayList getCateringInformation(BookingModel p_bmodel) throws Exception {

        BookingHasCateringController l_bccontroller = new BookingHasCateringController();
        ArrayList<CateringModel> CateringModels = l_bccontroller.getCateringForBooking(p_bmodel);

        ArrayList l_cateringList = new ArrayList();

        if (CateringModels.isEmpty()) {
            Map l_cateringMap = new HashMap();
            l_cateringMap.put("description", "Es wurde kein Bewirtung gebucht.");
            l_cateringList.add(l_cateringMap);
        } else {
            for (CateringModel temp : CateringModels) {
                Map l_cateringMap = new HashMap();
                l_cateringMap.put("description", temp.getDescription());
                l_cateringList.add(l_cateringMap);
            }
        }

        return l_cateringList;
    }


    private String createTemplate(BookingModel p_bmodel, String p_template) throws Exception{

        VelocityEngine l_ve = new VelocityEngine();
        l_ve.init(getVelocityProperties());

        VelocityContext l_context = new VelocityContext();

        Template l_temp = l_ve.getTemplate(p_template);
        StringWriter l_writer = new StringWriter();

        l_context.put("template", getTemplateInformation());
        l_context.put("booking", getBookingInformation(p_bmodel));
        l_context.put("equipmentList", getEquipmentInformation(p_bmodel));
        l_context.put("cateringList", getCateringInformation(p_bmodel));
        l_temp.merge( l_context, l_writer );

        return l_writer.toString();

    }

    public String getOrganizerConfirmation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_OrganizerConfirmation);
    }
    public String getExternalConfirmation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_ExternalConfirmation);
    }
    public String getCateringConfirmation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_CateringConfirmation);
    }
    public String getCaretakerConfirmation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_CaretakerConfirmation);
    }

    public String getOrganizerModification(BookingModel p_bmodel) throws Exception{

        return createTemplate(p_bmodel, temp_OrganizerModification);
    }
    public String getExternalModification(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_ExternalModification);
    }
    public String getCateringModification(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_CateringModification);
    }
    public String getCaretakerModification(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_CaretakerModification);
    }

    public String getOrganizerCancellation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_OrganizerCancellation);
    }
    public String getExternalCancellation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_ExternalCancellation);
    }
    public String getCateringCancellation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_CateringCancellation);
    }
    public String getCaretakerCancellation(BookingModel p_bmodel) throws Exception{
        return createTemplate(p_bmodel, temp_CaretakerCancellation);
    }


}
