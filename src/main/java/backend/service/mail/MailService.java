package backend.service.mail;


import backend.controller.help.BookingEquipmentController;
import backend.controller.help.BookingExternalMemberController;
import backend.model.help.AdditionalEquipmentModel;
import backend.model.help.BookingExternalMemberModel;
import backend.model.main.BookingModel;
import backend.service.DAOService;
import backend.service.config.ConfigService;
import backend.service.config.ServiceProperty;
import com.j256.ormlite.dao.Dao;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * Created by Felix on 27.11.15.
 */
public class MailService {

    private static MailService cla_self = null;
    private ConfigService cla_config = ConfigService.getConfigService();
    private ArrayList<ServiceProperty> cla_parameter;

    private String cla_cateringMail;
    private String cla_caretakerMail;

    public static MailService getMailService(){
        if(cla_self == null){
            cla_self = new MailService();
        }
        return cla_self;
    }

    private MailService(){
    }


    private Session getSession() {
        final Properties props = new Properties();

        cla_parameter = cla_config.getConfigForService("GMAIL");

        // Zum Empfangen
        props.setProperty( "mail.pop3.host", cla_parameter.get(0).getCla_value() );
        props.setProperty( "mail.pop3.user", cla_parameter.get(1).getCla_value() );
        props.setProperty( "mail.pop3.password", cla_parameter.get(2).getCla_value() );
        props.setProperty( "mail.pop3.port", cla_parameter.get(3).getCla_value() );
        props.setProperty( "mail.pop3.auth", cla_parameter.get(4).getCla_value() );
        props.setProperty( "mail.pop3.socketFactory.class", cla_parameter.get(5).getCla_value() );

        // Zum Senden
        props.setProperty( "mail.smtp.host", cla_parameter.get(6).getCla_value() );
        props.setProperty( "mail.smtp.port", cla_parameter.get(7).getCla_value() );
        props.setProperty( "mail.smtp.auth", cla_parameter.get(8).getCla_value());
        props.setProperty( "mail.smtp.socketFactory.port", cla_parameter.get(9).getCla_value() );
        props.setProperty( "mail.smtp.socketFactory.class", cla_parameter.get(10).getCla_value() );
        props.setProperty( "mail.smtp.socketFactory.fallback",cla_parameter.get(11).getCla_value() );

        return Session.getInstance(props, new javax.mail.Authenticator() {
            @Override protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication( props.getProperty( "mail.pop3.user" ),
                        props.getProperty( "mail.pop3.password" ) );
            }
        });}

    private void setServiceMails() {

        cla_parameter = cla_config.getConfigForService("SERVICEMAILS");
        cla_caretakerMail = cla_parameter.get(0).getCla_value();
        cla_cateringMail = cla_parameter.get(1).getCla_value();

    }

    public void sendHTMLMail( String p_recipient,String p_subject, String p_message){

        Runnable l_run = ()-> {
            Session l_session = this.getSession();


            MimeMessage l_msg = new MimeMessage(l_session);

            try {
                l_msg.setRecipients(Message.RecipientType.TO, p_recipient);
                l_msg.setSubject(p_subject);
                l_msg.setContent(p_message, "text/html; charset=utf-8");

                Transport.send(l_msg);
            }catch(Exception e){
                e.printStackTrace();
            }
        };

       Thread l_thread = new Thread(l_run);
        l_thread.start();
    }



    public void sendBookingConfirmation(BookingModel p_bm) throws Exception{

        TemplateService l_tempService = TemplateService.getTemplateService();
        this.sendHTMLMail(p_bm.getCla_user().getCla_mail(),"Buchungsbestätigung",l_tempService.getOrganizerConfirmation(p_bm));

        BookingExternalMemberController l_bemc = new BookingExternalMemberController();
        ArrayList<BookingExternalMemberModel> extMemberList = l_bemc.getExternalMemberforBooking(p_bm);

        for (BookingExternalMemberModel member : extMemberList) {
            this.sendHTMLMail(member.getCla_mail(),"Buchungseinladung",l_tempService.getExternalConfirmation(p_bm));
        }

        this.setServiceMails();

        if(p_bm.getCla_service()) {
            this.sendHTMLMail(cla_cateringMail,"Catering-Bestellung", l_tempService.getCateringConfirmation(p_bm));
        }

        BookingEquipmentController l_bec = new BookingEquipmentController();
        if(!l_bec.getAdditionalEquipmentForBooking(p_bm).isEmpty()) {
            this.sendHTMLMail(cla_caretakerMail, "Equipment-Anforderung", l_tempService.getCaretakerConfirmation(p_bm));
        }

    }
    public void sendBookingModification(BookingModel p_bm) throws Exception{
        TemplateService l_tempService = TemplateService.getTemplateService();
        this.sendHTMLMail(p_bm.getCla_user().getCla_mail(),"Buchungsänderung",l_tempService.getOrganizerModification(p_bm));

        BookingExternalMemberController l_bemc = new BookingExternalMemberController();
        ArrayList<BookingExternalMemberModel> extMemberList = l_bemc.getExternalMemberforBooking(p_bm);

        for (BookingExternalMemberModel member : extMemberList) {
            this.sendHTMLMail(member.getCla_mail(),"Buchungsänderung",l_tempService.getExternalModification(p_bm));
        }

        this.setServiceMails();

        if(p_bm.getCla_service()) {
            this.sendHTMLMail(cla_cateringMail,"Catering-Änderung", l_tempService.getCateringModification(p_bm));
        }

        BookingEquipmentController l_bec = new BookingEquipmentController();
        if(!l_bec.getAdditionalEquipmentForBooking(p_bm).isEmpty()) {
            this.sendHTMLMail(cla_caretakerMail, "Equipment-Änderung", l_tempService.getCaretakerModification(p_bm));
        }

    }
    public void sendBookingCancellation(BookingModel p_bm) throws Exception{
        TemplateService l_tempService = TemplateService.getTemplateService();
        this.sendHTMLMail(p_bm.getCla_user().getCla_mail(),"Buchungsabsage",l_tempService.getOrganizerCancellation(p_bm));

        BookingExternalMemberController l_bemc = new BookingExternalMemberController();
        ArrayList<BookingExternalMemberModel> extMemberList = l_bemc.getExternalMemberforBooking(p_bm);

        for (BookingExternalMemberModel member : extMemberList) {
            this.sendHTMLMail(member.getCla_mail(),"Buchungsabsage",l_tempService.getExternalCancellation(p_bm));
        }

        this.setServiceMails();

        if(p_bm.getCla_service()) {
            this.sendHTMLMail(cla_cateringMail,"Catering-Absage", l_tempService.getCateringCancellation(p_bm));
        }

        BookingEquipmentController l_bec = new BookingEquipmentController();
        if(!l_bec.getAdditionalEquipmentForBooking(p_bm).isEmpty()) {
            this.sendHTMLMail(cla_caretakerMail, "Equipment-Absage", l_tempService.getCaretakerCancellation(p_bm));
        }
    }

}
