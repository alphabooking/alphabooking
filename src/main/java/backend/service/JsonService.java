package backend.service;


import backend.service.message.MessageResponseService;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import spark.Request;
import spark.Response;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;

/**
 * Created by Felix on 21.12.15.
 */
public class JsonService {


    public JsonObject parseFromJson(Request p_request){
        String l_body = p_request.body();
        Gson gson = new Gson();

        JsonObject l_json = gson.fromJson(l_body, JsonObject.class);



        return l_json;
    }


    public Map<String, JsonElement> parseToMap(String json) throws Exception{
        Type type = new TypeToken<Map<String, JsonElement>>(){}.getType();
        Gson gson = new Gson();
        Map<String, JsonElement> myMap = gson.fromJson(json, type);

        return myMap;
    }


    public Map<String, JsonElement> parseToMap(Request p_req) throws Exception{
        String l_body = p_req.body();

        return parseToMap(l_body);
    }


    public String getValueFromJsonElement(JsonElement p_element){
       return new Gson().fromJson(p_element, String.class);
    }


    public String parseToJson(Object o){
        JsonSerializer<Date> ser = new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                    context) {
                return src == null ? null : new JsonPrimitive(src.getTime());
            }
        };
        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, ser).create();
        //niemals den PasswordHash zurückgeben
        return gson.toJson(o).replaceAll("(\"cla_password\":\".{64}\"[,]?)", "");
    }


    public Map<String, JsonElement> verifyIncomeingJsons(Request p_req, Response p_res , String[] p_fields){
        Map<String, JsonElement> l_map ;
        try {
            l_map = this.parseToMap(p_req);
        }catch(Exception e){
            MessageResponseService.makeMessage(p_res,412,"Der JSON wurde falsch formatiert");
            return null;

        }



        if(l_map.get("data") == null){

        }else{
            JsonElement l_tmp = l_map.get("data");
            if(l_tmp.isJsonObject()){
                JsonObject l_tmp2 = (JsonObject) l_tmp;
                try{
                   l_map= parseToMap(l_tmp2.toString());
                }catch(Exception e){
                    MessageResponseService.makeMessage(p_res,412,"Der JSON wurde falsch formatiert");
                    return null;
                }

            }
        }

        //check if all fields are there
        for(String l_key : p_fields){
            JsonElement l_temp = l_map.get(l_key);
            if(l_temp == null) {
                MessageResponseService.makeMessage(p_res,412,"Es fehlt das Feld: " + l_key + " in dem JSON");
                return null;
            }
        }
        return l_map;

    }
}
