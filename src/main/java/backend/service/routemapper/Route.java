package backend.service.routemapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Felix on 09.12.15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Route {
        RouteType type() default RouteType.GET; //Über welche HTTP_Method erreichbar
        int requiredRole() default 1;           //Über welche Rolle 0 frei
        boolean requiresLogin() default true;   //Ist ein login für den Aufruf erforderlich





}
