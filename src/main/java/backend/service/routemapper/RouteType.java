package backend.service.routemapper;

/**
 * Created by Felix on 09.12.15.
 */
public enum RouteType {
     GET{
         public String toString(){
             return "GET";
         }
     },
    POST{
        public String toString(){
            return "POST";
        }
    },
    PUT{
        public String toString(){
            return "PUT";
        }
    },
    DELETE{
        public String toString(){
            return "DELETE";
        }
    }


}
