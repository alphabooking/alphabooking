package backend.service.routemapper;

import spark.Request;
import spark.Response;
import spark.Session;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by Felix on 03.12.15.
 */





public class RouteMapper {


    // gibt HTTP-Status Code zurück
    public int isActionAllowed(Method r_method, Request p_req, Response p_res, RouteType p_type){
        Session session = p_req.session();
        int[] roles = session.attribute("Roles");

        String loginToken = session.attribute("X-Auth-Token");


        boolean isLoggedIn = false;

        if(loginToken!= null){
            if(p_req.headers("X-Auth-Token")!= null)
            if(p_req.headers("X-Auth-Token").equals(loginToken)){
                isLoggedIn = true;
            }
        }




        Annotation[] annotations = r_method.getDeclaredAnnotations();
        for(Annotation a : annotations){
            if(a.annotationType() == Route.class){
                Route r = (Route) a;

                RouteType l_type = r.type();
                int requiredRole = r.requiredRole();
                boolean requiresLogin = r.requiresLogin();



                // Wurde die richtige HTTP-Method gewählt
                if(l_type == p_type){
                    // Wenn er eingeloggt ist kann alles aufgerufen werden, wenn nicht nur offline
                    if((requiresLogin == isLoggedIn) || isLoggedIn){
                        boolean hasRequiredRole = false;

                        if(roles != null) {
                            for (int i : roles) {
                                if (requiredRole == i) {
                                    hasRequiredRole = true;
                                }
                            }
                        }

                        if(hasRequiredRole){
                            return 200;
                        }else{
                            return 403; //Mangelnde Berechtigung
                        }
                    }
                    else{
                        return 401; // Für die Methode muss der Anwender angemeldet sein
                    }
                }else{
                    return 405; // Falsche HTTP-Method
                }





            }
        }


        return 500;

    }





    private Class getClass(String p_controller) throws Exception{
        p_controller = p_controller.toLowerCase();
        String char1 = "" + p_controller.charAt(0);
        String char2 = "" + char1.toUpperCase();

        p_controller = p_controller.replaceFirst(char1,char2);


        p_controller = p_controller + "Controller";
        Class cla = Class.forName("backend.controller." + p_controller);

        return cla;
    }


    private String invokeMethod(Class p_cla, String p_action, Request p_req, Response p_res,int p_id, boolean p_withID, RouteType p_type)throws Exception{

        int l_parameterCount = 2;
        if(p_withID){
            l_parameterCount = 3;
        }

        p_action = p_action.toLowerCase();



        Method[] methods =p_cla.getMethods();

        for(Method m : methods){
            if(m.getName().equals(p_action)){
                if(m.getParameterCount() == l_parameterCount){


                    // Überprüfen ob die Methode für die aufgerufene Request-Method definiert wurde



                    int authorisedAction = this.isActionAllowed(m, p_req,p_res,p_type);




                    if(authorisedAction == 200) {
                        if (p_withID) {
                            return (String) m.invoke(p_cla.newInstance(), p_req, p_res, p_id);
                        } else {
                            return (String) m.invoke(p_cla.newInstance(), p_req, p_res);
                        }
                    }else{
                        p_res.status(authorisedAction);
                    }


                }


            }


        }
        return "";
    }

    public String mapRouteWithId(Request p_req, Response p_res, String p_controller, String p_action, int p_id, RouteType p_type){
        try {

            Class cla = this.getClass(p_controller);

            return this.invokeMethod(cla,p_action,p_req,p_res,p_id,true,p_type);



        }catch (Exception e){
            e.printStackTrace();
            p_res.status(404);
            return "";
        }



    }



    public String mapRoute(Request p_req, Response p_res, String p_controller, String p_action,RouteType p_type) {
        try {

                Class cla = this.getClass(p_controller);

                return this.invokeMethod(cla,p_action,p_req,p_res,-1,false,p_type);


        }catch (Exception e){
            e.printStackTrace();
            p_res.status(404);
            return "";
        }



    }

}
