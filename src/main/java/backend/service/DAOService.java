package backend.service;

import backend.service.config.ConfigService;
import backend.service.config.ServiceProperty;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.db.MysqlDatabaseType;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by patrick on 30.11.15.
 */

public class DAOService{

    private ConfigService cla_config = ConfigService.getConfigService();
    private ArrayList<ServiceProperty> cla_parameter;

    private String db_hostename;
    private String db_schema;
    private String db_port;
    private String db_user;
    private String db_password;

    private String db_path;

    private static JdbcPooledConnectionSource connectionSource;

    public DAOService() {
        setConnection();
    }

    private void setConnection() {
        try {
            cla_parameter = cla_config.getConfigForService("MYSQL");
            db_hostename = cla_parameter.get(0).getCla_value();
            db_schema = cla_parameter.get(1).getCla_value();
            db_port = cla_parameter.get(2).getCla_value();
            db_user = cla_parameter.get(3).getCla_value();
            db_password = cla_parameter.get(4).getCla_value();

            db_path = "jdbc:mysql://"+ db_hostename +":"+db_port+ "/" + db_schema;
            if(connectionSource == null) {
                connectionSource = new JdbcPooledConnectionSource(db_path, db_user, db_password);
                connectionSource.setMaxConnectionAgeMillis(1500000);
                connectionSource.initialize();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dao createDAO(Class p_modelClass) {
        try {
            Dao dao = DaoFactory.createDao(connectionSource , p_modelClass);
            //System.out.println("Offene Verbindungen an den DB-Server:"+ connectionSource.getOpenCount());
            return dao;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
