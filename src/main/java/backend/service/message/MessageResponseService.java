package backend.service.message;

import backend.service.JsonService;
import spark.Request;
import spark.Response;

/**
 * Created by Felix on 10.01.16.
 */
public class MessageResponseService {
    public static String makeMessage(Response p_res , int p_httpStatus, String p_message){
        Message message = new Message(p_message);

        p_res.status(p_httpStatus);
        JsonService l_jService = new JsonService();
        p_res.body(l_jService.parseToJson(message));

        return "";


    }
    private static class Message {

        public Message(String p_message){
            this.message = p_message;
        }
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
