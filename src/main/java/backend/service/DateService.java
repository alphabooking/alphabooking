package backend.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Felix on 12.01.16.
 */
public class DateService {
    private ValidateService c_vs = new ValidateService();

    public Calendar getCalendar(String p_date , String p_time){
        if(c_vs.verifyDate(p_date) && c_vs.verifyTime(p_time)){
            int [] l_date = this.parseStringArrayToInt(p_date.split("\\."));
            int [] l_time = this.parseStringArrayToInt(p_time.split(":"));

            int l_year = l_date[2];
            int l_month = this.mapMonthToCalender(l_date[1]);
            int l_day = l_date[0];
            int l_hour = l_time[0];
            int l_minute = l_time[1];

            Calendar l_calendar = Calendar.getInstance();
            l_calendar.set(l_year,l_month,l_day,l_hour,l_minute);


            return l_calendar;




        }else{
            return null;
        }

    }

    public Calendar getCalendar(String p_date){
        if(c_vs.verifyDate(p_date) ){
            int [] l_date = this.parseStringArrayToInt(p_date.split("\\."));


            int l_year = l_date[2];
            int l_month = this.mapMonthToCalender(l_date[1]);
            int l_day = l_date[0];
            int l_hour = 0;
            int l_minute = 0;


            Calendar l_calendar = Calendar.getInstance();
            l_calendar.set(l_year,l_month,l_day,l_hour,l_minute);



            return l_calendar;




        }else{
            return null;
        }

    }


    private int mapMonthToCalender(int p_month){
        switch (p_month){
            case 1: return Calendar.JANUARY;
            case 2: return Calendar.FEBRUARY;
            case 3: return Calendar.MARCH;
            case 4: return Calendar.APRIL;
            case 5: return Calendar.MAY;
            case 6: return Calendar.JUNE;
            case 7: return Calendar.JULY;
            case 8: return Calendar.AUGUST;
            case 9: return Calendar.SEPTEMBER;
            case 10: return Calendar.OCTOBER;
            case 11: return Calendar.NOVEMBER;
            case 12: return Calendar.DECEMBER;
            default: return 0;
        }


    }

    private int[] parseStringArrayToInt(String[] p_array){
        if(c_vs.canBeCastToNumeric(p_array)){
            int [] l_response = new int[p_array.length];
            for(int i = 0; i < l_response.length; i++){
                l_response[i] = Integer.valueOf(p_array[i]);
            }
            return  l_response;
        }
        return null;
    }
}
