package backend.controller.help;


import backend.model.help.BookingSeenModel;
import backend.model.main.BookingModel;
import backend.model.main.StatusModel;
import backend.model.main.UserModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 08.01.16.
 */
public class BookingSeenController {

    public void addNewEntry(BookingModel p_booking, StatusModel p_old, StatusModel p_new) throws Exception{
        BookingSeenModel l_model = new BookingSeenModel();

        Dao<BookingSeenModel, Integer> l_dao = l_model.getDao();

        l_model.setAlreadyseen(false);
        l_model.setBooking(p_booking);
        l_model.setStatus_new(p_new);
        l_model.setStatus_old(p_old);
        l_model.setUser(p_booking.getCla_user());

        l_dao.create(l_model);

    }

    public void markasseen(UserModel p_user) throws Exception{
        Dao<BookingSeenModel, Integer> l_dao = new BookingSeenModel().getDao();
        List<BookingSeenModel> l_list = this.getNewsForUser(p_user);
        if(l_list != null)

        for(BookingSeenModel l_model : l_list) {
            if (l_model != null) {
                l_dao.refresh(l_model);
                l_model.setAlreadyseen(true);
                l_dao.update(l_model);
            }
        }


    }

    public List<BookingSeenModel> getNewsForUser(UserModel p_user){
        Map<String, Object> l_map = new HashMap<String, Object>();
        l_map.put("idUser",p_user);
        l_map.put("seen", false);

        Dao<BookingSeenModel, Integer> l_dao = new BookingSeenModel().getDao();

        try {
            List<BookingSeenModel> l_list = l_dao.queryForFieldValues(l_map);
            for(BookingSeenModel l_model : l_list){
                l_dao.refresh(l_model);
            }

            return l_list;


        }catch(Exception e){
            e.printStackTrace();
            return null;
        }




    }





}
