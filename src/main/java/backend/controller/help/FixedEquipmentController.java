package backend.controller.help;

import backend.model.help.FixEquipmentModel;
import backend.model.main.BookingModel;
import backend.model.main.StatusModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by Felix on 16.01.16.
 */
public class FixedEquipmentController {
    public List<FixEquipmentModel> getALL(){
        Dao<FixEquipmentModel, Integer> l_dao = new FixEquipmentModel().getDao();
        try {
            List<FixEquipmentModel> l_list = l_dao.queryForAll();
            for (FixEquipmentModel l_feM : l_list)
                l_dao.refresh(l_feM);

            return l_list;
        }catch (Exception e) {
            return null;
        }


    }

}
