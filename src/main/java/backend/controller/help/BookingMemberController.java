package backend.controller.help;

import backend.model.help.BookingMemberModel;
import backend.model.main.BookingModel;
import backend.model.main.UserModel;
import com.j256.ormlite.dao.Dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by patrick on 06.01.16.
 */
public class BookingMemberController {

    public BookingMemberController() { }

    public ArrayList<UserModel> getMemberforBooking(BookingModel p_bmodel) throws Exception{
        BookingMemberModel model = new BookingMemberModel();
        Dao<BookingMemberModel,Integer> l_dao = model.getDao();
        Dao<UserModel, Integer> l_daoU = new UserModel().getDao();

        ArrayList<UserModel> UserList = new ArrayList<UserModel>();

        model.setCla_booking(p_bmodel);

        Map<String, Object> map = new HashMap<String,Object>();

        map.put("idBooking",p_bmodel);


        List<BookingMemberModel> l_list =  l_dao.queryForFieldValues(map);


        for(BookingMemberModel l_bmm : l_list) {


            UserModel l_tempUserModel = l_bmm.getCla_user();
            l_daoU.refresh(l_tempUserModel);

            UserList.add(l_tempUserModel);

        }

        return UserList;

    }


}
