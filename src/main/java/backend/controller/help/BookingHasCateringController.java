package backend.controller.help;

import backend.controller.CateringController;
import backend.model.help.BookingEquipmentModel;
import backend.model.help.BookingExternalMemberModel;
import backend.model.help.BookingHasCateringModel;
import backend.model.main.BookingModel;
import backend.model.main.CateringModel;
import backend.service.JsonService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import jdk.nashorn.internal.ir.debug.JSONWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 07.01.16.
 */
public class BookingHasCateringController {

    public void rollback(BookingModel p_booking){
        Dao<BookingHasCateringModel, Integer> l_dao = new BookingHasCateringModel().getDao();
        Map<String, Object> l_query = new HashMap<String, Object>();
        l_query.put("idBooking",p_booking);
        try {
            List<BookingHasCateringModel> l_result = l_dao.queryForFieldValues(l_query);
            l_dao.delete(l_result);



        }catch(Exception e){
            e.printStackTrace();
        }


    }

    private ArrayList<CateringModel> verify(JsonElement p_catering){
        List<CateringModel> l_list = new CateringController().getAll();
        JsonService l_jsonService = new JsonService();
        ArrayList<CateringModel> l_result = new ArrayList<CateringModel>();

        if(p_catering.isJsonObject()){
            JsonObject l_catering = (JsonObject) p_catering;

            try {
                Map<String, JsonElement> l_map = l_jsonService.parseToMap(l_catering.toString());

                for(Map.Entry<String, JsonElement> l_entry : l_map.entrySet()){
                   int l_id = Integer.valueOf(l_jsonService.getValueFromJsonElement(l_entry.getValue()));


                    for(CateringModel l_model : l_list){
                        if(l_model.getId() == l_id)
                            l_result.add(l_model);
                    }

                }

                return l_result;


            }catch(Exception e){
                return null;
            }


        }
        else{
            return null;
        }


    }

    public boolean addCatering(BookingModel p_booking, JsonElement p_catering){

        ArrayList<CateringModel> l_possibleCatering = this.verify(p_catering);

        if(l_possibleCatering == null)
            return false;

        try{
            Dao<BookingHasCateringModel, Integer> l_dao = new BookingHasCateringModel().getDao();
            for(CateringModel l_catering : l_possibleCatering){
                BookingHasCateringModel l_newModel = new BookingHasCateringModel();
                l_newModel.setBooking(p_booking);
                l_newModel.setCatering(l_catering);
                l_dao.create(l_newModel);

            }

            return true;

        }catch (Exception e){
            return false;
        }



    }


    public ArrayList<CateringModel> getCateringForBooking(BookingModel p_bmodel) throws Exception {
        BookingHasCateringModel model = new BookingHasCateringModel();
        Dao<BookingHasCateringModel, Integer> l_dao = model.getDao();
        Dao<CateringModel, Integer> l_daoAE = new CateringModel().getDao();

        ArrayList<CateringModel> Catering = new ArrayList<CateringModel>();

        model.setBooking(p_bmodel);

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("idBooking", p_bmodel);


        List<BookingHasCateringModel> l_list = l_dao.queryForFieldValues(map);


        for (BookingHasCateringModel l_bhscm : l_list) {


            CateringModel l_tempCatering = l_bhscm.getCatering();
            l_daoAE.refresh(l_tempCatering);

            Catering.add(l_tempCatering);

        }

        return Catering;

    }


}
