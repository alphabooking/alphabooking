package backend.controller.help;

import backend.model.help.UserRoleModel;
import backend.model.main.RoleModel;
import backend.model.main.UserModel;
import com.j256.ormlite.dao.Dao;
import spark.Request;
import spark.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 22.12.15.
 */
public class UserRoleController {



    public ArrayList<RoleModel> getRolesForUser(UserModel p_umodel) throws Exception{
        UserRoleModel model = new UserRoleModel();
        Dao<UserRoleModel,Integer> l_dao = model.getDao();
        Dao<UserModel, Integer> l_daoU = new UserModel().getDao();
        Dao<RoleModel, Integer> l_daoR = new RoleModel().getDao();

        ArrayList<RoleModel> roles = new ArrayList<RoleModel>();

        model.setCla_user(p_umodel);

        Map<String, Object> map = new HashMap<String,Object>();

        map.put("User_idUser",p_umodel);


       List<UserRoleModel> l_list =  l_dao.queryForFieldValues(map);


        for(UserRoleModel l_urm : l_list) {


            RoleModel l_tempRole = l_urm.getCla_role();
            l_daoR.refresh(l_tempRole);

            roles.add(l_tempRole);

        }

        return roles;

    }


    public void copyRolesToSession(Request p_request, UserModel p_user) throws Exception{

        ArrayList<RoleModel> l_roles = this.getRolesForUser(p_user);
        Session session = p_request.session();

        int[] l_rolesToAppend = new int[l_roles.size()+1];

        int i = 0;
        for(RoleModel model : l_roles){

            l_rolesToAppend[i] = model.getCla_roleId();
            i++;
        }

        l_rolesToAppend[i] = 0;

        session.attribute("Roles",l_rolesToAppend);



    }




}
