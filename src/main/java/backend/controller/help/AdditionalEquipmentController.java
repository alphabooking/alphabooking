package backend.controller.help;

import backend.model.help.AdditionalEquipmentModel;
import backend.model.main.StatusModel;
import backend.service.JsonService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 07.01.16.
 */
public class AdditionalEquipmentController {

    public AdditionalEquipmentModel getEquipmentByID(int id) throws Exception{
        Dao<AdditionalEquipmentModel, Integer> l_dao = new AdditionalEquipmentModel().getDao();

        AdditionalEquipmentModel l_model = l_dao.queryForId(id);

        return  l_model;

    }


    public List<AdditionalEquipmentModel> getALL(){
        Dao<AdditionalEquipmentModel, Integer> l_dao = new AdditionalEquipmentModel().getDao();
        try {
            List<AdditionalEquipmentModel> l_list = l_dao.queryForAll();
            for (AdditionalEquipmentModel l_aeM : l_list)
                l_dao.refresh(l_aeM);

            return l_list;
        }catch (Exception e) {
            return null;
        }


    }



    public ArrayList<AdditionalEquipmentModel> getAdditionalEquipments(JsonElement additionalEquipment) throws Exception{
        JsonService l_jservice = new JsonService();
        JsonObject l_equipments;
        if(additionalEquipment.isJsonObject()){
            l_equipments = (JsonObject) additionalEquipment;
        }else{
            return null;
        }



        Map<String, JsonElement> l_map = l_jservice.parseToMap(l_equipments.toString());
        Dao<AdditionalEquipmentModel, Integer> l_dao = new AdditionalEquipmentModel().getDao();
        ArrayList<AdditionalEquipmentModel> l_results = new ArrayList<AdditionalEquipmentModel>();

        for(Map.Entry<String, JsonElement> l_entry : l_map.entrySet()){
            int l_id = Integer.valueOf(l_jservice.getValueFromJsonElement(l_entry.getValue()));
            AdditionalEquipmentModel l_temp = l_dao.queryForId(l_id);

            l_dao.refresh(l_temp);
            l_results.add(l_temp);
            if(l_temp == null)
                return null;
        }

        return l_results;


    }
}
