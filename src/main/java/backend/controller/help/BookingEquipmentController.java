package backend.controller.help;

import backend.controller.StatusController;
import backend.model.help.AdditionalEquipmentModel;
import backend.model.help.BookingEquipmentModel;
import backend.model.help.BookingHasCateringModel;
import backend.model.main.BookingModel;

import backend.service.message.MessageResponseService;
import com.google.gson.JsonElement;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import spark.Response;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by patrick on 30.12.15.
 */
public class BookingEquipmentController {

    public BookingEquipmentController() {
    }

    public boolean canBeBooked(int[] p_ids, Date p_begin, Date p_end) throws Exception{
        boolean p_return = true;

        Dao<BookingModel, Integer> l_daoB = new BookingModel().getDao();

        QueryBuilder<BookingModel, Object> l_queryBooking = new BookingModel().getDao().queryBuilder();
        Where<BookingModel, Object> l_whereBooking = l_queryBooking.where();

        //Alle Buchungen die Angenommen, oder Geändert sind
        StatusController l_sc = new StatusController();
        l_whereBooking. eq(BookingModel.CN_STATUS, l_sc.getById(2))
                        .or()
                        .eq(BookingModel.CN_STATUS,l_sc.getById(4));

        l_queryBooking.setWhere(l_whereBooking);
        List<BookingModel> l_allHeldBookings = l_daoB.query(l_queryBooking.prepare());

        for(int l_id : p_ids){

            QueryBuilder<BookingEquipmentModel, Object> l_queryBuilder =  new BookingEquipmentModel().getDao().queryBuilder();
            AdditionalEquipmentModel l_AdditionalEquipment = new AdditionalEquipmentController().getEquipmentByID(l_id);
            if(l_AdditionalEquipment == null)
                continue;


            Where<BookingEquipmentModel, Object> l_where = l_queryBuilder.where();


            l_where.or(
                        l_where.between(BookingEquipmentModel.CN_STARTTIME,p_begin,p_end),
                        l_where.between(BookingEquipmentModel.CN_ENDTIME,p_begin,p_end),
                        l_where.and(l_where.ge(BookingEquipmentModel.CN_STARTTIME,p_begin),
                                l_where.le(BookingEquipmentModel.CN_ENDTIME, p_end)))
                    .and()
                    .in(BookingEquipmentModel.CN_BOOKINGID, l_allHeldBookings)
                    .and()
                    .eq(BookingEquipmentModel.CN_AdditionalEquipmentID,l_AdditionalEquipment);

            l_queryBuilder.setWhere(l_where);
            System.out.println(l_queryBuilder.prepareStatementString());

            Dao<BookingEquipmentModel,Integer> ldao = new BookingEquipmentModel().getDao();

           List<BookingEquipmentModel> l_list = ldao.query(l_queryBuilder.prepare());

            if(l_list != null){
                AdditionalEquipmentModel l_tmpModel = new AdditionalEquipmentController().getEquipmentByID(l_id);
               System.out.println("Size:" + l_list.size());
                if(l_list.size()>=l_tmpModel.getCla_stock()){
                    return false;
                }
            }
        }
        return  true;

    }


    public int getMinStock(int p_id, Date p_begin) throws Exception{

        Dao<BookingModel, Object> l_daoB = new BookingModel().getDao();
        Dao<BookingEquipmentModel,Object> l_daoE = new BookingEquipmentModel().getDao();


        QueryBuilder<BookingEquipmentModel, Object> l_queryEquipment = l_daoE.queryBuilder();
        Where<BookingEquipmentModel, Object> l_whereEquipment = l_queryEquipment.where();
        l_whereEquipment.eq(BookingEquipmentModel.CN_AdditionalEquipmentID, 4);

        l_queryEquipment.setWhere(l_whereEquipment);
        //System.out.println(l_queryEquipment.prepareStatementString());

        List<BookingEquipmentModel> l_allEqList= l_daoE.query(l_queryEquipment.prepare());
        List<Integer> l_helpBidList = new ArrayList<>();

        for(BookingEquipmentModel l_tmp : l_allEqList) {
            l_helpBidList.add(l_tmp.getCla_Booking().getCla_bookingId());
        }



        QueryBuilder<BookingModel, Object> l_queryBooking = new BookingModel().getDao().queryBuilder();
        Where<BookingModel, Object> l_whereBooking = l_queryBooking.where();

        l_whereBooking.ge(BookingModel.CN_BEGINDATE,p_begin)
                .and()
                .in(BookingModel.CN_BOOINGID, l_helpBidList);

        l_queryBooking.setWhere(l_whereBooking);
        //System.out.println(l_queryBooking.prepareStatementString());
        List<BookingModel> l_allBList= l_daoB.query(l_queryBooking.prepare());

        int counter = 0;


        for (BookingModel l_tmp1 : l_allBList) {
            int temp_Counter = 1;

            for (BookingModel l_tmp2 : l_allBList) {

                if(l_tmp2.getCla_beginDate().after(l_tmp1.getCla_beginDate()) && l_tmp2.getCla_beginDate().before(l_tmp1.getCla_endDate())
                        || l_tmp2.getCla_endDate().after(l_tmp1.getCla_beginDate()) && l_tmp2.getCla_endDate().before(l_tmp1.getCla_endDate())
                        || l_tmp2.getCla_beginDate().before(l_tmp1.getCla_beginDate()) && l_tmp2.getCla_endDate().after(l_tmp1.getCla_endDate())) {
                    temp_Counter++;
                }

            }

            if (temp_Counter> counter) {
                counter = temp_Counter;
            }

        }


        return counter;
    }

    public void rollback(BookingModel p_booking){
        Dao<BookingEquipmentModel, Integer> l_dao = new BookingEquipmentModel().getDao();
        Map<String, Object> l_query = new HashMap<String, Object>();
        l_query.put("Booking_idBooking",p_booking);
        try {
            List<BookingEquipmentModel> l_result = l_dao.queryForFieldValues(l_query);
            l_dao.delete(l_result);



        }catch(Exception e){
            e.printStackTrace();
        }


    }

    public ArrayList<AdditionalEquipmentModel> getAdditionalEquipmentForBooking(BookingModel p_bmodel) throws Exception {
        BookingEquipmentModel model = new BookingEquipmentModel();
        Dao<BookingEquipmentModel, Integer> l_dao = model.getDao();
        Dao<AdditionalEquipmentModel, Integer> l_daoAE = new AdditionalEquipmentModel().getDao();

        ArrayList<AdditionalEquipmentModel> AdditionalEquipment = new ArrayList<AdditionalEquipmentModel>();

        model.setCla_Booking(p_bmodel);

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("Booking_idBooking", p_bmodel);


        List<BookingEquipmentModel> l_list = l_dao.queryForFieldValues(map);


        for (BookingEquipmentModel l_bem : l_list) {


            AdditionalEquipmentModel l_tempAdditionalEquipment = l_bem.getCla_AdditionalEquipment();
            l_daoAE.refresh(l_tempAdditionalEquipment);

            AdditionalEquipment.add(l_tempAdditionalEquipment);

        }

        return AdditionalEquipment;

    }
    public boolean addAdditionalEquipment(BookingModel p_booking , JsonElement p_additionalEquipments, Response p_res){
        AdditionalEquipmentController l_aec = new AdditionalEquipmentController();
        Dao<BookingEquipmentModel, Integer> l_dao = new BookingEquipmentModel().getDao();

        try {
            ArrayList<AdditionalEquipmentModel> l_list = l_aec.getAdditionalEquipments(p_additionalEquipments);

            if(l_list == null) {
                MessageResponseService.makeMessage(p_res,412,"Überprüfen Sie die Formatierung der extra Auststattungs Ids");
                return false;
            }


            int[] l_ids = new int[l_list.size()];

            int l_iterator = 0;
            for(AdditionalEquipmentModel l_convert : l_list){

                l_ids[l_iterator] = l_convert.getCla_additionalEquipmentId();
                l_iterator++;

            }

            if(!this.canBeBooked(l_ids,p_booking.getCla_beginDate(),p_booking.getCla_endDate())){
                MessageResponseService.makeMessage(p_res,412,"Eine ihrer selektierten extra Ausstattungen ist nicht Verfügbar");
                return false;
            }




            for(AdditionalEquipmentModel l_ae : l_list){
                BookingEquipmentModel l_temp = new BookingEquipmentModel();
                l_temp.setCla_Booking(p_booking);
                l_temp.setCla_AdditionalEquipment(l_ae);

                l_dao.create(l_temp);
            }

            return  true;

        }catch (Exception e){
            e.printStackTrace();
            MessageResponseService.makeMessage(p_res,500,"Bei der Bearbeitung Ihrer Anfrage ist ein Fehler aufgetreten");
            return false;
        }

    }


}
