package backend.controller.help;

import backend.model.help.FixEquipmentModel;
import backend.model.help.RoomEquipmentModel;
import backend.model.help.RoomSeatingModel;
import backend.model.main.RoomModel;
import backend.service.JsonService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by patrick on 03.01.16.
 */
public class RoomEquipmentController {

    public RoomEquipmentController() {
    }

    public void deleteEntrys(RoomModel p_room) throws Exception{
        Map<String, Object> l_query = new HashMap<>();
        l_query.put(RoomEquipmentModel.CN_ROOMID,p_room);

        Dao<RoomEquipmentModel,Integer> l_dao = new RoomEquipmentModel().getDao();
        List<RoomEquipmentModel> l_list = l_dao.queryForFieldValues(l_query);
        l_dao.delete(l_list);
    }


    public void createEntrys(RoomModel p_room ,JsonObject p_fixedEquipemnts) throws Exception{
        JsonService l_jService = new JsonService();
        Dao<FixEquipmentModel, Integer> l_dao = new FixEquipmentModel().getDao();
        Dao<RoomEquipmentModel, Integer> l_daoRE = new RoomEquipmentModel().getDao();
            Map<String , JsonElement> l_map = l_jService.parseToMap(p_fixedEquipemnts.toString());

            for(Map.Entry<String, JsonElement> l_entry : l_map.entrySet()){
                int l_id = Integer.valueOf(l_jService.getValueFromJsonElement(l_entry.getValue()));
                FixEquipmentModel l_equi = l_dao.queryForId(l_id);
                l_dao.refresh(l_equi);

                RoomEquipmentModel l_nEntry = new RoomEquipmentModel();
                l_nEntry.setCla_room(p_room);
                l_nEntry.setCla_fixEquipment(l_equi);
                l_daoRE.create(l_nEntry);

            }

    }


    public boolean verify(JsonObject p_fixedEquipemnts){
        JsonService l_jService = new JsonService();
        Dao<FixEquipmentModel, Integer> l_dao = new FixEquipmentModel().getDao();
        try {
            Map<String , JsonElement> l_map = l_jService.parseToMap(p_fixedEquipemnts.toString());

            for(Map.Entry<String, JsonElement> l_entry : l_map.entrySet()){
                try{
                    int l_id = Integer.valueOf(l_jService.getValueFromJsonElement(l_entry.getValue()));
                    FixEquipmentModel l_equi = l_dao.queryForId(l_id);
                    if(l_equi== null)
                        return false;

                }catch (Exception e){
                    return false;
                }


            }

        }catch (Exception e){
            return false;
        }

        return true;
    }

    public ArrayList<FixEquipmentModel> getFixEquipmentForRoom(RoomModel p_rmodel) throws Exception{
        RoomEquipmentModel model = new RoomEquipmentModel();
        Dao<RoomEquipmentModel,Integer> l_dao = model.getDao();
        Dao<FixEquipmentModel, Integer> l_daoE = new FixEquipmentModel().getDao();

        ArrayList<FixEquipmentModel> FixEquipment = new ArrayList<FixEquipmentModel>();

        model.setCla_room(p_rmodel);

        Map<String, Object> map = new HashMap<String,Object>();

        map.put("Room_idRoom",p_rmodel);


        List<RoomEquipmentModel> l_list =  l_dao.queryForFieldValues(map);


        for(RoomEquipmentModel l_rem : l_list) {


            FixEquipmentModel l_tempAdditionalEquipment = l_rem.getCla_fixEquipment();
            l_daoE.refresh(l_tempAdditionalEquipment);

            FixEquipment.add(l_tempAdditionalEquipment);

        }

        return FixEquipment;

    }
}
