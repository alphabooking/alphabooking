package backend.controller.help;

import backend.model.help.FixEquipmentModel;
import backend.model.help.RoomSeatingModel;
import backend.model.main.RoomModel;
import backend.model.main.SeatingModel;
import backend.service.JsonService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 07.01.16.
 */
public class RoomSeatingController {



    public void deleteEntrys(RoomModel p_room) throws Exception{
        Map<String, Object> l_query = new HashMap<>();
        l_query.put(RoomSeatingModel.CN_ROOMID,p_room);

        Dao<RoomSeatingModel,Integer> l_dao = new RoomSeatingModel().getDao();
        List<RoomSeatingModel> l_list = l_dao.queryForFieldValues(l_query);
        l_dao.delete(l_list);
    }

    public void createEntrys(RoomModel p_room, JsonObject p_seating) throws Exception{
        JsonService l_jService = new JsonService();
        Dao<SeatingModel, Integer> l_dao = new SeatingModel().getDao();
        Dao<RoomSeatingModel, Integer> l_daoRS = new RoomSeatingModel().getDao();
            Map<String , JsonElement> l_map = l_jService.parseToMap(p_seating.toString());

            for(Map.Entry<String, JsonElement> l_entry : l_map.entrySet()){

                int l_id = Integer.valueOf(l_jService.getValueFromJsonElement(l_entry.getValue()));
                SeatingModel l_seating = l_dao.queryForId(l_id);
                l_dao.refresh(l_seating);

                RoomSeatingModel l_rsm = new RoomSeatingModel();
                l_rsm.setCla_room(p_room);
                l_rsm.setCla_seating(l_seating);

                l_daoRS.create(l_rsm);

            }


    }
    public boolean verify(JsonObject p_seating){
        JsonService l_jService = new JsonService();
        Dao<SeatingModel, Integer> l_dao = new SeatingModel().getDao();
        try {
            Map<String , JsonElement> l_map = l_jService.parseToMap(p_seating.toString());

            for(Map.Entry<String, JsonElement> l_entry : l_map.entrySet()){
                try{
                    int l_id = Integer.valueOf(l_jService.getValueFromJsonElement(l_entry.getValue()));
                    SeatingModel l_seating = l_dao.queryForId(l_id);
                    if(l_seating== null)
                        return false;

                }catch (Exception e){
                    return false;
                }


            }

        }catch (Exception e){
            return false;
        }

        return true;
    }

    public ArrayList<SeatingModel> getSeatingsForRoom(RoomModel p_room) throws Exception{
        Dao<RoomSeatingModel, Integer> l_daoRSM = new RoomSeatingModel().getDao();
        Dao<SeatingModel, Integer> l_daoSM = new SeatingModel().getDao();

        Map<String, Object> l_queryMap = new HashMap<String, Object>();


        l_queryMap.put("Room_idRoom",p_room);


            List<RoomSeatingModel> l_queryResult = l_daoRSM.queryForFieldValues(l_queryMap);
            ArrayList<SeatingModel> l_result = new ArrayList<SeatingModel>();
            for(RoomSeatingModel l_temp : l_queryResult){
                l_daoRSM.refresh(l_temp);


                SeatingModel l_entry = l_temp.getCla_seating();
                l_daoSM.refresh(l_entry);

                l_result.add(l_entry);
            }

        return l_result;
    }


    public boolean roomHasSeating(RoomModel p_room , int p_seatingId){
        boolean l_hasSeating = false;

        try {
            ArrayList<SeatingModel> l_availableSeating = getSeatingsForRoom(p_room);
            for(SeatingModel l_temp : l_availableSeating){
                if(l_temp.getCla_seatingId() == p_seatingId){
                    return  true;
                }

            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;

    }

}
