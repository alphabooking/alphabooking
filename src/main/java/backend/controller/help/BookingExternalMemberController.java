package backend.controller.help;

import backend.model.help.BookingExternalMemberModel;
import backend.model.help.BookingMemberModel;
import backend.model.main.BookingModel;
import backend.model.main.UserModel;
import backend.service.JsonService;
import backend.service.ValidateService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by patrick on 06.01.16.
 */
public class BookingExternalMemberController {

    public BookingExternalMemberController() {
    }

    public ArrayList<BookingExternalMemberModel> getExternalMemberforBooking(BookingModel p_bmodel) throws Exception {
        BookingExternalMemberModel model = new BookingExternalMemberModel();
        Dao<BookingExternalMemberModel, Integer> l_dao = model.getDao();

        ArrayList<BookingExternalMemberModel> ExternalMemberList = new ArrayList<BookingExternalMemberModel>();

        model.setCla_booking(p_bmodel);

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("idBooking", p_bmodel);


        List<BookingExternalMemberModel> l_list = l_dao.queryForFieldValues(map);


        for (BookingExternalMemberModel l_bem : l_list) {
            ExternalMemberList.add(l_bem);
        }

        return ExternalMemberList;
    }


    public void rollback(BookingModel p_booking){
        Dao<BookingExternalMemberModel, Integer> l_dao = new BookingExternalMemberModel().getDao();
        Map<String, Object> l_query = new HashMap<String, Object>();
        l_query.put("idBooking",p_booking);
        try {
            List<BookingExternalMemberModel> l_result = l_dao.queryForFieldValues(l_query);
            l_dao.delete(l_result);



        }catch(Exception e){
            e.printStackTrace();
        }


}

    public boolean addExternalMember(BookingModel p_booking, JsonElement p_emailJson) {
        JsonService l_jsonService = new JsonService();
        JsonObject l_emails;

        if (p_emailJson.isJsonObject()) {
            l_emails = (JsonObject) p_emailJson;
        } else {
            return false;
        }
        ValidateService l_validate = new ValidateService();

        try {
            Map<String, JsonElement> l_adresses = l_jsonService.parseToMap(l_emails.toString());
            Dao<BookingExternalMemberModel, Integer> l_dao = new BookingExternalMemberModel().getDao();

            for (Map.Entry<String, JsonElement> l_entry : l_adresses.entrySet()) {
                BookingExternalMemberModel l_newEntry = new BookingExternalMemberModel();

                l_newEntry.setCla_booking(p_booking);

                String l_email = l_jsonService.getValueFromJsonElement(l_entry.getValue());

                if(!l_validate.validateEmail(l_email))
                    return false;

                l_newEntry.setCla_mail(l_email);

                l_dao.create(l_newEntry);
            }


            return true;
        } catch (Exception e) {
            return false;
        }
    }
}