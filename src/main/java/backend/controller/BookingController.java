package backend.controller;

import backend.controller.help.*;
import backend.model.help.BookingSeenModel;
import backend.model.main.*;
import backend.service.DateService;
import backend.service.JsonService;
import backend.service.ValidateService;
import backend.service.mail.MailService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import spark.Request;
import spark.Response;

import java.sql.SQLException;
import java.util.*;

public class BookingController {


    public BookingController() { }


    /**
     * Link:
     * /api/booking/bufferTime/:id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String bufferTime(Request p_req, Response p_res, int id){
        JsonService l_jservice = new JsonService();
        String[] l_fields = new String[]{"bufferTime"};
        Map<String,JsonElement> l_json = l_jservice.verifyIncomeingJsons(p_req,p_res,l_fields);
        if(l_json == null)
            return "";
        int l_bufferTime;
        try {
           l_bufferTime = Integer.valueOf(l_jservice.getValueFromJsonElement(l_json.get("bufferTime")));
        }catch (Exception e){
            return MessageResponseService.makeMessage(p_res,412,"Die Pufferzeit muss numerisch sein");
        }

        Dao<BookingModel,Integer> l_dao = new BookingModel().getDao();

        try {
            BookingModel l_model = l_dao.queryForId(id);
            if(l_model == null)
                return MessageResponseService.makeMessage(p_res,412,"Die Buchung existiert nicht");

            l_model.setBufferTime(l_bufferTime);
            l_dao.update(l_model);

            return MessageResponseService.makeMessage(p_res,200,"Erfolgreich");

        }catch (Exception e){
            return MessageResponseService.makeMessage(p_res,500,"Interner Fehler");
        }


    }

    /**
     * Link:
     * /api/booking/update
     *
     *Beschreibung:
     *ändert eine Buchung
     */
    @Route(type = RouteType.POST, requiredRole = 2,requiresLogin = true)
    public String update(Request p_req, Response p_res){
        final String [] fields = new String[]{
                "bookingId",
                "roomId",
                };

        JsonService jservice = new JsonService();

        Map<String, JsonElement> l_map = jservice.verifyIncomeingJsons(p_req,p_res,fields);

        if(l_map == null)
            return "";

        int l_bookingId;
        int l_roomId;
        try{
             l_bookingId = Integer.valueOf(jservice.getValueFromJsonElement(l_map.get("bookingId")));
             l_roomId = Integer.valueOf(jservice.getValueFromJsonElement(l_map.get("roomId")));
        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res,412,"Die Felder bookingId & roomId müssen numerisch sein");
        }

        Dao<BookingModel, Integer> l_daoB = new BookingModel().getDao();
        Dao<RoomModel, Integer> l_daoR = new RoomModel().getDao();
        BookingModel l_booking;
        RoomModel l_room;
        try {
            l_booking = l_daoB.queryForId(l_bookingId);
            l_room = l_daoR.queryForId(l_roomId);
        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res,500,"Bei der Verarbeitung ist ein Fehler aufgetreten");
        }


        if(l_booking == null){
            return MessageResponseService.makeMessage(p_res,412,"Die Angeforderte Buchung existiert nicht");
        }
        if(l_room == null){
            return MessageResponseService.makeMessage(p_res,412,"Der Angeforderte Raum existiert nicht");
        }

        try{
            l_booking.setCla_room(l_room);
            l_booking.setCla_status(new StatusController().getById(4));
            l_daoB.update(l_booking);
        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res,500,"Beim Update ist ein Fehler aufgetreten");
        }

        try {
            MailService mailService = MailService.getMailService();
            mailService.sendBookingModification(l_booking);
        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res,200,"Beim Benachrichtigen der Teilnehmer ist ein Fehler aufgetreten");

        }

        return "";



    }

    /**
     * Link:
     * /api/booking/create
     *
     *Beschreibung:
     *legt eine neue Buchung an
     */
    @Route(type = RouteType.POST, requiredRole = 1,requiresLogin = true)
    public String create(Request p_req, Response p_res){

        final String [] l_fields = new String[]{
                "description",
                "beginDate",
                "beginTime",
                "endDate",
                "endTime",
                "extern",
                "memberCount",
                "costcenter",
                "service",
                "roomID",
                "seatingID",
                "additionalEquipment",
                "externalEmails",
                "comment",
        };




        JsonService jservice = new JsonService();
        ValidateService l_vs = new ValidateService();

        Map<String, JsonElement> l_map = jservice.verifyIncomeingJsons(p_req,p_res,l_fields);
        if(l_map == null)
            return "";





        BookingModel l_newBooking = new BookingModel();

        JsonElement l_buffer = l_map.get("description");
        l_newBooking.setCla_description(jservice.getValueFromJsonElement(l_buffer));




        // Verarbeitung des Datums
        //____________________________________________________
            JsonElement l_beginDate = l_map.get("beginDate");
            JsonElement l_beginTime = l_map.get("beginTime");
            JsonElement l_endDate = l_map.get("endDate");
            JsonElement l_endTime = l_map.get("endTime");

            Calendar l_begin;
            Calendar l_end;

            try {
                DateService l_ds = new DateService();
                l_begin = l_ds.getCalendar(
                        jservice.getValueFromJsonElement(l_beginDate),
                        jservice.getValueFromJsonElement(l_beginTime));

                l_end = l_ds.getCalendar(
                        jservice.getValueFromJsonElement(l_endDate),
                        jservice.getValueFromJsonElement(l_endTime));

                if(l_end.before(l_begin))
                    return MessageResponseService.makeMessage(p_res,412,"Das Ende einer Buchung darf nicht vor dem Beginn der Buchung liegen");

            } catch (Exception e) {

                e.printStackTrace();
                return MessageResponseService.makeMessage(p_res,412,"Formatierung wie folgt: Datum: DD.MM.YYYY , Uhrzeit: HH:MM");
            }

            l_newBooking.setCla_beginDate(l_begin.getTime());
            l_newBooking.setCla_endDate(l_end.getTime());
        //____________________________________________________



        String extern = jservice.getValueFromJsonElement(l_map.get("extern"));
        if(extern.equals("true")){
            l_newBooking.setCla_extern(true);

        }else{
            l_newBooking.setCla_extern(false);
        }




        try{
            int l_memberCount = Integer.valueOf(jservice.getValueFromJsonElement(l_map.get("memberCount")));
            l_newBooking.setCla_memberCount(l_memberCount);

        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res,412,"Das Feld Teilnehmerzahl muss numerisch sein");
        }


        // Überprüfen der Bestuhlung und des Raums
        //________________________________________________________


        int l_roomID;
        int l_seatingID;

        try {
            l_buffer = l_map.get("roomID");

            l_roomID = Integer.valueOf(jservice.getValueFromJsonElement(l_buffer));

            l_buffer = l_map.get("seatingID");

            l_seatingID = Integer.valueOf(jservice.getValueFromJsonElement(l_buffer));
        }catch(Exception e){

               return MessageResponseService.makeMessage(p_res,412, "Die Felder RaumID & BesthulungsID müssen numerisch sein");
        }



        RoomController l_roomController = new RoomController();
        RoomModel l_room ;
        try{
            l_room = l_roomController.getRoomById(l_roomID);
        }catch (Exception e){
            return MessageResponseService.makeMessage(p_res,412, "Der ausgewählte Raum könnte nicht existieren");
        }

        RoomSeatingController l_roomSeatingController = new RoomSeatingController();
        boolean l_roomHasSeating = l_roomSeatingController.roomHasSeating(l_room,l_seatingID);

        if(!l_roomHasSeating){
            return MessageResponseService.makeMessage(p_res,412, "Die Bestuhlung kann für diesen Raum nicht gebucht werden");
        }


        SeatingController l_seatingController = new SeatingController();
        SeatingModel l_seating;
        try {
            l_seating = l_seatingController.getSeatingById(l_seatingID);
        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res,412, "Die Bestuhlung könnte nicht existieren");
        }

        l_newBooking.setCla_room(l_room);
        l_newBooking.setCla_seating(l_seating);





        //Buchung abschließen
        //_____________________________________





        l_newBooking.setCla_createdAt(new Date(System.currentTimeMillis()));
        l_newBooking.setCla_status(new StatusController().getById(1)); // Angefordert
        l_newBooking.setCla_user(p_req.session().attribute("User"));

        Dao<BookingModel, Integer> l_DAO = l_newBooking.getDao();
        try {
            l_DAO.create(l_newBooking);
        }catch(Exception e){
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res,500, "Es ist ein Fehler beim Erstellen der Buchung aufgetreten");
        }


        //External Member & AdditionalEquipment
        //____________________________________________________________________________
        JsonElement l_emails = l_map.get("externalEmails");
        JsonElement l_additionalEquipments = l_map.get("additionalEquipment");
        BookingExternalMemberController l_bemc = new BookingExternalMemberController();
        BookingEquipmentController l_bec = new BookingEquipmentController();

        boolean l_successExternalMember = l_bemc.addExternalMember(l_newBooking, l_emails);

        if(l_successExternalMember){

            boolean l_successAdditionalEquipment = l_bec.addAdditionalEquipment(l_newBooking, l_additionalEquipments,p_res);


            //Rollback
            if(!l_successAdditionalEquipment){
                this.rollback(l_newBooking);
                return  "";//Message wird von addAdditionalEquipment gesetzt
            }


        }else{
            //Rollback
                this.rollback(l_newBooking);

            return MessageResponseService.makeMessage(p_res,412, "Überprüfen Sie die Formatierung der Email Adressen");
        }

        //service
        //______________________________________________

        if(l_newBooking.getCla_extern()){
            JsonElement l_service = l_map.get("service");


            String l_costcenter = jservice.getValueFromJsonElement(l_map.get("costcenter"));
            if(!l_vs.validateCostcenter(l_costcenter)){
               return MessageResponseService.makeMessage(p_res,412,"Die Kostenstelle muss achtstellig und numerisch sein");
            }



            //check if it is empty
            if(l_service.isJsonObject()){
                JsonObject l_tempObject = (JsonObject) l_service;
                //if it is empty, there are is no service/catering required
               if(l_tempObject.toString().equals("{}")){

               }else {


                   BookingHasCateringController l_bhcc = new BookingHasCateringController();
                   boolean successCatering = l_bhcc.addCatering(l_newBooking, l_service);
                   if(successCatering){
                   l_newBooking.setCla_service(true);
                       l_newBooking.setCostCenter(Integer.valueOf(l_costcenter));
                       l_newBooking.setComment(jservice.getValueFromJsonElement(l_map.get("comment")));
                   try {
                       l_DAO.update(l_newBooking);
                   }
                     catch (Exception e){
                         this.rollback(l_newBooking);
                     }}else{
                       this.rollback(l_newBooking);
                       return MessageResponseService.makeMessage(p_res,412, "Überprüfen Sie die Formatierung der Service-Ids");
                   }

               }


            }




        }




        return "";
    }


    public void rollback(BookingModel p_booking){
        new BookingEquipmentController().rollback(p_booking);
        new BookingExternalMemberController().rollback(p_booking);
        new BookingHasCateringController().rollback(p_booking);
        try{
        p_booking.getDao().delete(p_booking);}catch(Exception e){
            e.printStackTrace();
        }

        System.out.println("A Rollback for Booking:" + p_booking.getCla_bookingId() +" has been performed");
    }






    /**
     * Link:
     * /api/booking/getsinglebooking/:id
     *
     *Beschreibung:
     *gibt die Buchung mit der id für den user zurück
     */
    @Route(type = RouteType.GET, requiredRole = 1,requiresLogin = true)
    public String getsingle(Request p_req, Response p_res, int id){
        UserModel l_user = p_req.session().attribute("User");
        Map<String, Object> l_map = new HashMap<>();
        l_map.put("User_idUser",l_user);
        l_map.put("idBooking", id);
        return queryForBookings(p_req, p_res, l_map);
    }

    /**
     * Link:
     * /api/booking/getownby/:status
     *
     *Beschreibung:
     *gibt alle Buchungen des Status für den User zurück
     */
    @Route(type = RouteType.GET, requiresLogin = true, requiredRole = 1)
    public String getownbystatus(Request p_req, Response p_res, int id){

        UserModel l_user = p_req.session().attribute("User");
        Map<String, Object> l_map = new HashMap<>();
        l_map.put("User_idUser",l_user);
        l_map.put("status", id);
        return queryForBookings(p_req, p_res, l_map);
    }

    /**
     * Link:
     * /api/booking/getbookingsbystatus/:status
     *
     *Beschreibung:
     *gibt alle Buchungen des Status für den User zurück
     */
    @Route(type = RouteType.GET, requiresLogin = true, requiredRole = 2)
    public String getbystatus(Request p_req, Response p_res, int id){

        Map<String, Object> l_map = new HashMap<>();
        l_map.put("status", id);
        return queryForBookings(p_req, p_res, l_map);
    }





    /**
     * Link:
     * /api/booking/getallforuser
     *
     *Beschreibung:
     *gibt alle Buchungen für einen Nutzer zurück
     */
    @Route(type = RouteType.GET, requiresLogin = true, requiredRole = 1)
    public String getallforuser(Request p_req, Response p_res){

        UserModel l_user = p_req.session().attribute("User");
        Map<String, Object> l_map = new HashMap<>();
        l_map.put("User_idUser",l_user);
        return queryForBookings(p_req, p_res, l_map);
    }




    private boolean hasConflictingBookings(BookingModel l_model){

        Dao<BookingModel,Object> l_dao = l_model.getDao();
        Date l_begin = l_model.getCla_beginDate();
        Date l_end = l_model.getCla_endDate();

        StatusModel l_approve = new StatusController().getById(2);
        StatusModel l_changed = new StatusController().getById(4);

        QueryBuilder<BookingModel,Object> l_queryB = l_dao.queryBuilder();
        Where<BookingModel,Object> l_where = l_queryB.where();

        try {
            l_where.and(
                    l_where.or(
                        l_where.eq(BookingModel.CN_STATUS, l_approve),
                       l_where.eq(BookingModel.CN_STATUS, l_changed)),
                    l_where.or(
                            l_where.and(
                                    l_where.ge(BookingModel.CN_BEGINDATE,l_begin),
                                    l_where.le(BookingModel.CN_BEGINDATE,l_end)
                            ),
                            l_where.and(
                                    l_where.ge(BookingModel.CN_ENDDATE,l_begin),
                                    l_where.le(BookingModel.CN_ENDDATE,l_end)
                            ),
                            l_where.and(
                                    l_where.le(BookingModel.CN_BEGINDATE,l_begin),
                                    l_where.ge(BookingModel.CN_ENDDATE,l_end)
                            )

                    ),
                    l_where.eq(BookingModel.CN_ROOM,l_model.getCla_room())
            );
            l_queryB.setWhere(l_where);

            List<BookingModel> l_list = l_dao.query(l_queryB.prepare());
            if(l_list!=null)
                if(l_list.size() != 0)
                    return true;
            System.out.println(l_queryB.prepareStatementString());



        }catch (Exception e){
            return true;
        }




        return false;
    }

    /**
     * Link:
     * /api/booking/approve/:id
     *
     *Beschreibung:
     *gibt die buchung frei/bestätigung
     */
    @Route(type = RouteType.POST, requiresLogin = true, requiredRole = 2)
    public String approve(Request p_req, Response p_res, int id){

        Dao<BookingModel, Integer> l_dao = new BookingModel().getDao();

        try {

            BookingModel l_booking = l_dao.queryForId(id);

            if(l_booking == null){
                return MessageResponseService.makeMessage(p_res, 412, "Die Buchung existiert nicht");

            }
            l_dao.refresh(l_booking);

            if(l_booking.getCla_status().getId() != 1){
                return MessageResponseService.makeMessage(p_res, 409, "Die Buchung wurde bereits berarbeitet");
            }

            if(hasConflictingBookings(l_booking)){
                return MessageResponseService.makeMessage(p_res, 412, "In dem Zeitraum dieser Buchung findet bereits eine andere in dem selben Raum statt");

            }


            StatusModel l_status_new = new StatusController().getById(2);

            BookingSeenController bsc = new BookingSeenController();

            bsc.addNewEntry(l_booking, l_booking.getCla_status(), l_status_new);

            l_booking.setCla_status(l_status_new);

            Date l_date = new Date(System.currentTimeMillis());
            l_booking.setCla_approvedDate(l_date);

            l_dao.update(l_booking);

            try {
                MailService mailService = MailService.getMailService();
                mailService.sendBookingConfirmation(l_booking);
            }catch(Exception e){
                return MessageResponseService.makeMessage(p_res,200,"Beim Benachrichtigen der Teilnehmer ist ein Fehler aufgetreten");

            }
            return MessageResponseService.makeMessage(p_res, 200, "Die Buchung wurde erfolgreich freigegeben");
        }catch(Exception e){
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");

        }
    }

    /**
     * Link:
     * /api/booking/decline/:id
     *
     *Beschreibung:
     *lehnt die buchung ab
    */
    @Route(type = RouteType.POST, requiresLogin = true, requiredRole = 2)
    public String decline(Request p_req, Response p_res, int id){

        Dao<BookingModel, Integer> l_dao = new BookingModel().getDao();

        try {
            BookingModel l_booking = l_dao.queryForId(id);

            if(l_booking == null){
                return MessageResponseService.makeMessage(p_res,412, "Die Buchung existiert nicht");
            }
            l_dao.refresh(l_booking);
            if(l_booking.getCla_status().getId() != 1 || l_booking.getCla_status().getId() != 2 || l_booking.getCla_status().getId() != 4){
                if (l_booking.getCla_status().getId() == 3 )
                    return MessageResponseService.makeMessage(p_res, 409, "Die Buchung kann nicht abgelenht werden, da die Buchung bereits abgelehnt wurde");
                if (l_booking.getCla_status().getId() == 5 )
                    return MessageResponseService.makeMessage(p_res, 409, "Die Buchung kann nicht abgelenht werden, da die Buchung bereits abgelaufen ist");
                if (l_booking.getCla_status().getId() == 6 )
                    return MessageResponseService.makeMessage(p_res, 409, "Die Buchung kann nicht abgelenht werden, daa die Buchung bereits storniert wurde");
            }
            StatusModel l_status_new = new StatusController().getById(3);

            BookingSeenController bsc = new BookingSeenController();

            bsc.addNewEntry(l_booking, l_booking.getCla_status(), l_status_new);

            l_booking.setCla_status(l_status_new);
            l_dao.update(l_booking);

            try {
                MailService mailService = MailService.getMailService();
                mailService.sendBookingCancellation(l_booking);
            }catch(Exception e){
                return MessageResponseService.makeMessage(p_res,200,"Beim Benachrichtigen der Teilnehmer ist ein Fehler aufgetreten");

            }

            return MessageResponseService.makeMessage(p_res, 200, "Die Buchung wurde erfolgreich abgelehnt");
        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res, 500, "Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");

        }
    }


    /**
     * Link:
     * /api/booking/cancel/:id
     *
     *Beschreibung:
     *storniert die Buchung mit der id
     */
    @Route(type = RouteType.POST, requiresLogin = true, requiredRole = 1)
    public String cancel(Request p_req, Response p_res, int id){

        Dao<BookingModel, Integer> l_dao = new BookingModel().getDao();

        try {
            UserModel l_user = p_req.session().attribute("User");

            BookingModel l_booking = l_dao.queryForId(id);

            if(l_booking == null){
                return MessageResponseService.makeMessage(p_res, 412, "Die Buchung existiert nicht");
            }
            l_dao.refresh(l_booking);


            StatusModel l_status_new = new StatusController().getById(6);

            BookingSeenController bsc = new BookingSeenController();

            bsc.addNewEntry(l_booking, l_booking.getCla_status(), l_status_new);

            l_booking.setCla_status(l_status_new);
           if(l_booking.getCla_user().getCla_userId() == l_user.getCla_userId()){
               l_dao.update(l_booking);

               try {
                   MailService mailService = MailService.getMailService();
                   mailService.sendBookingCancellation(l_booking);
               }catch(Exception e){
                   return MessageResponseService.makeMessage(p_res,200,"Beim Benachrichtigen der Teilnehmer ist ein Fehler aufgetreten");
               }

               return MessageResponseService.makeMessage(p_res, 200, "Die Buchung wurde erfolgreich storniert");
           }else{
               return MessageResponseService.makeMessage(p_res, 409, "Die Buchung gehört nicht Ihnen");
           }




        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res, 500, "Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");

        }
    }



    /**
     * Link:
     * /api/booking/news
     */
    @Route(type = RouteType.GET, requiresLogin = true, requiredRole = 1)
    public String news(Request p_req ,Response p_res){
        List<BookingSeenModel> l_list = new BookingSeenController().getNewsForUser(p_req.session().attribute("User"));

        JsonService l_jservice = new JsonService();

        return l_jservice.parseToJson(l_list);

    }


    /**
     * Link:
     * /api/booking/marknewsasseen
     */
    @Route(type = RouteType.GET, requiresLogin = true, requiredRole = 1)
    public String marknewsasseen(Request p_req ,Response p_res){
        BookingSeenController l_bsc = new BookingSeenController();

        try {
            UserModel l_user = p_req.session().attribute("User");
            l_bsc.markasseen(l_user);
            return MessageResponseService.makeMessage(p_res,200,"Neuigkeiten erfolgreich als gelesen markiert");
        }catch (Exception e){
            return MessageResponseService.makeMessage(p_res,500,"Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");
        }
    }



    public List<BookingModel> getAllMeetingsByDate(List<RoomModel> p_list , Date p_beginOfDay , Date p_endOfDay) throws Exception{

           List<BookingModel> l_list = this.getAllHeldBookings(p_list);
           List<BookingModel> l_result = new ArrayList<>();

           Calendar l_begin = Calendar.getInstance();
           l_begin.setTime(p_beginOfDay);
           Calendar l_end = Calendar.getInstance();
           l_end.setTime(p_endOfDay);


           for(BookingModel l_model : l_list){
               System.out.println();
               Calendar l_bookingBegin = Calendar.getInstance();
               l_bookingBegin.setTime(l_model.getCla_beginDate());

               Calendar l_bookingEnd = Calendar.getInstance();
               l_bookingEnd.setTime(l_model.getCla_endDate());



               // Die Buchung ist relevant, wenn sie an dem Tag anfängt
               if(l_bookingBegin.after(l_begin) && l_bookingBegin.before(l_end)) {
                   l_result.add(l_model);
                   continue;
               }

               //Die Buchung ist relevant, wenn sie an dem Tag endet
               if(l_bookingEnd.after(l_begin) && l_bookingEnd.before(l_end)) {
                   l_result.add(l_model);
                   continue;
               }

               //Die Buchung ist relevant, wenn sie vor dem Tag anfängt und nach dem Tag endet
               if(l_bookingBegin.before(l_begin) && l_bookingEnd.after(l_end)) {
                   l_result.add(l_model);
                   continue;
               }






           }


           return  l_result;
       }

    public List<BookingModel> getAllHeldBookings(List<RoomModel> p_list) throws Exception{
        Dao<BookingModel, Integer> l_daoB = new BookingModel().getDao();
        QueryBuilder<BookingModel, Object> queryBuilder =
                new BookingModel().getDao().queryBuilder();

        Where<BookingModel, Object> l_where1 = queryBuilder.where();

        //Alle Buchungen die Angenommen, oder Geändert sind
        StatusController l_sc = new StatusController();

        l_where1.or(l_where1.eq(BookingModel.CN_STATUS, l_sc.getById(2)),
                    l_where1.eq(BookingModel.CN_STATUS,l_sc.getById(4)))
                    .and().
                    in(BookingModel.CN_ROOM,p_list);

        queryBuilder.setWhere(l_where1);
        List<BookingModel> l_bookings = l_daoB.query(queryBuilder.prepare());

        for(BookingModel l_booking: l_bookings){
            l_daoB.refresh(l_booking);
        }
        return  l_bookings;
    }

    public String queryForBookings(Request p_req, Response p_res, Map<String, Object> p_map)
    {
        Dao<BookingModel, Integer> l_dao = new BookingModel().getDao();

        try {
            List<BookingModel> l_list = l_dao.queryForFieldValues(p_map);
            for(BookingModel l_booking : l_list){
                l_dao.refresh(l_booking);
            }

            JsonService l_jsonService = new JsonService();
            String l_jsonString = l_jsonService.parseToJson(l_list);




            return l_jsonString;



        } catch (SQLException e) {
            return MessageResponseService.makeMessage(p_res, 500, "Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");
        }


    }



    public Runnable checkForExpiredBookings(){
        /* Checkt alle 15min ob es Buchungen gibt, die Abgelaufen sind
         * eine Buchung ist abgelaufen, wenn sie den Status "Angefordert" hat
         * und ihr ende in der Vergangenheit liegt*/

        final long l_wait = 900000;
        return ()->{
            Dao<BookingModel, Integer> l_dao = new BookingModel().getDao();
            StatusController l_stc = new StatusController();

            QueryBuilder<BookingModel, Integer> l_queryBuilder = l_dao.queryBuilder();

            StatusModel l_statusReq = l_stc.getById(1);
            StatusModel l_statusExp = l_stc.getById(5);

            while(42==42){
                Date l_date = new Date(System.currentTimeMillis());

                Where<BookingModel,Integer> l_where = l_queryBuilder.where();

                try {
                    l_where.eq(BookingModel.CN_STATUS, l_statusReq)
                            .and()
                            .le(BookingModel.CN_ENDDATE, l_date);


                    l_queryBuilder.setWhere(l_where);

                    List<BookingModel> l_list = l_dao.query(l_queryBuilder.prepare());
                    for(BookingModel l_model : l_list){
                        l_dao.refresh(l_model);
                        l_model.setCla_status(l_statusExp);
                        l_dao.update(l_model);
                    }







                }catch (Exception e){
                    System.out.println("An Exception encountered in the Thread  checkForExpiredBookings");
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(l_wait);
                }catch (Exception e){

                }


            }






        };


    }





}
