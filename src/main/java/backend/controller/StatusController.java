package backend.controller;

import backend.model.main.SeatingModel;
import backend.model.main.StatusModel;
import backend.service.JsonService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.j256.ormlite.dao.Dao;
import spark.Request;
import spark.Response;

import java.util.List;

/**
 * Created by Felix on 08.01.16.
 */
public class StatusController {

    /**
     * Link:
     * /api/status/getList
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String getList(Request p_req, Response p_res){
        List<StatusModel> l_list = getALL();
        if(l_list!=null)
            return new JsonService().parseToJson(l_list);
        else
            return MessageResponseService.makeMessage(p_res,500, "Es ist ein interner Fehler aufgetreten");



    }


    public List<StatusModel> getALL(){
        Dao<StatusModel, Integer> l_dao = new StatusModel().getDao();
        try {
            List<StatusModel> l_list = l_dao.queryForAll();
            for (StatusModel l_statusModel : l_list)
                l_dao.refresh(l_statusModel);

            return l_list;
        }catch (Exception e) {
            return null;
        }


    }


    public StatusModel getById(int p_id){
        Dao<StatusModel, Integer> l_dao = new StatusModel().getDao();

        try {
            StatusModel l_model = l_dao.queryForId(p_id);
            l_dao.refresh(l_model);
            return  l_model;
        }catch(Exception e) {
            return null;
        }
    }



    }



