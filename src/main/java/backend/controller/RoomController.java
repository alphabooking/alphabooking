package backend.controller;

import backend.controller.help.BookingSeenController;
import backend.controller.help.RoomEquipmentController;
import backend.controller.help.RoomSeatingController;
import backend.model.help.FixEquipmentModel;
import backend.model.main.BookingModel;
import backend.model.main.RoomModel;
import backend.model.main.SeatingModel;
import backend.model.main.StatusModel;
import backend.service.DateService;
import backend.service.JsonService;
import backend.service.TimetableEntry;
import backend.service.ValidateService;
import backend.service.mail.MailService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;

import java.text.SimpleDateFormat;
import java.util.*;

public class RoomController {


    public RoomController() {
    }

    /**
     * Link:
     * /api/room/getpossiblerooms
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getpossiblerooms(Request p_req, Response p_res, int p_id){

        SimpleDateFormat l_timeFormatter = new SimpleDateFormat("HH:mm",Locale.GERMANY);
        SimpleDateFormat l_dateFormatter = new SimpleDateFormat("dd.MM.yyyy",Locale.GERMANY);

        Dao<BookingModel, Integer> l_dao = new BookingModel().getDao();
        List<RoomModel> l_list;

        try {
            BookingModel l_booking = l_dao.queryForId(p_id);


            DateService l_dateService = new DateService();

            Calendar l_beginCalendar = l_dateService.getCalendar(l_dateFormatter.format(l_booking.getCla_beginDate()), l_timeFormatter.format(l_booking.getCla_beginDate()));
            Calendar l_endCalendar = l_dateService.getCalendar(l_dateFormatter.format(l_booking.getCla_endDate()), l_timeFormatter.format(l_booking.getCla_endDate()));
            int l_member = l_booking.getCla_memberCount();



            l_list = getAllFreeRooms(p_req, p_res, l_beginCalendar, l_endCalendar, l_member, 5);
            if(l_list.isEmpty()) {
                l_list = getAllFreeRooms(p_req, p_res, l_beginCalendar, l_endCalendar, l_member, 100);
            }

            JsonService l_jservice = new JsonService();
            l_list.add(l_booking.getCla_room());
            return l_jservice.parseToJson(l_list);

        }catch(Exception e){
            return MessageResponseService.makeMessage(p_res, 500, "Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");

        }

    }

    /**
     * Link:
     * /api/room/getallrooms
     */
    @Route(type = RouteType.GET,requiredRole = 0,requiresLogin = false)
    public String getallrooms(Request p_req, Response p_res){
            try {

                List<RoomModel> l_rList = new RoomModel().getDao().queryForAll();
                ArrayList l_returnList = new ArrayList<>();

                for(RoomModel rmodel : l_rList) {
                    l_returnList.add(rmodel);
                }

                JsonService l_jservice = new JsonService();
                return l_jservice.parseToJson(l_returnList);
            } catch (Exception e) {
                return MessageResponseService.makeMessage(p_res,500, "Es ist ein Fehler bei der Bearbeitung aufgetreten");
            }
        }


    /**
     * Link:
     * /api/room/list
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String list(Request p_req, Response p_res) {
        Map<String, Object> l_query = new HashMap<>();
        l_query.put(RoomModel.CN_ACTIVE, true);

        Dao<RoomModel, Integer> l_dao = new RoomModel().getDao();
        try {
            List<RoomModel> l_rooms = l_dao.queryForFieldValues(l_query);
            for (RoomModel l_tmp : l_rooms)
                l_dao.refresh(l_tmp);


            JsonService l_json = new JsonService();
            return l_json.parseToJson(l_rooms);

        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein interner Fehler aufgetreten");

        }


    }


    /**
     * Link:
     * /api/room/create
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String create(Request p_req, Response p_res) {
        String[] l_jsonFields = new String[]{
                "roomName",
                "description",
                "capacity",
                "seatings",
                "fixedEquipment"
        };

        JsonService l_jService = new JsonService();
        ValidateService l_validate = new ValidateService();

        Map<String, JsonElement> l_json = l_jService.verifyIncomeingJsons(p_req, p_res, l_jsonFields);

        if (l_json == null)
            return "";


        int l_capacity = -1;
        try {
            l_capacity = Integer.valueOf(l_jService.getValueFromJsonElement(l_json.get("capacity")));
        } catch (Exception e) {
            MessageResponseService.makeMessage(p_res, 412, "Die Kapizität eines Raums muss numerisch sein");
            return "";
        }


        String l_description = l_jService.getValueFromJsonElement(l_json.get("description"));
        String l_roomname = l_jService.getValueFromJsonElement(l_json.get("roomName"));

        JsonElement l_seating = l_json.get("seatings");
        JsonElement l_fixedEquipments = l_json.get("fixedEquipment");

        if (l_seating.isJsonObject() && l_fixedEquipments.isJsonObject()) {
            JsonObject l_seatingJO = (JsonObject) l_seating;
            JsonObject l_fixedJO = (JsonObject) l_fixedEquipments;

            if (l_seatingJO.toString().equals("{}")) {
                MessageResponseService.makeMessage(p_res, 412, "Ein Raum muss wenigstens eine Bestuhulung haben");
                return "";
            }
            if (l_fixedJO.toString().equals("{}")) {
                MessageResponseService.makeMessage(p_res, 412, "Ein Raum muss wenigstens eine feste Auststattung haben");
                return "";
            }

            RoomEquipmentController l_reC = new RoomEquipmentController();
            RoomSeatingController l_rsC = new RoomSeatingController();


            boolean l_seatingVerified = l_rsC.verify(l_seatingJO);
            boolean l_fixedVerified = l_reC.verify(l_fixedJO);

            if (l_seatingVerified && l_fixedVerified) {
                RoomModel l_newRoom = new RoomModel();
                l_newRoom.setCla_active(true);
                l_newRoom.setCla_user(p_req.session().attribute("User"));
                l_newRoom.setCla_capacity(l_capacity);
                l_newRoom.setCla_name(l_roomname);
                l_newRoom.setCla_description(l_description);

                Dao<RoomModel, Integer> l_dao = l_newRoom.getDao();
                try {
                    l_dao.create(l_newRoom);
                    l_rsC.createEntrys(l_newRoom, l_seatingJO);
                    l_reC.createEntrys(l_newRoom, l_fixedJO);


                } catch (Exception e) {
                    e.printStackTrace();
                    return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
                }


            } else {
                MessageResponseService.makeMessage(p_res, 412, "Überprüfen Sie, ob die gewählten Bestuhulungen und Ausstattungen existieren");
                return "";
            }

        } else {
            MessageResponseService.makeMessage(p_res, 412, "Die Bestuhlung und Ausstattung müssen Als JSON-Object übergeben werden");
            return "";
        }


        return "";
    }

    /**
     * Link:
     * /api/room/edit/:id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String edit(Request p_req, Response p_res, int p_id) {
        String[] l_jsonFields = new String[]{
                "roomName",
                "description",
                "capacity",
                "seatings",
                "fixedEquipments"
        };

        JsonService l_jService = new JsonService();
        ValidateService l_validate = new ValidateService();
        Dao<RoomModel, Integer> l_dao = new RoomModel().getDao();

        Map<String, JsonElement> l_json = l_jService.verifyIncomeingJsons(p_req, p_res, l_jsonFields);

        if (l_json == null)
            return "";


        int l_capacity = 0;
        try {
            l_capacity = Integer.valueOf(l_jService.getValueFromJsonElement(l_json.get("capacity")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 412, "Die Kapazität eines Raums muss numerisch sein");
        }


        RoomModel l_room = null;
        try {
            l_room = l_dao.queryForId(p_id);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }

        if (l_room == null)
            return MessageResponseService.makeMessage(p_res, 412, "Der angefragte Raum existiert nicht");


        RoomSeatingController l_rsC = new RoomSeatingController();
        RoomEquipmentController l_reC = new RoomEquipmentController();


        JsonElement l_seating = l_json.get("seatings");
        JsonElement l_equip = l_json.get("fixedEquipments");

        JsonObject l_seatingJO = null;
        JsonObject l_equipJO = null;

        if (l_seating.isJsonObject() && l_equip.isJsonObject()) {
            l_seatingJO = (JsonObject) l_seating;
            l_equipJO = (JsonObject) l_equip;

            if (l_seatingJO.toString().equals("{}")) {
                return MessageResponseService.makeMessage(p_res, 412, "Ein Raum muss wenigstens eine Bestuhulung haben");
            }
            if (l_equipJO.toString().equals("{}")) {
                return MessageResponseService.makeMessage(p_res, 412, "Ein Raum muss wenigstens eine feste Auststattung haben");
            }

            boolean l_seatingVerified = l_rsC.verify(l_seatingJO);
            boolean l_fixedVerified = l_reC.verify(l_equipJO);

            if (l_seatingVerified && l_fixedVerified) {
                l_room.setCla_name(l_jService.getValueFromJsonElement(l_json.get("roomName")));
                l_room.setCla_description(l_jService.getValueFromJsonElement(l_json.get("description")));
                l_room.setCla_capacity(l_capacity);


                try {
                    l_dao.update(l_room);

                    l_rsC.deleteEntrys(l_room);
                    l_reC.deleteEntrys(l_room);


                    l_rsC.createEntrys(l_room, l_seatingJO);
                    l_reC.createEntrys(l_room, l_equipJO);


                } catch (Exception e) {
                    e.printStackTrace();
                    return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
                }


            } else {
                return MessageResponseService.makeMessage(p_res, 412, "Überprüfen Sie, ob die gewählten Bestuhulungen und Ausstattungen existieren");
            }
        } else {
            return MessageResponseService.makeMessage(p_res, 412, "Die Bestuhlungen und Ausstatunngen müssen als JSON-Objekt übergeben werden");
        }


        return MessageResponseService.makeMessage(p_res, 200, "Die Änderungen wurden erfolgreich durchgeführt");


    }

    /**
     * Link:
     * /api/room/deactivate/:id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String deactivate(Request p_req, Response p_res, int p_id) {
        Dao<RoomModel, Integer> l_dao = new RoomModel().getDao();
        Dao<BookingModel, Object> l_daoB = new BookingModel().getDao();
        try {
            RoomModel l_roomModel = l_dao.queryForId(p_id);
            if (l_roomModel != null) {
                l_dao.refresh(l_roomModel);

                l_roomModel.setCla_active(false);

                l_dao.update(l_roomModel);

                MessageResponseService.makeMessage(p_res, 200, "Der Raum wurde erfolgreich deaktiviert");
            } else {
                MessageResponseService.makeMessage(p_res, 412, "Der angefragte Raum existiert nicht");
            }

            QueryBuilder<BookingModel, Object> l_qb = l_daoB.queryBuilder();
            Where l_where = l_qb.where();
            l_where.and(
                    l_where.eq("Room_idRoom",p_id),
                    l_where.not().eq("status",3),
                    l_where.ge("startTime",new Date())
            );

            List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());

            if (!l_bookingList.isEmpty()){
                StatusModel l_status_new = new StatusController().getById(3);
                for(BookingModel l_booking : l_bookingList) {
                    BookingSeenController bsc = new BookingSeenController();

                    bsc.addNewEntry(l_booking, l_booking.getCla_status(), l_status_new);

                    l_booking.setCla_status(l_status_new);
                    l_daoB.update(l_booking);

                    try {
                        MailService mailService = MailService.getMailService();
                        mailService.sendBookingCancellation(l_booking);
                    }catch(Exception e){
                        return MessageResponseService.makeMessage(p_res,200,"Beim Benachrichtigen der Teilnehmer ist ein Fehler aufgetreten");

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            MessageResponseService.makeMessage(p_res, 500, "Beim Bearbeiten der Anfrage ist ein Fehler aufgetreten");
        }
        return "";

    }

    /**
     * Link:
     * /api/room/timetable/:id
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String timetable(Request p_req, Response p_res, int p_id) {
        return this.timetableadm(p_req, p_res, p_id).replaceAll("(\"booking_description\":\"[^\"]{0,}\"[,]?)", "").replaceAll("(\"booking_id\":[^\"^,]*[,]?)", "");

    }

    /**
     * Link:
     * /api/room/timetableadm/:id
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String timetableadm(Request p_req, Response p_res, int p_id) {
        QueryParamsMap l_map = p_req.queryMap();
        String l_Date = l_map.value("date");

        ValidateService l_vs = new ValidateService();
        if (l_Date == null)
            return MessageResponseService.makeMessage(p_res, 412, "Der GET Parameter date muss gesetzt sein");

        boolean l_isDate = l_vs.verifyDate(l_Date);
        if (!l_isDate) {
            return MessageResponseService.makeMessage(p_res, 412, "Das Datum muss nach DD:MM:JJJJ formatiert sein");
        }
        DateService l_ds = new DateService();

        Calendar l_calendarB = l_ds.getCalendar(l_Date, "00:00");
        Calendar l_calendarE = l_ds.getCalendar(l_Date, "23:59");

        RoomModel l_room;
        try {
            l_room = this.getRoomById(p_id);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Beim Berarbeiten der Anfrage ist ein Fehler aufgetreten");

        }

        if (l_room == null) {
            return MessageResponseService.makeMessage(p_res, 412, "Der Angeforderte Raum Existiert nicht");
        }

        ArrayList<RoomModel> l_roomList = new ArrayList<>();
        l_roomList.add(l_room);

        try {


            BookingController l_bc = new BookingController();
            List<BookingModel> l_bookingList = l_bc.getAllMeetingsByDate(l_roomList, l_calendarB.getTime(), l_calendarE.getTime());
            List<TimetableEntry> l_timetable = new ArrayList<>();

            for (BookingModel l_booking : l_bookingList) {

                l_timetable.add(new TimetableEntry(l_booking.getCla_beginDate(),
                        l_booking.getCla_endDate(),
                        l_booking.getCla_bookingId(),
                        l_booking.getCla_description(),
                        l_booking.getCla_room()));
            }

            JsonService l_jService = new JsonService();

            return l_jService.parseToJson(l_timetable);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Beim Berarbeiten der Anfrage ist ein Fehler aufgetreten");
        }


    }

    /**
     * Link:
     * /api/room/getfreerooms
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String getfreerooms(Request p_req, Response p_res) {
        QueryParamsMap l_map = p_req.queryMap();
        String l_beginDate = l_map.value("beginDate");
        String l_beginTime = l_map.value("beginTime");
        String l_endTime = l_map.value("endTime");
        String l_seating = l_map.value("seating");
        String l_memberCount = l_map.value("memberCount");
        String l_fixEquipment = l_map.value("fixedEquipment");

        DateService l_dateService = new DateService();

        if (l_beginDate == null) {
            return MessageResponseService.makeMessage(p_res, 412, "Der GET-Parameter muss gesetzt werden: beginDate");
        }

        ValidateService l_vs = new ValidateService();
        boolean l_isDate = l_vs.verifyDate(l_beginDate);
        if (!l_isDate) {
            return MessageResponseService.makeMessage(p_res, 412, "Ein Datum muss DD.MM.JJJJ formatiert sein");
        }

        Calendar l_begin = l_dateService.getCalendar(l_beginDate);


        if (l_beginTime != null) {
            if (!l_vs.verifyTime(l_beginTime)) {
                return MessageResponseService.makeMessage(p_res, 412, "Die Uhrzeit muss nach HH:MM formatiert sein");
            } else {
                l_begin = l_dateService.getCalendar(l_beginDate, l_beginTime);
            }
        }

        Calendar l_end = null;

        if (l_endTime != null) {
            if (!l_vs.verifyTime(l_endTime)) {
                return MessageResponseService.makeMessage(p_res, 412, "Die Uhrzeit muss nach HH:MM formatiert sein");
            } else {
                l_end = l_dateService.getCalendar(l_beginDate, l_endTime);
            }
        }

        List<RoomModel> l_list;
        int mem_count = 1;

        if (l_memberCount != null) {

            try {
                mem_count = Integer.valueOf(l_memberCount);
            } catch (Exception e) {

                return MessageResponseService.makeMessage(p_res, 412, "Die Mitgliederanzahl muss numerisch sein");
            }

        }


        if (l_end == null) {
            if (mem_count == 1) {
                l_list = this.getAllFreeRooms(p_req, p_res, l_begin, null, mem_count, 100);
            } else {
                l_list = this.getAllFreeRooms(p_req, p_res, l_begin, null, mem_count, 5);
            }
        } else {
            if (mem_count == 1) {
                l_list = this.getAllFreeRooms(p_req, p_res, l_begin, l_end, mem_count, 100);
            } else {
                l_list = this.getAllFreeRooms(p_req, p_res, l_begin, l_end, mem_count, 5);
            }

        }

        l_list = this.getFixedAndSeating(l_seating, l_fixEquipment, l_list, p_res);


        if (l_list == null)
            return "";

        return new JsonService().parseToJson(l_list);
    }

    /**
     * Link:
     * /api/room/getfreeroomsmultipledays
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String getfreeroomsmultipledays(Request p_req, Response p_res) {
        QueryParamsMap l_map = p_req.queryMap();
        String l_beginDate = l_map.value("beginDate");
        String l_beginTime = l_map.value("beginTime");
        String l_endDate = l_map.value("endDate");
        String l_endTime = l_map.value("endTime");
        String l_seating = l_map.value("seating");
        String l_memberCount = l_map.value("memberCount");
        String l_fixEquipment = l_map.value("fixedEquipment");

        ValidateService l_validator = new ValidateService();

        if (l_beginDate == null || l_beginTime == null || l_endDate == null || l_endTime == null) {
            return MessageResponseService.makeMessage(p_res, 412, "Die GET-Parameter beginDate, beginTime, endDate, endTime müssen gefüllt sein");
        }

        if (!(l_validator.verifyDate(l_beginDate) && l_validator.verifyDate(l_endDate))) {
            return MessageResponseService.makeMessage(p_res, 412, "Ein Datum muss DD.MM.YYYY formatiert sein");
        }

        if (!(l_validator.verifyTime(l_beginTime) && l_validator.verifyTime(l_endTime))) {
            return MessageResponseService.makeMessage(p_res, 412, "Eine Uhrzeit muss HH:MM formatiert sein");
        }

        DateService l_ds = new DateService();

        Calendar l_beginCalendar = l_ds.getCalendar(l_beginDate, l_beginTime);


        Calendar l_endCalendar = l_ds.getCalendar(l_endDate, l_endTime);


        int l_member = 1;

        if (l_memberCount != null) {
            try {
                l_member = Integer.valueOf(l_memberCount);
            } catch (Exception e) {
                return MessageResponseService.makeMessage(p_res, 412, "Die Teilnehmeranzahl muss numerisch sein");
            }
        }

        List<RoomModel> l_Liste = null;

        if (l_member == 1) {
            l_Liste = getAllFreeRooms(p_req, p_res, l_beginCalendar, l_endCalendar, l_member, 100);
        } else {
            l_Liste = getAllFreeRooms(p_req, p_res, l_beginCalendar, l_endCalendar, l_member, 5);
        }


        l_Liste = this.getFixedAndSeating(l_seating, l_fixEquipment, l_Liste, p_res);
        if (l_Liste == null) {
            return "";
        }


        JsonService l_jService = new JsonService();

        return l_jService.parseToJson(l_Liste);
    }


    private List<RoomModel> getFixedAndSeating(String l_seating, String l_fixEquipment, List<RoomModel> l_list, Response p_res) {
        ValidateService l_vs = new ValidateService();

        if (l_seating != null) {
            try {
                l_list = getRoomsWithSeating(Integer.valueOf(l_seating), l_list);
            } catch (Exception e) {
                MessageResponseService.makeMessage(p_res, 412, "Die Bestuhlungsid muss numerisch sein");
                return null;
            }
        }

        if (l_fixEquipment != null) {


            if (!l_vs.verifyFixedEquipmentGetParameter(l_fixEquipment)) {

                MessageResponseService.makeMessage(p_res, 412, "Fixed Equipments müsssen [{id:1},{id:2}] formatiert sein");
                return null;
            }

            l_fixEquipment = l_fixEquipment.replaceAll("\\[|\\]|\\{|\\}|id:", "");
            String[] l_aEquipment = l_fixEquipment.split(",");
            int[] l_fIds = new int[l_aEquipment.length];
            int count = 0;
            try {
                for (String l_tmp : l_aEquipment) {
                    l_fIds[count] = Integer.valueOf(l_aEquipment[count]);
                    count++;
                }
            } catch (Exception e) {
                MessageResponseService.makeMessage(p_res, 412, "Die EquipmentIDs müssen numerisch sein");
                return null;
            }

            l_list = getRoomsWithFixedEquipment(l_fIds, l_list);

        }

        return l_list;
    }

    private List<RoomModel> getRoomsWithFixedEquipment(int[] ids, List<RoomModel> p_potentials) {
        List<RoomModel> l_result = new ArrayList<>();
        RoomEquipmentController l_rec = new RoomEquipmentController();

        for (RoomModel l_model : p_potentials) {
            try {
                ArrayList<FixEquipmentModel> l_fix = l_rec.getFixEquipmentForRoom(l_model);

                boolean l_hasAllEquipments = false;
                for (int i : ids) {
                    for (FixEquipmentModel l_fixModel : l_fix) {
                        if (l_fixModel.getCla_idFixEquipment() == i) {
                            l_hasAllEquipments = true;
                            break;
                        } else {
                            l_hasAllEquipments = false;
                        }
                    }
                }

                if (l_hasAllEquipments)
                    l_result.add(l_model);

            } catch (Exception e) {
                return null;
            }
        }
        return l_result;


    }

    private List<RoomModel> getRoomsWithSeating(int p_seatingId, List<RoomModel> p_potentials) {
        RoomSeatingController l_rsc = new RoomSeatingController();
        List<RoomModel> l_result = new ArrayList<RoomModel>();

        for (RoomModel l_model : p_potentials) {
            if (l_rsc.roomHasSeating(l_model, p_seatingId))
                l_result.add(l_model);
        }

        return l_result;

    }


    private List<RoomModel> getAllFreeRooms(Request p_req, Response p_res, Calendar p_begin, Calendar p_end, int p_memberCount, int p_positiveDeviation) {
        Map<String, Object> l_map = new HashMap<String, Object>();


        Dao<RoomModel, Integer> l_dao = new RoomModel().getDao();


        try {
            Map<String, Object> l_queryMap = new HashMap<>();
            l_queryMap.put(RoomModel.CN_ACTIVE, true);

            List<RoomModel> l_list = l_dao.queryForFieldValues(l_queryMap);
            List<RoomModel> l_potentials = new ArrayList<>();
            for (RoomModel l_tmpRoom : l_list) {
                l_dao.refresh(l_tmpRoom);
                if (p_memberCount > 0) {
                    if (l_tmpRoom.getCla_capacity() < p_memberCount ||
                            l_tmpRoom.getCla_capacity() > (p_memberCount + p_positiveDeviation)) {

                    } else {
                        l_potentials.add(l_tmpRoom);
                    }
                }
            }


            if (l_potentials.isEmpty()) {
                return null;
            }


            List<BookingModel> l_bookings = new BookingController().getAllHeldBookings(l_potentials);

            for (BookingModel l_bookingModel : l_bookings) {
                boolean l_deleteFlag = false;

                Calendar l_bookingBegin = Calendar.getInstance();
                l_bookingBegin.setTime(l_bookingModel.getCla_beginDate());
                l_bookingBegin.add(Calendar.MINUTE, -1 * l_bookingModel.getBufferTime());

                Calendar l_bookingEnd = Calendar.getInstance();
                l_bookingEnd.setTime(l_bookingModel.getCla_endDate());
                l_bookingEnd.add(Calendar.MINUTE, l_bookingModel.getBufferTime());

                //check if the requested starting time is in between the time of a booking

                if (p_begin != null) {



                    boolean l_before = l_bookingBegin.before(p_begin);
                    boolean l_after = l_bookingEnd.after(p_begin);



                    if (l_before && l_after) {
                        if(p_end == null){

                        }else {
                            l_deleteFlag = true;
                        }
                    }
                }

                //check if the requested ending time is in between the time of a booking
                if (p_end != null) {
                    boolean l_before = l_bookingBegin.before(p_end);
                    boolean l_after = l_bookingEnd.after(p_end);



                    if (l_before && l_after) {
                        l_deleteFlag = true;
                    }
                }

                //check if the required booking has a booking inbetween
                if (p_end != null && p_begin != null) {
                    boolean bool1 = l_bookingBegin.after(p_begin);
                    boolean bool2 = l_bookingEnd.before(p_end);

                    if (bool1 && bool2) {
                        l_deleteFlag = true;
                    }

                }


                if (l_deleteFlag) {
                    removeRoomFromList(l_potentials, l_bookingModel.getCla_room().getCla_roomId());
                }


            }
            return l_potentials;


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    private void removeRoomFromList(List<RoomModel> l_list, int p_id) {
        RoomModel l_remove = null;
        for (RoomModel l_model : l_list) {

            if (l_model.getCla_roomId() == p_id) {
                l_remove = l_model;
            }
            ;
        }

        if (l_remove != null) {

            l_list.remove(l_remove);
        }
    }

    public RoomModel getRoomById(int p_id) throws Exception {
        Dao<RoomModel, Integer> dao = new RoomModel().getDao();
        RoomModel l_model = dao.queryForId(p_id);

        dao.refresh(l_model);
        return l_model;
    }


    /**
     * Link:
     * /api/room/possibleseatings/:id
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String possibleseatings(Request p_req, Response p_res, int p_id) {

        RoomSeatingController l_rsc = new RoomSeatingController();
        try {
            RoomModel l_roomModel = getRoomById(p_id);
            if (l_roomModel != null) {
                List<SeatingModel> l_list = l_rsc.getSeatingsForRoom(l_roomModel);
                return new JsonService().parseToJson(l_list);
            } else {
                p_res.status(412);
                p_res.body("An invalid id was sent");
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            p_res.status(500);
            return "";
        }

    }


    /**
     * Link:
     * /api/room/possibleseatings/:id
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String fixedequipments(Request p_req, Response p_res, int p_id) {
        Dao<RoomModel, Integer> l_daoRoom = new RoomModel().getDao();

        try {
            RoomModel l_room = l_daoRoom.queryForId(p_id);
            if (l_room == null)
                return MessageResponseService.makeMessage(p_res, 412, "Der übergebene Raum existiert nicht");

            RoomEquipmentController l_rec = new RoomEquipmentController();
            List<FixEquipmentModel> l_list = l_rec.getFixEquipmentForRoom(l_room);

            JsonService l_jsonService = new JsonService();
            return l_jsonService.parseToJson(l_list);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein interner Fehler aufgetreten");

        }

    }




}
