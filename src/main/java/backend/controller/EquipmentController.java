package backend.controller;

import backend.controller.help.BookingEquipmentController;
import backend.model.help.AdditionalEquipmentModel;
import backend.model.help.FixEquipmentModel;
import backend.service.JsonService;
import backend.service.ValidateService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.google.gson.JsonElement;
import com.j256.ormlite.dao.Dao;
import spark.Request;
import spark.Response;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by patrick on 25.01.16.
 */
public class EquipmentController {

    public EquipmentController() { }

    /**
     * Link:
     * /api/equipment/listadditionalequipment
     *
     *Beschreibung:
     *gibt alle aktiven Zusatzausstattungen zurück
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String listadditionalequipment(Request p_req, Response p_res) {
        Map<String, Object> l_query = new HashMap<>();
        l_query.put(AdditionalEquipmentModel.CN_ACTIVE, true);

        Dao<AdditionalEquipmentModel, Integer> l_dao = new AdditionalEquipmentModel().getDao();
        try {
            List<AdditionalEquipmentModel> l_adEquipment = l_dao.queryForFieldValues(l_query);
            for (AdditionalEquipmentModel l_tmp : l_adEquipment)
                l_dao.refresh(l_tmp);


            JsonService l_json = new JsonService();
            return l_json.parseToJson(l_adEquipment);

        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein interner Fehler aufgetreten");

        }
    }

    /**
     * Link:
     * /api/equipment/createadditionalequipment
     *
     *Beschreibung:
     *legt eine neue Zusatzausstattungen an
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String createadditionalequipment(Request p_req, Response p_res) {
        String[] l_jsonFields = new String[]{
                "description",
                "stock"
        };


        JsonService l_jService = new JsonService();
        ValidateService l_validate = new ValidateService();

        Map<String, JsonElement> l_json = l_jService.verifyIncomeingJsons(p_req, p_res, l_jsonFields);

        if (l_json == null)
            return "";


        int l_stock = -1;
        try {
            l_stock = Integer.valueOf(l_jService.getValueFromJsonElement(l_json.get("stock")));
        } catch (Exception e) {
            MessageResponseService.makeMessage(p_res, 412, "Die Anzahl der Zusatzausstattung muss numerisch sein");
            return "";
        }

        if (l_stock == 0) {
            MessageResponseService.makeMessage(p_res, 412, "Die Anzahl der Zusatzausstattung muss mindestens 1 sein");
            return "";
        }

        String l_description = l_jService.getValueFromJsonElement(l_json.get("description"));
        if (l_description == null || l_description.isEmpty()) {
            MessageResponseService.makeMessage(p_res, 412, "Die Beschreibung der Zusatzausstattung darf nicht leer sein");
            return "";
        }

        AdditionalEquipmentModel l_newAdEquipment = new AdditionalEquipmentModel();
        l_newAdEquipment.setCla_description(l_description);
        l_newAdEquipment.setCla_stock(l_stock);
        l_newAdEquipment.setCla_active(true);

        Dao<AdditionalEquipmentModel, Integer> l_dao = l_newAdEquipment.getDao();

        try {
            l_dao.create(l_newAdEquipment);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
        }


        return MessageResponseService.makeMessage(p_res, 200, "Die Zusatzausstattung wurde erfolgreich angelegt");
    }

    /**
     * Link:
     * /api/equipment/editadditionalequipment/:id
     *
     *Beschreibung:
     *ändert die Zusatzausstattung mit der id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String editadditionalequipment(Request p_req, Response p_res, int p_id) {
        String[] l_jsonFields = new String[]{
                "description",
                "stock"
        };

        JsonService l_jService = new JsonService();
        ValidateService l_validate = new ValidateService();
        Dao<AdditionalEquipmentModel, Integer> l_dao = new AdditionalEquipmentModel().getDao();

        Map<String, JsonElement> l_json = l_jService.verifyIncomeingJsons(p_req, p_res, l_jsonFields);

        if (l_json == null)
            return "";


        int l_stock = 0;
        try {
            l_stock = Integer.valueOf(l_jService.getValueFromJsonElement(l_json.get("stock")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 412, "Die Anzahl der Zusatzausstattung muss numerisch sein");
        }
        if (l_stock == 0) {
            MessageResponseService.makeMessage(p_res, 412, "Die Anzahl der Zusatzausstattung muss mindestens 1 sein");
            return "";
        }

        try {
            BookingEquipmentController l_bec = new BookingEquipmentController();
            int l_minStock = l_bec.getMinStock(p_id, new Date());
            System.out.println(l_minStock);
            if(l_stock < l_minStock) {
                MessageResponseService.makeMessage(p_res, 412, "Die Anzahl der Zusatzausstattung darf nicht kleiner als " + l_minStock + " sein, da diese Anzahl in zukünftigen Buchungen parallel benötigt wird");
                return "";
            }
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }


        String l_description = l_jService.getValueFromJsonElement(l_json.get("description"));
        if (l_description == null || l_description.isEmpty()) {
            MessageResponseService.makeMessage(p_res, 412, "Die Beschreibung der Zusatzausstattung darf nicht leer sein");
            return "";
        }

        AdditionalEquipmentModel l_adEquipment = null;
        try {
            l_adEquipment = l_dao.queryForId(p_id);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }

        if (l_adEquipment == null)
            return MessageResponseService.makeMessage(p_res, 412, "Die angefragte Zusatzausstattung existiert nicht");

        try {
            l_adEquipment.setCla_description(l_description);
            l_adEquipment.setCla_stock(l_stock);
            l_dao.update(l_adEquipment);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
        }

        return MessageResponseService.makeMessage(p_res, 200, "Die Zusatzausstattung wurde erfolgreich geändert");
    }

    /**
     * Link:
     * /api/equipment/editadditionalequipment/:id
     *
     *Beschreibung:
     *deaktiviert die Zusatzausstattung mit der id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String deactivateadditionalequipment(Request p_req, Response p_res, int p_id) {

        Dao<AdditionalEquipmentModel, Integer> l_dao = new AdditionalEquipmentModel().getDao();

        try {
            BookingEquipmentController l_bec = new BookingEquipmentController();
            int l_minStock = l_bec.getMinStock(p_id, new Date());
            if(l_minStock != 0) {
                MessageResponseService.makeMessage(p_res, 412, "Die Anzahl der Zusatzausstattung kann nicht gelöscht werden, weil sie in zukünftigen Buchungen benötigt wird");
                return "";
            }
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }


        AdditionalEquipmentModel l_adEquipment = null;
        try {
            l_adEquipment = l_dao.queryForId(p_id);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }

        if (l_adEquipment == null)
            return MessageResponseService.makeMessage(p_res, 412, "Die angefragte Zusatzausstattung existiert nicht");

        try {
            l_adEquipment.setCla_active(false);
            l_dao.update(l_adEquipment);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
        }

        return MessageResponseService.makeMessage(p_res, 200, "Die Zusatzausstattung wurde erfolgreich gelöscht");
    }


    /**
     * Link:
     * /api/equipment/listadditionalequipment
     *
     *Beschreibung:
     *gibt alle aktiven Raumausstattungen zurück
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String listroomequipment(Request p_req, Response p_res) {

        Map<String, Object> l_query = new HashMap<>();
        l_query.put(FixEquipmentModel.CN_ACTIVE, true);

        Dao<FixEquipmentModel, Integer> l_dao = new FixEquipmentModel().getDao();
        try {
            List<FixEquipmentModel> l_roomEquipment = l_dao.queryForFieldValues(l_query);
            for (FixEquipmentModel l_tmp : l_roomEquipment)
                l_dao.refresh(l_tmp);


            JsonService l_json = new JsonService();
            return l_json.parseToJson(l_roomEquipment);

        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein interner Fehler aufgetreten");

        }
    }

    /**
     * Link:
     * /api/equipment/createroomequipment
     *
     *Beschreibung:
     *legt eine neue Raumausstattung an
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String createroomequipment(Request p_req, Response p_res) {
        String[] l_jsonFields = new String[]{
                "description"
        };


        JsonService l_jService = new JsonService();
        ValidateService l_validate = new ValidateService();

        Map<String, JsonElement> l_json = l_jService.verifyIncomeingJsons(p_req, p_res, l_jsonFields);

        if (l_json == null)
            return "";

        String l_description = l_jService.getValueFromJsonElement(l_json.get("description"));
        if (l_description == null || l_description.isEmpty()) {
            MessageResponseService.makeMessage(p_res, 412, "Die Beschreibung der Raumausstattung darf nicht leer sein");
            return "";
        }

        FixEquipmentModel l_newFixEquipment = new FixEquipmentModel();
        l_newFixEquipment.setCla_description(l_description);
        l_newFixEquipment.setCla_active(true);

        Dao<FixEquipmentModel, Integer> l_dao = l_newFixEquipment.getDao();

        try {
            l_dao.create(l_newFixEquipment);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
        }


        return MessageResponseService.makeMessage(p_res, 200, "Die Raumausstattung wurde erfolgreich angelegt");
    }

    /**
     * Link:
     * /api/equipment/editroomequipment/:id
     *
     *Beschreibung:
     *ändert die Raumausstattung mit der id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String editroomequipment(Request p_req, Response p_res, int p_id) {
        String[] l_jsonFields = new String[]{
                "description"
        };

        JsonService l_jService = new JsonService();
        ValidateService l_validate = new ValidateService();
        Dao<FixEquipmentModel, Integer> l_dao = new FixEquipmentModel().getDao();

        Map<String, JsonElement> l_json = l_jService.verifyIncomeingJsons(p_req, p_res, l_jsonFields);

        if (l_json == null)
            return "";

        String l_description = l_jService.getValueFromJsonElement(l_json.get("description"));
        if (l_description == null || l_description.isEmpty()) {
            MessageResponseService.makeMessage(p_res, 412, "Die Beschreibung der Raumausstattung darf nicht leer sein");
            return "";
        }


        FixEquipmentModel l_fixEquipment = null;
        try {
            l_fixEquipment = l_dao.queryForId(p_id);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }

        if (l_fixEquipment == null)
            return MessageResponseService.makeMessage(p_res, 412, "Die angefragte Raumausstattung existiert nicht");

        try {
            l_fixEquipment.setCla_description(l_description);
            l_dao.update(l_fixEquipment);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
        }

        return MessageResponseService.makeMessage(p_res, 200, "Die Raumausstattung wurde erfolgreich geändert");
    }

    /**
     * Link:
     * /api/equipment/deactivateroomequipment/:id
     *
     *Beschreibung:
     *deaktiviert die Raumausstattung mit der id
     */
    @Route(type = RouteType.POST, requiredRole = 2, requiresLogin = true)
    public String deactivateroomequipment(Request p_req, Response p_res, int p_id) {

        Dao<FixEquipmentModel, Integer> l_dao = new FixEquipmentModel().getDao();

        FixEquipmentModel l_fixEquipment = null;
        try {
            l_fixEquipment = l_dao.queryForId(p_id);
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res, 500, "Bei der Bearbeitung ist ein interner Fehler aufgetreten");
        }

        if (l_fixEquipment == null)
            return MessageResponseService.makeMessage(p_res, 412, "Die angefragte Raumausstattung existiert nicht");

        try {
            l_fixEquipment.setCla_active(false);
            l_dao.update(l_fixEquipment);

        } catch (Exception e) {
            e.printStackTrace();
            return MessageResponseService.makeMessage(p_res, 500, "Es ist ein Fehler mit der Datenbank aufgetreten");
        }

        return MessageResponseService.makeMessage(p_res, 200, "Die Raumausstattung wurde erfolgreich gelöscht");
    }

}
