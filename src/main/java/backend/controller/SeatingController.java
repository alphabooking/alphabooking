package backend.controller;

import backend.model.main.SeatingModel;
import backend.service.JsonService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.j256.ormlite.dao.Dao;
import java.util.List;
import spark.Request;
import spark.Response;

/**
 * Created by Felix on 07.01.16.
 */
public class SeatingController {


    public SeatingModel getSeatingById(int p_id) throws Exception{
        Dao<SeatingModel, Integer> l_dao = new SeatingModel().getDao();
        SeatingModel l_model = l_dao.queryForId(p_id);
        l_dao.refresh(l_model);

        return l_model;

    }

    /**
     * Link:
     * /api/seating/getlist
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String getlist(Request p_req, Response p_res){


        List<SeatingModel> l_list = this.getALL();
        if(l_list != null)
           return new JsonService().parseToJson(l_list);
        else
            return MessageResponseService.makeMessage(p_res,500,"Ein interner Fehler ist aufgetreten");


    }


    public List<SeatingModel> getALL(){
        Dao<SeatingModel, Integer> l_dao = new SeatingModel().getDao();
        try {
            List<SeatingModel> l_list = l_dao.queryForAll();
            for(SeatingModel l_temp : l_list)
                l_dao.refresh(l_temp);

            return l_list;

        }catch(Exception e){

            return null;
        }

    }


}
