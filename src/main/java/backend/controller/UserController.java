package backend.controller;


import backend.controller.help.UserRoleController;
import backend.model.help.UserRoleModel;
import backend.model.main.RoleModel;
import backend.model.main.UserModel;
import backend.service.JsonService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class UserController {


    public UserController() {

    }

    /**
     * Link:
     * /api/user/changepassword
     */
    @Route(type = RouteType.POST, requiredRole = 1 ,requiresLogin = true)
    public String changepassword (Request p_req, Response p_res){
        final String [] l_fields = new String[]{
                "password_old",
                "password_new",};


        JsonService l_jservice = new JsonService();

        Map<String, JsonElement> l_map = l_jservice.verifyIncomeingJsons(p_req,p_res, l_fields);
        if(l_map == null)
            return "" ;

        UserModel l_user = p_req.session().attribute("User");

        String l_passwordOld = l_jservice.getValueFromJsonElement(l_map.get("password_old"));
        String l_passwordNew = l_jservice.getValueFromJsonElement(l_map.get("password_new"));
        if(l_user.getCla_password().equals(l_passwordOld)){
            l_user.setCla_password(l_passwordNew);
            Dao<UserModel, Integer> l_dao = l_user.getDao();
            try {
                l_dao.update(l_user);
                return MessageResponseService.makeMessage(p_res,200,"Ihr Passwort wurde erfolgreich geändert");
            }catch(Exception e){
               return MessageResponseService.makeMessage(p_res,500,"Das Passwort konnte aufgrund eines internen Fehlers nicht geöndert werden");
            }

        }else {
            return MessageResponseService.makeMessage(p_res,412,"Ihr eingegebenes Passwort ist nicht korrekt");
        }

    }


    /**
     * Link:
     * /api/user/info
     */
    @Route(type = RouteType.GET, requiredRole = 1 ,requiresLogin = true)
    public String info (Request p_req, Response p_res){
        UserModel l_model = p_req.session().attribute("User");
        return new JsonService().parseToJson(l_model);
    }


    /**
     * Link:
     * /api/user/logout
     */
    @Route(type = RouteType.GET, requiredRole = 0, requiresLogin = true)
    public String logout(Request p_req, Response p_res) {
        p_req.session().invalidate();
        return "{\"loggedOut\":\"true\"}";
    }

    /**
     * Link:
     * /api/user/hasrole
     */
    @Route(type = RouteType.GET, requiredRole = 0, requiresLogin = true)
    public String hasrole(Request p_req, Response p_res, int id) {
        int[] l_roles = p_req.session().attribute("Roles");

        for (int l_roleid : l_roles) {
            if (id == l_roleid) {
                return "{\"hasRole\":\"true\"}";
            }
        }
        return "{\"hasRole\":\"false\"}";
    }


    /**
     * Link:
     * /api/user/getroles
     */
    @Route(type = RouteType.GET, requiredRole = 0 , requiresLogin = true)
    public String getroles(Request p_req, Response p_res){
        UserRoleController l_urc = new UserRoleController();
        try {
            List<RoleModel> l_list = l_urc.getRolesForUser(p_req.session().attribute("User"));

            return new JsonService().parseToJson(l_list);


        }catch(Exception e){
            e.printStackTrace();
            return "";
        }

    }

    /**
     * Link:
     * /api/user/login
     */
    @Route(type = RouteType.POST, requiredRole = 0, requiresLogin = false)
    public String login(Request p_request, Response p_response) {

        String username = "empty";
        String password = "empty";


        JsonService jservice = new JsonService();


        JsonObject jsonObject = jservice.parseFromJson(p_request);


        if (jsonObject != null) {

            JsonElement l_temp = jsonObject.get("data");

            if (l_temp.isJsonObject() && l_temp != null) {


                jsonObject = (JsonObject) l_temp;
                l_temp = jsonObject.get("email");

                if (l_temp != null)
                    username = l_temp.toString().replace("\"", "");

                l_temp = jsonObject.get("password");

                if (l_temp != null)
                    password = l_temp.toString().replace("\"", "");
            }
        }
        Dao<UserModel, Integer> dao = new UserModel().getDao();

        List<UserModel> l_list = new ArrayList<UserModel>();
        try {
            l_list = dao.queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean l_credentialsCorrect = false;
        for (UserModel u : l_list) {

            if (u.getCla_mail().equals(username)) {
                if (u.getCla_password().equals(password)) {
                    l_credentialsCorrect = true;


                    try {
                        dao.refresh(u);
                        p_request.session().attribute("User", u);
                        new UserRoleController().copyRolesToSession(p_request, u);
                    } catch (Exception e) {
                        e.printStackTrace();
                        p_response.status(500);
                    }

                }
            }
        }

        if (!l_credentialsCorrect) {
            p_response.status(401);
            return "{\"status\": \"false\"}";
        }

        UUID l_id = UUID.randomUUID();
        p_request.session().attribute("X-Auth-Token", l_id.toString());
        p_response.header("X-Auth-Token", l_id.toString());


        return "{\"status\": \"OK\"}";

    }


    /**
     * Link:
     * /api/user/isloggedin
     */
    @Route(type = RouteType.GET, requiredRole = 0, requiresLogin = false)
    public String isloggedin(Request p_request, Response response) {
        String tokenReceived = p_request.headers("X-Auth-Token");
        String tokenSession = p_request.session().attribute("X-Auth-Token");

        if (tokenSession != null) {
            if(tokenReceived != null) {
                if (tokenReceived.equals(tokenSession)) {
                    return "{\"loggedIn\": \"true\"}";
                }
            }


        }

        response.status(401);
        return "{\"loggedIn\": \"false\"}";
    }

}
