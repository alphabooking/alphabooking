package backend.controller;

import backend.controller.help.AdditionalEquipmentController;
import backend.controller.help.FixedEquipmentController;
import backend.model.main.CateringModel;
import backend.service.JsonService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 16.01.16.
 */
public class InitController {



    /**
     * Link:
     * /api/init/init
     */
    @Route(type = RouteType.GET, requiredRole = 0, requiresLogin = false)
    public String init(Request p_req, Response p_res){
        Map<String, Object>  l_map = new HashMap<>();

        FixedEquipmentController l_feC = new FixedEquipmentController();
        AdditionalEquipmentController l_aeC = new AdditionalEquipmentController();
        CateringController l_caC = new CateringController();
        SeatingController l_seC = new SeatingController();
        StatusController l_stC = new StatusController();


        l_map.put("fixedEquipment", l_feC.getALL());
        l_map.put("additionalEquipment", l_aeC.getALL());
        l_map.put("catering", l_caC.getAll());
        l_map.put("seating", l_seC.getALL());
        l_map.put("status", l_stC.getALL());



        List<Object> l_list = new ArrayList<Object>(l_map.values());

        JsonService l_jservice = new JsonService();



        return l_jservice.parseToJson(l_map);



    }

}
