package backend.controller;

import backend.model.main.BookingModel;
import backend.model.main.ReportModel;
import backend.model.main.RoomModel;
import backend.model.main.StatusModel;

import backend.service.JsonService;
import backend.service.message.MessageResponseService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by patrick on 08.01.16.
 */
public class ReportController {

    private SimpleDateFormat cla_dateTimeFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm",Locale.GERMANY);
    private SimpleDateFormat cla_dateFormatter = new SimpleDateFormat("dd.MM.yyyy",Locale.GERMANY);
    private SimpleDateFormat cla_monthFormatter = new SimpleDateFormat("MMMM",Locale.GERMANY);
    String beginTime = " 00:00";
    String endTime = " 23:59";

    public ReportController() { }


    /**
     * Liefert ein JSON mit ReportModels den Top 3 Räumen mit den meisten Buchungen.
     *
     * @param p_req { startTime ; endTime }
     * @return  JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getcounttopthreerooms(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        Dao<BookingModel, Object>  l_dao= new BookingModel().getDao();
        QueryBuilder<BookingModel, Object> l_qb = l_dao.queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> tmp_resultList = new ArrayList<>();
        ArrayList<ReportModel> l_resultList = new ArrayList<>();

        Calendar l_calendar = GregorianCalendar.getInstance(Locale.GERMANY);
        int l_currentYear = l_calendar.get(Calendar.YEAR);

        Date startDate= cla_dateTimeFormatter.parse("01.01."+l_currentYear+beginTime);
        Date endDate=cla_dateTimeFormatter.parse("31.12."+l_currentYear+endTime);


        l_where.ge("startTime",startDate)
                .and()
                .le("endTime",endDate);


        List<BookingModel> l_bookingList = l_dao.query(l_qb.prepare());

        List<RoomModel> l_roomList = new RoomModel().getDao().queryForAll();

        for(RoomModel rmodel : l_roomList) {
            int counter = 0;
            for(BookingModel bmodel : l_bookingList){
                if(bmodel.getCla_room().getCla_roomId() == rmodel.getCla_roomId()) {
                    counter++;
                }

            }
            ReportModel l_rpmodel = new ReportModel(rmodel.getCla_roomId(),rmodel.getCla_name(), counter);
            tmp_resultList.add(l_rpmodel);
        }


        HashMap map = new HashMap();
        int i = 0;
        for (ReportModel tmp_rmodel : tmp_resultList) {
            map.put(i, tmp_rmodel.getValue());
            i++;
        }


        List list = new LinkedList(map.entrySet());

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        i = 0;

        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            l_resultList.add(tmp_resultList.get((int)entry.getKey()));

            if (i >= 2) {
                break;
            }
            i++;
        }


        return l_jservice.parseToJson(l_resultList);
    }



    /**
     * Liefert ein JSON mit ReportModels mit der Anzahl der Buchungen für alle Räume.
     *
     * @param p_req { startTime ; endTime }
     * @return  JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getcountallrooms(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> l_resultList = new ArrayList<>();

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");


        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }

        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }

        l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate)
        );

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }



        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());

        List<RoomModel> l_roomList = new RoomModel().getDao().queryForAll();

        for(RoomModel rmodel : l_roomList) {
            int counter = 0;
            for(BookingModel bmodel : l_bookingList){
                if(bmodel.getCla_room().getCla_roomId() == rmodel.getCla_roomId()) {
                    counter++;
                }

            }
            ReportModel l_rpmodel = new ReportModel(rmodel.getCla_roomId(),rmodel.getCla_name(), counter);
            l_resultList.add(l_rpmodel);
        }

        return l_jservice.parseToJson(l_resultList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit der durchschnittlichen Anzahl der Buchungen für alle Räume.
     *
     * @param p_req { startTime ; endTime }
     * @return  JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getavgcountallrooms(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> l_resultList = new ArrayList<>();

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");



        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }

        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }

        l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate)
        );

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());

        List<RoomModel> l_roomList = new RoomModel().getDao().queryForAll();

        long dateDiff = (p_eDate.getTime() - p_sDate.getTime())/(24 * 60 * 60 * 1000);

        for(RoomModel rmodel : l_roomList) {
            int counter = 0;
            for(BookingModel bmodel : l_bookingList){
                if(bmodel.getCla_room().getCla_roomId() == rmodel.getCla_roomId()) {
                    counter++;
                }

            }

            float rcounter = (float) counter/ (float) dateDiff;

            ReportModel l_rpmodel = new ReportModel(rmodel.getCla_roomId(),rmodel.getCla_name(), rcounter);
            l_resultList.add(l_rpmodel);
        }

        return l_jservice.parseToJson(l_resultList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit den gebuchten Stunden der Buchungen für alle Räume.
     *
     * @param p_req { startTime ; endTime }
     * @return  JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String gethoursallrooms(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> l_resultList = new ArrayList<>();


        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");



        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }



        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }

        l_where.or(
                l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate)
                ),

                l_where.and(
                        l_where.ge("endTime",p_sDate),
                        l_where.le("endTime",p_eDate)
                ),

                l_where.and(
                        l_where.le("startTime",p_sDate),
                        l_where.ge("endTime",p_eDate)
                ),

                l_where.eq("startTime",p_sDate),


                l_where.eq("endTime",p_eDate)
                );

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());

        List<RoomModel> l_roomList = new RoomModel().getDao().queryForAll();

        for(RoomModel rmodel : l_roomList) {

            float durationCounter = 0;
            float duration = 0;
            for(BookingModel bmodel : l_bookingList){

                if(bmodel.getCla_room().getCla_roomId() == rmodel.getCla_roomId()) {
                    duration = (float) ( bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);

                    if(bmodel.getCla_beginDate().before(p_sDate) && bmodel.getCla_endDate().after(p_eDate) ) {
                        float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                        temp_duration += (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                        duration = duration - temp_duration;
                    } else if(bmodel.getCla_beginDate().before(p_sDate)) {
                        float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                        duration = duration - temp_duration;
                    } else if (bmodel.getCla_endDate().after(p_eDate)) {
                        float temp_duration = (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                        duration = duration - temp_duration;
                    }
                }
                durationCounter += duration;

            }
            ReportModel l_rpmodel = new ReportModel(rmodel.getCla_roomId(),rmodel.getCla_name(), durationCounter);
            l_resultList.add(l_rpmodel);
        }

        return l_jservice.parseToJson(l_resultList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit den durchschnittlich gebuchten Stunden der Buchungen für alle Räume.
     *
     * @param p_req { startTime ; endTime }
     * @return  JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getavghoursallrooms(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> l_resultList = new ArrayList<>();

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");


        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }


        if(p_eDate.before(p_eDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }

        l_where.or(
                l_where.and(
                        l_where.ge("startTime",p_sDate),
                        l_where.le("startTime",p_eDate)
                ),

                l_where.and(
                        l_where.ge("endTime",p_sDate),
                        l_where.le("endTime",p_eDate)
                ),

                l_where.and(
                        l_where.le("startTime",p_sDate),
                        l_where.ge("endTime",p_eDate)
                ),

                l_where.eq("startTime",p_sDate),


                l_where.eq("endTime",p_eDate)
        );

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());

        List<RoomModel> l_roomList = new RoomModel().getDao().queryForAll();

        long dateDiff = (p_eDate.getTime() - p_sDate.getTime())/(24 * 60 * 60 * 1000);




        for(RoomModel rmodel : l_roomList) {
            float durationCounter = 0;
            for(BookingModel bmodel : l_bookingList){
                float duration = 0;
                if(bmodel.getCla_room().getCla_roomId() == rmodel.getCla_roomId()) {

                    duration = (float) ( bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);

                    if(bmodel.getCla_beginDate().before(p_sDate) && bmodel.getCla_endDate().after(p_eDate) ) {
                        float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                        temp_duration += (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                        duration = duration - temp_duration;
                    } else if(bmodel.getCla_beginDate().before(p_sDate)) {
                        float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                        duration = duration - temp_duration;
                    } else if (bmodel.getCla_endDate().after(p_eDate)) {
                        float temp_duration = (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                        duration = duration - temp_duration;
                    }
                } else {
                    durationCounter = 0;
                }
                durationCounter += duration;
            }
            durationCounter = durationCounter / (float) dateDiff;
            ReportModel l_rpmodel = new ReportModel(rmodel.getCla_roomId(),rmodel.getCla_name(), durationCounter);
            l_resultList.add(l_rpmodel);

        }

        return l_jservice.parseToJson(l_resultList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit der Anzahl der Buchungen je nach Status für alle Räume.
     *
     * @param p_req { startTime ; endTime }
     * @return  JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getstatuscountsallrooms(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> l_resultList = new ArrayList<>();

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }
        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");



        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }

        l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("endTime",p_eDate)
        );

        l_where.or(
                l_where.and(
                        l_where.ge("startTime",p_sDate),
                        l_where.le("startTime",p_eDate)
                ),

                l_where.and(
                        l_where.ge("endTime",p_sDate),
                        l_where.le("endTime",p_eDate)
                ),

                l_where.and(
                        l_where.le("startTime",p_sDate),
                        l_where.ge("endTime",p_eDate)
                ),

                l_where.eq("startTime",p_sDate),


                l_where.eq("endTime",p_eDate)
        );

        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());
        List<RoomModel> l_roomList = new RoomModel().getDao().queryForAll();
        List<StatusModel> l_statusList = new StatusModel().getDao().queryForAll();

        for (StatusModel smodel : l_statusList){
            int counter = 0;

            for(BookingModel bmodel : l_bookingList){
                if(bmodel.getCla_status().getId() == smodel.getId()) {
                    counter++;
                }
            }
            ReportModel l_rpmodel = new ReportModel(smodel.getId(),smodel.getDescription(), counter);
            l_resultList.add(l_rpmodel);
        }

        return l_jservice.parseToJson(l_resultList);
    }




    /**
     * Liefert ein JSON mit ReportModels mit der Anzahl der Buchungen für einen bestimmten Raum.
     *
     * @param p_req { startTime ; endTime ; roomId }
     * @return JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getcountsingleroom(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        ArrayList<ReportModel> l_resultList = new ArrayList<>();


        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");


        int roomId = 0;
        try {
            roomId = Integer.parseInt(l_map.value("roomId"));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Der Raum darf nicht leer sein");
        }


        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }

        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }


        l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
        );


        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());

        if(l_bookingList.isEmpty()){
            RoomModel l_rmodel = (RoomModel) new RoomModel().getDao().queryForId(roomId);
            l_resultList.add(new ReportModel(roomId, l_rmodel.getCla_name(), l_bookingList.size()));
        } else {
            l_resultList.add(new ReportModel(l_bookingList.get(0).getCla_bookingId(), l_bookingList.get(0).getCla_room().getCla_name(), l_bookingList.size()));
        }

        return l_jservice.parseToJson(l_resultList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit den gebuchten Stunden für einen bestimmten Raum.
     *
     * @param p_req { startTime ; endTime ; roomId }
     * @return JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String gethourssingleroom(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        ArrayList<ReportModel> l_rList= new ArrayList<>();
        float duration = 0;

        QueryParamsMap l_map = p_req.queryMap();

        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");

        int roomId;
        try {
            roomId = Integer.parseInt(l_map.value("roomId"));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Der Raum darf nicht leer sein");
        }


        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }



        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }



        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();

        l_where.or(l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.ge("endTime",p_sDate),
                        l_where.le("endTime",p_eDate),
                        l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.le("startTime",p_sDate),
                        l_where.ge("endTime",p_eDate),
                        l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.eq("startTime",p_sDate),
                        l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.eq("endTime",p_eDate),
                        l_where.eq("Room_idRoom",roomId)
                ));

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> l_bookingList = new BookingModel().getDao().query(l_qb.prepare());


        if(l_bookingList.isEmpty()) {
            RoomModel l_rmodel = (RoomModel) new RoomModel().getDao().queryForId(roomId);
            l_rList.add(new ReportModel(roomId, l_rmodel.getCla_name(), l_bookingList.size()));
        } else {
            float durationCounter = 0;
            for (BookingModel bmodel : l_bookingList) {

                duration = (float) ( bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);

                if(bmodel.getCla_beginDate().before(p_sDate) && bmodel.getCla_endDate().after(p_eDate) ) {
                    float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                    temp_duration += (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                    duration = duration - temp_duration;
                } else if(bmodel.getCla_beginDate().before(p_sDate)) {
                    float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                    duration = duration - temp_duration;
                } else if (bmodel.getCla_endDate().after(p_eDate)) {
                    float temp_duration = (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                    duration = duration - temp_duration;
                }

                durationCounter += duration;
            }
            l_rList.add(new ReportModel(roomId, l_bookingList.get(0).getCla_room().getCla_name(), durationCounter));
        }

        return l_jservice.parseToJson(l_rList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit den monatlich gebuchten Stunden für einen bestimmten Raum.
     *
     * @param p_req { startTime ; endTime ; roomId }
     * @return JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getmonthhourssingleroom(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        ArrayList<ReportModel> l_rList= new ArrayList<>();
        float duration = 0;

        QueryParamsMap l_map = p_req.queryMap();

        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }

        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");

        int roomId;
        try {
            roomId = Integer.parseInt(l_map.value("roomId"));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Der Raum darf nicht leer sein");
        }


        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }


        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }



        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();
        l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("endTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
        );

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> bookingList = new BookingModel().getDao().query(l_qb.prepare());


        Calendar l_calendar = Calendar.getInstance();
        l_calendar.setTime(p_sDate);

        if(bookingList.isEmpty()) {
            RoomModel l_rmodel = (RoomModel) new RoomModel().getDao().queryForId(roomId);
            l_rList.add(new ReportModel(roomId, l_rmodel.getCla_name(), bookingList.size()));
        } else {

            for (int i = 0; i <= (p_eDate.getMonth() - p_sDate.getMonth()); i++) {
                duration = 0;
                for (BookingModel bmodel : bookingList) {
                    if (cla_monthFormatter.format(bmodel.getCla_beginDate()).equals(cla_monthFormatter.format(l_calendar.getTime()))) {
                        long timeDiff = bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime();
                        duration += (float) timeDiff / (60 * 60 * 1000);
                    }
                }
                l_rList.add(new ReportModel(roomId, cla_monthFormatter.format(l_calendar.getTime()), duration));
                l_calendar.add(Calendar.MONTH, 1);
            }

        }

        return l_jservice.parseToJson(l_rList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit den durchschnittlich gebuchten Stunden für einen bestimmten Raum.
     *
     * @param p_req { startTime ; endTime ; roomId }
     * @return JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getavghourssingleroom(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        ArrayList<ReportModel> l_rList= new ArrayList<>();
        float duration = 0;

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }
        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");

        int roomId = 0;
        try {
            roomId = Integer.parseInt(l_map.value("roomId"));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Der Raum darf nicht leer sein");
        }


        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }


        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }


        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();
        l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("endTime",p_eDate),
                l_where.eq("Room_idRoom", roomId)
        );

        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> bookingList = new BookingModel().getDao().query(l_qb.prepare());



        long dateDiff = (p_eDate.getTime() - p_sDate.getTime())/(24 * 60 * 60 * 1000);


        if(bookingList.isEmpty()) {
            RoomModel l_rmodel = (RoomModel) new RoomModel().getDao().queryForId(roomId);
            l_rList.add(new ReportModel(roomId, l_rmodel.getCla_name(), bookingList.size()));
        } else {
            float durationCounter = 0;
            for (BookingModel bmodel : bookingList) {

                duration = (float) ( bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);

                if(bmodel.getCla_beginDate().before(p_sDate) && bmodel.getCla_endDate().after(p_eDate) ) {
                    float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                    temp_duration += (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                    duration = duration - temp_duration;
                } else if(bmodel.getCla_beginDate().before(p_sDate)) {
                    float temp_duration = (float) ( p_sDate.getTime() - bmodel.getCla_beginDate().getTime() ) / (float) (60 * 60 * 1000);
                    duration = duration - temp_duration;
                } else if (bmodel.getCla_endDate().after(p_eDate)) {
                    float temp_duration = (float) ( bmodel.getCla_endDate().getTime() - p_eDate.getTime() ) / (float) (60 * 60 * 1000);
                    duration = duration - temp_duration;
                }

                durationCounter += duration;

            }
            durationCounter = durationCounter / (float) dateDiff;

            l_rList.add(new ReportModel(roomId, bookingList.get(0).getCla_room().getCla_name(), durationCounter));
        }

        return l_jservice.parseToJson(l_rList);
    }


    /**
     * Liefert ein JSON mit ReportModels mit der täglich gebuchte Stunden Auslastung für einen bestimmten Raum.
     *
     * @param p_req { startTime ; endTime ; roomId }
     * @return JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getdailyhourssingleroom(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        ArrayList<ReportModel> l_rList= new ArrayList<>();

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }
        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");

        int roomId;
        try {
            roomId = Integer.parseInt(l_map.value("roomId"));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Der Raum darf nicht leer sein");
        }

        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }

        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }



        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();


        // Buchung ist liegt zwischen Reportzeitraum
        l_where.or(l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
            ),

        l_where.and(
                l_where.ge("endTime",p_sDate),
                l_where.le("endTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
        ),

        l_where.and(
                l_where.le("startTime",p_sDate),
                l_where.ge("endTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
        ),

        l_where.and(
                l_where.eq("startTime",p_sDate),
                l_where.eq("Room_idRoom",roomId)
        ),

        l_where.and(
                l_where.eq("endTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
        ));


        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> bookingList = new BookingModel().getDao().query(l_qb.prepare());


        Calendar l_calendar = Calendar.getInstance();
        l_calendar.setTime(p_sDate);


        Map<Integer,Float> bookingMap = new HashMap<>();
        Map<String,Float> dateMap = new HashMap<>();

        int dateDiff = (int) TimeUnit.DAYS.convert((p_eDate.getTime() - p_sDate.getTime()), TimeUnit.MILLISECONDS);


        if(bookingList.isEmpty()) {
            for (int i = 0; i <= dateDiff ; i++) {
                dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), (float) 0.0);
                l_calendar.add(Calendar.DAY_OF_MONTH, 1);
            }
        } else {


            for (int i = 0; i <= dateDiff ; i++) {

                dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), ( float ) 0.0);

                for (BookingModel bmodel : bookingList) {

                    //Berechnung Buchungsgesamtdauer in Stunden
                    float duration = (float) (bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime()) / (float) (60 * 60 * 1000);

                    /*
                    System.out.println("bId: " + bmodel.getCla_bookingId());
                    System.out.println("bDate: " + cla_dateFormatter.format(bmodel.getCla_beginDate()));
                    System.out.println("eDate: " + cla_dateFormatter.format(bmodel.getCla_endDate()));
                    System.out.println("sDate: " + cla_dateFormatter.format(l_calendar.getTime()));
                    */

                    // PRÜFUNG: Buchungbeginn und Buchungsende entsprechen gesuchtem Datum
                    if (cla_dateFormatter.format(bmodel.getCla_beginDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))
                            && cla_dateFormatter.format(bmodel.getCla_endDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {


                        //System.out.println("Schleife Beginn + Ende = Datum");

                        if (dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            float temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            //System.out.println("temp_dur: " + temp_duration);
                            temp_duration += duration;
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), temp_duration);
                        } else {
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), duration);
                        }

                        // PRÜFUNG: Buchungbeginn entspricht gesuchtem Datum
                    } else if (cla_dateFormatter.format(bmodel.getCla_beginDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        //System.out.println("Schleife Beginn = Datum");

                        Calendar helpCalendar = GregorianCalendar.getInstance(Locale.GERMANY);
                        helpCalendar.setTime(cla_dateTimeFormatter.parse(cla_dateFormatter.format(bmodel.getCla_beginDate()) + beginTime));
                        helpCalendar.add(Calendar.DAY_OF_MONTH, 1);

                        float helpDuration = (float) TimeUnit.HOURS.convert((bmodel.getCla_endDate().getTime() - helpCalendar.getTime().getTime()), TimeUnit.MILLISECONDS);
                        ;
                        float dayDuration = duration - helpDuration;


                        bookingMap.put(bmodel.getCla_bookingId(), dayDuration);

                        if (dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            float temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            temp_duration += dayDuration;
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), temp_duration);
                        } else {
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), dayDuration);
                        }

                        // PRÜFUNG: Buchungende entspricht gesuchtem Datum
                    } else if (cla_dateFormatter.format(bmodel.getCla_endDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        //System.out.println("Schleife Ende = Datum");

                        float dayDuration;

                        if (bookingMap.containsKey(bmodel.getCla_bookingId())) {
                            dayDuration = duration - bookingMap.get(bmodel.getCla_bookingId());
                            bookingMap.put(bmodel.getCla_bookingId(), dayDuration);
                        } else {
                            float helpDuration = (float) TimeUnit.HOURS.convert((l_calendar.getTime().getTime() - bmodel.getCla_beginDate().getTime()), TimeUnit.MILLISECONDS);
                            ;
                            dayDuration = duration - helpDuration;
                            bookingMap.put(bmodel.getCla_bookingId(), dayDuration);
                        }
                        float temp_duration;
                        if (dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            temp_duration += dayDuration;
                        } else {
                            temp_duration = dayDuration;
                        }
                        dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), temp_duration);

                        // PRÜFUNG: Weder Buchungbeginn noch Buchungsende entsprechen gesuchtem Datum
                    } else if(bmodel.getCla_beginDate().before(l_calendar.getTime()) && bmodel.getCla_endDate().before(l_calendar.getTime())) {
                        //System.out.println("do nothing");

                    }else if(bmodel.getCla_beginDate().after(l_calendar.getTime()) && bmodel.getCla_endDate().after(l_calendar.getTime())) {
                        //System.out.println("do nothing");

                    }else if(!cla_dateFormatter.format(bmodel.getCla_beginDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))
                            && !cla_dateFormatter.format(bmodel.getCla_endDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        //System.out.println("Schleife Beginn + Ende != Datum");

                        float dayDuration = (float) 24.0;

                        float temp_duration;
                        if(bookingMap.containsKey(bmodel.getCla_bookingId())){
                            temp_duration = bookingMap.get(bmodel.getCla_bookingId());
                            temp_duration += dayDuration;
                            bookingMap.put(bmodel.getCla_bookingId(),temp_duration);
                        } else {

                            float helpDuration = (float )TimeUnit.HOURS.convert(( l_calendar.getTime().getTime() - bmodel.getCla_beginDate().getTime() ), TimeUnit.MILLISECONDS);;

                            temp_duration = dayDuration+helpDuration;
                            bookingMap.put(bmodel.getCla_bookingId(),temp_duration);
                        }



                        if(dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            temp_duration += dayDuration;
                        } else {
                            temp_duration = dayDuration;
                        }
                        dateMap.put(cla_dateFormatter.format(l_calendar.getTime()),temp_duration);

                    }

                }


                l_calendar.add(Calendar.DAY_OF_MONTH, 1);
            }


            List list = new LinkedList(dateMap.entrySet());

            Collections.sort(list, new Comparator() {
                public int compare(Object o1, Object o2) {
                    return ((Comparable) ((Map.Entry) (o1)).getKey())
                            .compareTo(((Map.Entry) (o2)).getKey());
                }
            });

            Map sortedDateMap = new LinkedHashMap();
            for (Iterator it = list.iterator(); it.hasNext();) {
                Map.Entry entry = (Map.Entry) it.next();
                sortedDateMap.put(entry.getKey(), entry.getValue());
            }

            Iterator<Map.Entry<String, Float>> entries = sortedDateMap.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Float> entry = entries.next();
                l_rList.add(new ReportModel(roomId,entry.getKey(),entry.getValue()));
            }
        }

        return l_jservice.parseToJson(l_rList);
    }

    /**
     * Liefert ein JSON mit ReportModels mit der täglich gebuchte Stunden Auslastung für einen bestimmten Raum.
     *
     * @param p_req { startTime ; endTime ; roomId }
     * @return JSON(ArrayList<ReportModel>)
     * @throws Exception
     */
    @Route(type = RouteType.GET, requiredRole = 2, requiresLogin = true)
    public String getdailyutilizationsingleroom(Request p_req, Response p_res ) throws Exception{

        JsonService l_jservice = new JsonService();
        ArrayList<ReportModel> l_rList= new ArrayList<>();

        QueryParamsMap l_map = p_req.queryMap();
        Date p_sDate = null;
        try {
            p_sDate = new Date(Long.parseLong(l_map.value("beginDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Startdatum");
        }
        Date p_eDate = null;
        try {
            p_eDate = new Date(Long.parseLong(l_map.value("endDate")));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Falsche Formatierung für Enddatum");
        }
        String p_status = l_map.value("status");
        String p_extern = l_map.value("extern");
        String p_service = l_map.value("service");

        int roomId;
        try {
            roomId = Integer.parseInt(l_map.value("roomId"));
        } catch (Exception e) {
            return MessageResponseService.makeMessage(p_res,412,"Der Raum darf nicht leer sein");
        }

        if(p_sDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Startdatum darf nicht leer sein");
        }
        if(p_eDate == null){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht leer sein");
        }

        if(p_eDate.before(p_sDate)){
            return MessageResponseService.makeMessage(p_res,412,"Das Enddatum darf nicht vor dem Startdatum liegen");
        }

        QueryBuilder<BookingModel, String> l_qb = new BookingModel().getDao().queryBuilder();
        Where l_where = l_qb.where();


        // Buchung ist liegt zwischen Reportzeitraum
        l_where.or(l_where.and(
                l_where.ge("startTime",p_sDate),
                l_where.le("startTime",p_eDate),
                l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.ge("endTime",p_sDate),
                        l_where.le("endTime",p_eDate),
                        l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.le("startTime",p_sDate),
                        l_where.ge("endTime",p_eDate),
                        l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.eq("startTime",p_sDate),
                        l_where.eq("Room_idRoom",roomId)
                ),

                l_where.and(
                        l_where.eq("endTime",p_eDate),
                        l_where.eq("Room_idRoom",roomId)
                ));


        if( p_status != null && !p_status.equals("null") ){
            l_where.and().eq("status",Integer.parseInt(p_status));
        }
        if( p_extern != null && !p_extern.equals("null") ){
            l_where.and().eq("extern",Integer.parseInt(p_extern));
        }
        if( p_service != null && !p_service.equals("null") ){
            l_where.and().eq("service",Integer.parseInt(p_service));
        }

        List<BookingModel> bookingList = new BookingModel().getDao().query(l_qb.prepare());


        Calendar l_calendar = Calendar.getInstance();
        l_calendar.setTime(p_sDate);


        Map<Integer,Float> bookingMap = new HashMap<>();
        Map<String,Float> dateMap = new HashMap<>();

        int dateDiff = (int) TimeUnit.DAYS.convert((p_eDate.getTime() - p_sDate.getTime()), TimeUnit.MILLISECONDS);

        if(bookingList.isEmpty()) {
            for (int i = 0; i <= dateDiff ; i++) {
                dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), (float) 0.0);
                l_calendar.add(Calendar.DAY_OF_MONTH, 1);
            }
        } else {
            for (int i = 0; i <= dateDiff ; i++) {
                dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), ( float ) 0.0);
                for (BookingModel bmodel : bookingList) {

                    //Berechnung Buchungsgesamtdauer in Stunden
                    float duration = (float) (bmodel.getCla_endDate().getTime() - bmodel.getCla_beginDate().getTime()) / (float) (60 * 60 * 1000);


                    // PRÜFUNG: Buchungbeginn und Buchungsende entsprechen gesuchtem Datum
                    if (cla_dateFormatter.format(bmodel.getCla_beginDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))
                            && cla_dateFormatter.format(bmodel.getCla_endDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        if (dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            float temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            //System.out.println("temp_dur: " + temp_duration);
                            temp_duration += duration;
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), temp_duration);
                        } else {
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), duration);
                        }

                        // PRÜFUNG: Buchungbeginn entspricht gesuchtem Datum
                    } else if (cla_dateFormatter.format(bmodel.getCla_beginDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        Calendar helpCalendar = GregorianCalendar.getInstance(Locale.GERMANY);
                        helpCalendar.setTime(cla_dateTimeFormatter.parse(cla_dateFormatter.format(bmodel.getCla_beginDate()) + beginTime));
                        helpCalendar.add(Calendar.DAY_OF_MONTH, 1);

                        float helpDuration = (float) TimeUnit.HOURS.convert((bmodel.getCla_endDate().getTime() - helpCalendar.getTime().getTime()), TimeUnit.MILLISECONDS);
                        ;
                        float dayDuration = duration - helpDuration;


                        bookingMap.put(bmodel.getCla_bookingId(), dayDuration);

                        if (dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            float temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            temp_duration += dayDuration;
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), temp_duration);
                        } else {
                            dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), dayDuration);
                        }

                        // PRÜFUNG: Buchungende entspricht gesuchtem Datum
                    } else if (cla_dateFormatter.format(bmodel.getCla_endDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        float dayDuration;

                        if (bookingMap.containsKey(bmodel.getCla_bookingId())) {
                            dayDuration = duration - bookingMap.get(bmodel.getCla_bookingId());
                            bookingMap.put(bmodel.getCla_bookingId(), dayDuration);
                        } else {
                            float helpDuration = (float) TimeUnit.HOURS.convert((l_calendar.getTime().getTime() - bmodel.getCla_beginDate().getTime()), TimeUnit.MILLISECONDS);
                            ;
                            dayDuration = duration - helpDuration;
                            bookingMap.put(bmodel.getCla_bookingId(), dayDuration);
                        }
                        float temp_duration;
                        if (dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            temp_duration += dayDuration;
                        } else {
                            temp_duration = dayDuration;
                        }
                        dateMap.put(cla_dateFormatter.format(l_calendar.getTime()), temp_duration);

                        // PRÜFUNG: Weder Buchungbeginn noch Buchungsende entsprechen gesuchtem Datum
                    } else if(bmodel.getCla_beginDate().before(l_calendar.getTime()) && bmodel.getCla_endDate().before(l_calendar.getTime())) {
                        if(!dateMap.containsKey(l_calendar.getTime()) ) {
                            // do nothning
                        }
                    }else if(bmodel.getCla_beginDate().after(l_calendar.getTime()) && bmodel.getCla_endDate().after(l_calendar.getTime())) {
                        // do nothing

                    }else if(!cla_dateFormatter.format(bmodel.getCla_beginDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))
                            && !cla_dateFormatter.format(bmodel.getCla_endDate()).equals(cla_dateFormatter.format(l_calendar.getTime()))) {

                        float dayDuration = (float) 24.0;

                        float temp_duration;
                        if(bookingMap.containsKey(bmodel.getCla_bookingId())){
                            temp_duration = bookingMap.get(bmodel.getCla_bookingId());
                            temp_duration += dayDuration;
                            bookingMap.put(bmodel.getCla_bookingId(),temp_duration);
                        } else {

                            float helpDuration = (float )TimeUnit.HOURS.convert(( l_calendar.getTime().getTime() - bmodel.getCla_beginDate().getTime() ), TimeUnit.MILLISECONDS);;

                            temp_duration = dayDuration+helpDuration;
                            bookingMap.put(bmodel.getCla_bookingId(),temp_duration);
                        }

                        if(dateMap.containsKey(cla_dateFormatter.format(l_calendar.getTime()))) {
                            temp_duration = dateMap.get(cla_dateFormatter.format(l_calendar.getTime()));
                            temp_duration += dayDuration;
                        } else {
                            temp_duration = dayDuration;
                        }
                        dateMap.put(cla_dateFormatter.format(l_calendar.getTime()),temp_duration);

                    }

                }

                l_calendar.add(Calendar.DAY_OF_MONTH, 1);
            }

            Map<Date, Float> tmpDateMap = new HashMap<>();

            Iterator<Map.Entry<String, Float>> entries = dateMap.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Float> entry = entries.next();
                tmpDateMap.put(cla_dateFormatter.parse(entry.getKey()), entry.getValue());
            }


            List list = new LinkedList(dateMap.entrySet());

            Collections.sort(list, new Comparator() {
                public int compare(Object o1, Object o2) {
                    return ((Comparable) ((Map.Entry) (o1)).getKey())
                            .compareTo(((Map.Entry) (o2)).getKey());
                }
            });

            Map sortedDateMap = new LinkedHashMap();
            for (Iterator it = list.iterator(); it.hasNext();) {
                Map.Entry entry = (Map.Entry) it.next();
                sortedDateMap.put(entry.getKey(), entry.getValue());
            }


            entries = sortedDateMap.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Float> entry = entries.next();
                float utilization;
                if(entry.getValue()>10.0) {
                    utilization = (float) 100.0;
                } else {
                    utilization = (entry.getValue() / 10) * 100;
                }
                l_rList.add(new ReportModel(roomId,entry.getKey(),utilization));
            }
        }

        return l_jservice.parseToJson(l_rList);
    }

}
