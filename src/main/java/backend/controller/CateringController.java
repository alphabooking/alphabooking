package backend.controller;

import backend.model.main.CateringModel;
import backend.service.JsonService;
import backend.service.routemapper.Route;
import backend.service.routemapper.RouteType;
import com.j256.ormlite.dao.Dao;
import spark.Request;
import spark.Response;

import java.util.List;

/**
 * Created by Felix on 07.01.16.
 */
public class CateringController {

    /**
     *
     * @return Liste mit allen CateringModels
     */
    public List<CateringModel> getAll(){
        Dao<CateringModel, Integer> l_dao = new CateringModel().getDao();

        try{
            List<CateringModel> l_list = l_dao.queryForAll();
            for(CateringModel l_model : l_list)
                l_dao.refresh(l_model);

            return l_list;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }

    /**
     * Link:
     * /api/catering/getlist
     *
     *Beschreibung:
     *liefert eine Liste mit allen CateringModels
     */
    @Route(type = RouteType.GET, requiredRole = 1, requiresLogin = true)
    public String getlist(Request p_req, Response p_res){

        List<CateringModel> l_list = this.getAll();
        JsonService l_jservice = new JsonService();

        return  l_jservice.parseToJson(l_list);

    }
}
