/**
 * Created by Jan on 15.01.2016.
 */
(function () {
    'use strict';
    var app = angular.module('app');

    app.service('appService', ['$rootScope', '$timeout', '$http', function ($rootScope, $timeout, $http) {
        var isStartUpFinished = false;
        var alerts = [];
        var alertID = 0;
        var previousState = null;

        var configurations = {
            fixEquipment: [],
            varEquipment: [],
            serviceTypes: [],
            seatings: [],
            bookingStatus: []
        };

        function _removeAlert(p_alertID) {
            alerts.forEach(function (item, index) {
                if (item.id === p_alertID) {
                    alerts.splice(index, 1);
                }
            });
            $rootScope.alerts = alerts;
        }

        this.loadConfig = function () {
            $http.get('/api/init/init', {
                    params: {nocache: new Date().getTime()}
                })
                .then(function success(response) {

                    if (response.data.fixedEquipment) {
                        configurations.fixEquipment = [];
                        response.data.fixedEquipment.forEach(function (item) {
                            configurations.fixEquipment.push({
                                id: item.cla_idFixEquipment,
                                description: item.cla_description
                            });
                        });
                    }

                    if (response.data.additionalEquipment) {
                        configurations.varEquipment = [];
                        response.data.additionalEquipment.forEach(function (item) {
                            configurations.varEquipment.push({
                                id: item.cla_additionalEquipmentId,
                                description: item.cla_description,
                                stock: item.cla_stock
                            });
                        });
                    }

                    if (response.data.catering) {
                        configurations.serviceTypes = [];
                        response.data.catering.forEach(function (item) {
                            configurations.serviceTypes.push(item);
                        });
                    }

                    if (response.data.seating) {
                        configurations.seatings = [];
                        response.data.seating.forEach(function (item) {
                            configurations.seatings.push({
                                id: item.cla_seatingId,
                                description: item.cla_description,
                                name: item.cla_name
                            });
                        });
                    }

                    if (response.data.status) {
                        configurations.bookingStatus = [];
                        response.data.status.forEach(function (item) {
                            configurations.bookingStatus.push(item);
                        });
                    }

                    $rootScope.$emit('CONFIG_LOADED');
                }, function failure(response) {

                });

        };

        this.getAllBookingStatus = function () {
            return angular.copy(configurations.bookingStatus);
        };

        this.getAllFixedEquipments = function () {
            return angular.copy(configurations.fixEquipment);
        };

        this.getAllAdditionalEquipments = function () {
            return angular.copy(configurations.varEquipment);
        };

        this.getAllServiceTypes = function () {
            return angular.copy(configurations.serviceTypes);
        };

        this.getAllSeatings = function () {
            return angular.copy(configurations.seatings);
        };

        this.isStartUpFinished = function () {
            return isStartUpFinished;
        };
        this.startUpHasFinished =
            function () {
                $rootScope.$emit('STARTUP_FINISHED');
                isStartUpFinished = true;
            };

        this.addAlert = function (alert) {
            alert.id = alertID;
            alert.close = function () {
                _removeAlert(alert.id);
            };
            if (alert.timeout) {
                $timeout(alert.close, alert.timeout)
            }

            alertID++;
            alerts.push(alert);
            $rootScope.alerts = alerts;

        };
        this.removeAlert = function (p_alertID) {
            _removeAlert(p_alertID);
        };

        this.previousState = function (state) {
            if (state) {
                previousState = state;
            }
            return state;
        }
    }]);
})();
