/**
 * Created by Jan on 17.01.2016.
 */
(function () {
    'use strict';

    var app = angular.module('app');

    /**
     * Formatiert Eingabe ID zu BookingStatus-String
     *
     * Wenn String nicht gefunden, dann input zurück.
     */
    app.filter('bookingStatus', ['appService', function (appService) {
        return function (input) {
            var status = appService.getAllBookingStatus();
            input = input || -1;
            var out = false;
            if (angular.isNumber(input)) {
                status.forEach(function (item) {
                    if (item.id == input) {
                        out = item.description;
                    }
                });
                if (out) {
                    return out;
                }
            }
            return input;
        }
    }]);

    /**
     * Gibt in Ampelfarben den Status einer Buchung zurück
     */
    app.filter('bookingStatusLight', [function () {
        return function (input) {
            input = input || -1;
            var out = false;
            if (angular.isNumber(input)) {
                switch (input) {
                    case 1 :
                        out = 'info';
                        break;
                    case 2 :
                        out = 'success';
                        break;
                    case 3 :
                        out = 'danger';
                        break;
                    case 4 :
                        out = 'warning';
                        break;
                    case 5 :
                        out = 'default';
                        break;
                    case 6 :
                        out = 'default';
                        break;
                    default:
                        return false;
                }
                if (out) {
                    return out;
                }
            }
            return input;
        }
    }]);

    /**
     * Formatiert Eingabe ID zu FixedEquipment-String
     *
     * Wenn String nicht gefunden, dann input zurück.
     */
    app.filter('fixedEquipment', ['appService', function (appService) {
        return function (input) {
            var status = appService.getAllFixedEquipments();
            input = input || -1;
            var out = false;
            if (angular.isNumber(input)) {
                status.forEach(function (item) {
                    if (item.id == input) {
                        out = item.description;
                    }
                });
                if (out) {
                    return out;
                }
            }
            return input;
        }
    }]);

    /**
     * Formatiert Eingabe ID zu AdditionalEquipment-String
     *
     * Wenn String nicht gefunden, dann input zurück.
     */
    app.filter('additionalEquipment', ['appService', function (appService) {
        return function (input) {
            var status = appService.getAllAdditionalEquipments();
            input = input || -1;
            var out = false;
            if (angular.isNumber(input)) {
                status.forEach(function (item) {
                    if (item.id == input) {
                        out = item.description;
                    }
                });
                if (out) {
                    return out;
                }
            }
            return input;
        }
    }]);

    /**
     * Formatiert Eingabe ID zu ServiceType-String
     *
     * Wenn String nicht gefunden, dann input zurück.
     */
    app.filter('serviceType', ['appService', function (appService) {
        return function (input) {
            var status = appService.getAllServiceTypes();
            input = input || -1;
            var out = false;
            if (angular.isNumber(input)) {
                status.forEach(function (item) {
                    if (item.id == input) {
                        out = item.description;
                    }
                });
                if (out) {
                    return out;
                }
            }
            return input;
        }
    }]);

    /**
     * Formatiert Eingabe ID zu Seating-String
     * Type gibt an ob d->Description oder n->Name (default: n)
     *
     * Wenn String nicht gefunden, dann input zurück.
     */
    app.filter('seating', ['appService', function (appService) {
        return function (input, type) {
            var status = appService.getAllSeatings();
            input = input || -1;
            type = type || 'n';

            var out = false;
            if (angular.isNumber(input)) {
                status.forEach(function (item) {
                    if (item.id == input) {
                        if (type === 'd') {
                            out = item.description;
                        } else {
                            out = item.name;
                        }
                    }
                });
                if (out) {
                    return out;
                }
            }
            return input;
        }
    }]);

    /**
     * Filtert input nach input.date.begin an day
     */
    app.filter('atDate', [function () {
        return function (input, day) {
            var out = [];

            input.forEach(function (item) {
                var day_start = new Date(day.getTime());
                day_start.setHours(0);
                day_start.setMinutes(0);
                day_start.setSeconds(0);
                day_start.setMilliseconds(0);
                var day_end = new Date(day.getTime());
                day_end.setHours(23);
                day_end.setMinutes(59);
                day_end.setSeconds(59);
                day_end.setMilliseconds(999);

                if (item.date.begin >= day_start.getTime() && item.date.begin <= day_end.getTime()) {
                    out.push(item);
                }
            });

            return out;
        }
    }])

})();