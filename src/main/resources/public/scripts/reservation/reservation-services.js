/**
 * Created by Jan on 12.01.2016.
 */
(function () {
    'use strict';

    var reservation = angular.module('app.reservation');

    reservation.service('filterService', ['$http', '$filter', '$rootScope', 'roomManagerService',
        function ($http, $filter, $rootScope, roomManagerService) {

            function getQuarterHour(minutes) {
                if (minutes === 0) {
                    return -15;
                }
                if (minutes > 0 && minutes <= 15) {
                    return 0;
                }
                if (minutes > 15 && minutes <= 30) {
                    return 15;
                }
                if (minutes > 30 && minutes <= 45) {
                    return 30;
                }
                if (minutes > 45 && minutes) {
                    return 45;
                }
            }

            this.fetchTimeTables = function (avRooms, currDate) {

                if (avRooms.length > 0) {

                    avRooms.forEach(function (room) {

                        //timetable erstellen bzw. timetable leeren
                        room.timetable = {};

                        //timetable füllen
                        $http.get("/api/room/timetable/" + room.cla_roomId, {
                            params: {
                                date: currDate,
                                nocache: new Date().getTime()
                            }
                        }).
                        then(function success(response) {

                            response.data.forEach(function (booking) {

                                //beginTime in Stunde und Minuten (in Viertelstunden) teilen
                                var tempBookingBeginH = parseInt($filter('date')(booking.begin, "HH"));
                                var tempBookingBeginM = parseInt($filter('date')(booking.begin, "mm"));
                                tempBookingBeginM = getQuarterHour(tempBookingBeginM);

                                //endTime in Stunde und Minuten (in Viertelstunden) teilen
                                var tempBookingEndH = parseInt($filter('date')(booking.end, "HH"));
                                var tempBookingEndM = parseInt($filter('date')(booking.end, "mm"));
                                tempBookingEndM = getQuarterHour(tempBookingEndM);
                                if (tempBookingEndM === -15) {
                                    tempBookingEndM = 45;
                                    tempBookingEndH--;
                                }

                                //Buchung über mehrere Tage?
                                if ($filter('date')(booking.begin, "dd.MM.yyyy") < $filter('date')(booking.end, "dd.MM.yyyy")) {


                                    //belegte Räume (mehrtägige Buchungen) auf unavailable setzen

                                    if ($filter('date')(currDate, "dd.MM.yyyy") === $filter('date')(booking.begin, "dd.MM.yyyy")) {
                                        for (var n = tempBookingBeginH; n < 20; n++) {
                                            if (n === tempBookingBeginH) {
                                                switch (tempBookingBeginM) {
                                                    case -15:
                                                        room.timetable["h" + n + 0] = "unavailable";
                                                        room.timetable["h" + n + 15] = "unavailable";
                                                        room.timetable["h" + n + 30] = "unavailable";
                                                        room.timetable["h" + n + 45] = "unavailable";
                                                        break;
                                                    case 0:
                                                        room.timetable["h" + n + 15] = "unavailable";
                                                        room.timetable["h" + n + 30] = "unavailable";
                                                        room.timetable["h" + n + 45] = "unavailable";
                                                        break;
                                                    case 15:
                                                        room.timetable["h" + n + 30] = "unavailable";
                                                        room.timetable["h" + n + 45] = "unavailable";
                                                        break;
                                                    case 30:
                                                        room.timetable["h" + n + 45] = "unavailable";
                                                        break;
                                                    case 45:
                                                        //nichts
                                                        break;
                                                }
                                            } else {
                                                room.timetable["h" + n + 0] = "unavailable";
                                                room.timetable["h" + n + 15] = "unavailable";
                                                room.timetable["h" + n + 30] = "unavailable";
                                                room.timetable["h" + n + 45] = "unavailable";
                                            }
                                        }
                                    } else if ($filter('date')(currDate, "dd.MM.yyyy") === $filter('date')(booking.end, "dd.MM.yyyy")) {
                                        for (var n = 7; n <= tempBookingEndH; n++) {
                                            if (n === tempBookingEndH) {
                                                switch (tempBookingEndM) {
                                                    case -15: //nichts
                                                        break;
                                                    case 0:
                                                        room.timetable["h" + n + 0] = "unavailable";
                                                        break;
                                                    case 15:
                                                        room.timetable["h" + n + 0] = "unavailable";
                                                        room.timetable["h" + n + 15] = "unavailable";
                                                        break;
                                                    case 30:
                                                        room.timetable["h" + n + 0] = "unavailable";
                                                        room.timetable["h" + n + 15] = "unavailable";
                                                        room.timetable["h" + n + 30] = "unavailable";
                                                        break;
                                                    case 45:
                                                        room.timetable["h" + n + 0] = "unavailable";
                                                        room.timetable["h" + n + 15] = "unavailable";
                                                        room.timetable["h" + n + 30] = "unavailable";
                                                        room.timetable["h" + n + 45] = "unavailable";
                                                        break;
                                                }
                                            } else {
                                                room.timetable["h" + n + 0] = "unavailable";
                                                room.timetable["h" + n + 15] = "unavailable";
                                                room.timetable["h" + n + 30] = "unavailable";
                                                room.timetable["h" + n + 45] = "unavailable";
                                            }
                                        }
                                    } else { //sollte nie auftreten
                                        for (var n = 7; n < 20; n++) {
                                            room.timetable["h" + n + 0] = "unavailable";
                                            room.timetable["h" + n + 15] = "unavailable";
                                            room.timetable["h" + n + 30] = "unavailable";
                                            room.timetable["h" + n + 45] = "unavailable";
                                        }
                                    }

                                } else {

                                    //belegte Räume (eintägige Buchungen) auf unavailable setzen
                                    for (var n = tempBookingBeginH; n <= tempBookingEndH; n++) {
                                        if (n === tempBookingBeginH) {
                                            switch (tempBookingBeginM) {
                                                case -15:
                                                    room.timetable["h" + n + 0] = "unavailable";
                                                    room.timetable["h" + n + 15] = "unavailable";
                                                    room.timetable["h" + n + 30] = "unavailable";
                                                    room.timetable["h" + n + 45] = "unavailable";
                                                    break;
                                                case 0:
                                                    room.timetable["h" + n + 15] = "unavailable";
                                                    room.timetable["h" + n + 30] = "unavailable";
                                                    room.timetable["h" + n + 45] = "unavailable";
                                                    break;
                                                case 15:
                                                    room.timetable["h" + n + 30] = "unavailable";
                                                    room.timetable["h" + n + 45] = "unavailable";
                                                    break;
                                                case 30:
                                                    room.timetable["h" + n + 45] = "unavailable";
                                                    break;
                                                case 45:
                                                    //nichts
                                                    break;
                                            }
                                        } else if (n === tempBookingEndH) {
                                            switch (tempBookingEndM) {
                                                case -15: //nichts
                                                    break;
                                                case 0:
                                                    room.timetable["h" + n + 0] = "unavailable";
                                                    break;
                                                case 15:
                                                    room.timetable["h" + n + 0] = "unavailable";
                                                    room.timetable["h" + n + 15] = "unavailable";
                                                    break;
                                                case 30:
                                                    room.timetable["h" + n + 0] = "unavailable";
                                                    room.timetable["h" + n + 15] = "unavailable";
                                                    room.timetable["h" + n + 30] = "unavailable";
                                                    break;
                                                case 45:
                                                    room.timetable["h" + n + 0] = "unavailable";
                                                    room.timetable["h" + n + 15] = "unavailable";
                                                    room.timetable["h" + n + 30] = "unavailable";
                                                    room.timetable["h" + n + 45] = "unavailable";
                                                    break;
                                            }
                                        } else {
                                            room.timetable["h" + n + 0] = "unavailable";
                                            room.timetable["h" + n + 15] = "unavailable";
                                            room.timetable["h" + n + 30] = "unavailable";
                                            room.timetable["h" + n + 45] = "unavailable";
                                        }
                                    }
                                }
                            });
                        }, function failure(response) {
                        });
                    });
                }
            };

            //Mögliche Bestuhlungen für Raum zurückgeben
            this.fetchPossibleSeatingsForRoom = function (selectedRoom, scope) {
                roomManagerService.fetchPossibleSeatingsForRoom(selectedRoom.cla_roomId, function success(response, seatings) {
                    angular.copy(seatings, scope.possibleSeatingsForRoom);
                });
            };

            var availableRooms = [];

            //Freie Räume anhand Filterkriterien von Server erhalten
            this.fetchAvailableRooms = function (successFn, params) {

                var filterMultipleDays = false; //Ein- oder mehrtägige Suche?

                var formattedParams = {}; //Einige Parameter müssen formatiert werden bevor sie an den Server geschickt werden

                formattedParams.beginDate = $filter('date')(params.beginDate, "dd.MM.yyyy");

                if (params.endDate !== null) {
                    formattedParams.endDate = $filter('date')(params.endDate, "dd.MM.yyyy");
                    if (formattedParams.endDate > formattedParams.beginDate && formattedParams.endDate !== null) {
                        filterMultipleDays = true;
                    }
                }

                if (params.beginTime !== null) {
                    formattedParams.beginTime = $filter('date')(params.beginTime, "HH:mm");
                }

                if (params.endTime !== null) {
                    formattedParams.endTime = $filter('date')(params.endTime, "HH:mm");
                }

                formattedParams.memberCount = params.memberCount;

                if (params.seating !== 0 && params.seating !== null) {
                    formattedParams.seating = params.seating.id;
                }


                if (params.fixedEquipment > 0) {

                    var fxdeq = "[";

                    params.fixedEquipment.forEach(function (equ, index) {
                        if (index !== 0) {
                            fxdeq += ",";
                        }
                        fxdeq += "{id:" + equ + "}";
                    });

                    fxdeq += "]";
                    formattedParams.fixedEquipment = fxdeq;
                }

                formattedParams.nocache = new Date().getTime();

                if (filterMultipleDays) {
                    $http.get("/api/room/getfreeroomsmultipledays", {
                        params: formattedParams
                    }).then(function success(response) {
                        availableRooms = response.data;
                        successFn(response);
                    }, function failure(response) {
                    });
                } else {
                    $http.get("/api/room/getfreerooms", {
                        params: formattedParams
                    }).then(function success(response) {
                        availableRooms = response.data;
                        successFn(response);
                    }, function failure(response) {
                    });
                }
            };

            //Freie Räume bereitstellen
            this.getAvailableRooms = function () {
                return availableRooms;
            };
        }]);

    reservation.service('bookingService', ['$http', '$filter', 'appService', function ($http, $filter, appService) {

        /**
         * Berechnet Tage zwischen zwei Daten
         * @param date1
         * @param date2
         * @returns {*}
         */
        function daysBetweenDates(date1, date2) {
            if (!date1 || !date2) return null;
            var check1 = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var check2 = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            return Math.round(Math.abs(check1 - check2) / (1000 * 60 * 60 * 24 ));
        }

        /*
         ** Reservierung erzeugen **
         */

        this.createBooking = function (selectedBookingData, additionalEquipment, services, uibModalInstance, scope) {

            //Reservierungsdaten anpassen

            /*
             BETREFF MEHR ALS 50 ZEICHEN ?
             */
            if (selectedBookingData.description.length > 50) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Der Betreff darf eine Länge von 50 Zeichen nicht überschreiten.',
                    'timeout': 3000
                });
                scope.reservationLoading = false;
                return;
            }

            /*
             ANZAHL TEILNEHMER ÜBER MAXIMUM ?
             */
            if (selectedBookingData.memberCount < 2 || selectedBookingData.memberCount > selectedBookingData.room.cla_capacity) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Die Anzahl der Teilnehmer muss zwischen 2 und ' + selectedBookingData.room.cla_capacity + ' liegen.',
                    'timeout': 3000
                });
                scope.reservationLoading = false;
                return;
            }

            /*
             DATUM UND UHRZEIT ANGEGEBEN ?
             */
            if (selectedBookingData.beginDate === null || selectedBookingData.beginTime === null || selectedBookingData.endTime === null) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Bitte geben Sie Datum und Uhrzeit der Reservierung an.',
                    'timeout': 3000
                });
                scope.reservationLoading = false;
                return;
            }

            /*
             SEATING ANGEGEBEN ?
             */
            if (selectedBookingData.seating === 0) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Bitte geben Sie Ihre gewünschte Bestuhlung an.',
                    'timeout': 3000
                });
                scope.reservationLoading = false;
                return;
            }


            /*
             EXTERNE TEILNEHMER ?
             */
            if (selectedBookingData.extern) {
                if (selectedBookingData.serviceSelected) {

                    //Service nur 3 Tage im vorraus möglich
                    if (daysBetweenDates(new Date(), selectedBookingData.beginDate) < 3) {
                        appService.addAlert({
                            type: 'danger',
                            msg: 'Catering muss drei Tage im Voraus gebucht werden.',
                            'timeout': 3000
                        });
                        scope.reservationLoading = false;
                        return;
                    }

                    selectedBookingData.service = {};

                    var tempServices = {};
                    var tempServicesCount = 1;
                    services.forEach(function (service) {
                        if (service.selected) {
                            tempServices["id" + tempServicesCount] = service.id;
                            tempServicesCount++;
                        }
                    });
                    selectedBookingData.service = tempServices;

                    if (getObjectSize(selectedBookingData.service) === 0) {
                        appService.addAlert({
                            type: 'danger',
                            msg: 'Bitte wählen Sie die gewünschte Bewirtung aus.',
                            'timeout': 3000
                        });
                        scope.reservationLoading = false;
                        return;
                    }

                    if (selectedBookingData.costcenter.length != 8 || selectedBookingData.costcenter.match(/^[0-9]+$/) === null) {
                        appService.addAlert({
                            type: 'danger',
                            msg: 'Bitte geben Sie eine gültige Kostenstelle für die Bewirtung an (8 Ziffern).',
                            'timeout': 3000
                        });
                        scope.reservationLoading = false;
                        return;
                    }
                }
            }

            /*
             KOMMENTAR MEHR ALS 500 ZEICHEN ?
             */
            if (selectedBookingData.comment.lenght > 500) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Der Kommentar darf eine Länge von 500 Zeichen nicht überschreiten',
                    'timeout': 3000
                });
                scope.reservationLoading = false;
                return;
            }

            /*
             FORMAT ADDITIONAL EQUIPMENTS
             */

            var tempAddEqu = {};
            var tempAddEquCount = 1;
            additionalEquipment.forEach(function (equ) {
                if (equ.selected) {
                    tempAddEqu["equipment" + tempAddEquCount] = equ.id;
                    tempAddEquCount++;
                }
            });
            selectedBookingData.additionalEquipment = tempAddEqu;

            /*
             STATUS 412 VERHINDERN (Fehlende Parameter anlegen)
             */

            //Keine externen Teilnehmer --> Kostenstelle muss trotzdem gültigen Wert enthalten (wird nicht in DB eingetragen)
            if (selectedBookingData.costcenter.length != 8 || selectedBookingData.costcenter.match(/^[0-9]+$/) === null) {
                selectedBookingData.costcenter = 10000000;
            }

            //Wenn Enddatum leer, dann Enddatum == Startdatum
            if (selectedBookingData.endDate === null || selectedBookingData.endDate === "") {
                selectedBookingData.endDate = selectedBookingData.beginDate;
            }

            if (selectedBookingData.description === "") {
                selectedBookingData.description = "- Kein Betreff -";
            }

            /*
             CREATE PARAMS (converting)
             - description
             - beginDate
             - beginTime
             - endDate
             - endTime
             - extern
             - memberCount
             - roomID
             - seatingID
             - additionalEquipment
             - externalEmails
             - costcenter
             - service
             - comment
             */

            var params = {};

            angular.copy(selectedBookingData, params);

            params.beginDate = $filter('date')(params.beginDate, "dd.MM.yyyy");
            params.beginTime = $filter('date')(params.beginTime, "HH:mm");
            params.endDate = $filter('date')(params.endDate, "dd.MM.yyyy");
            params.endTime = $filter('date')(params.endTime, "HH:mm");

            params.costcenter = parseInt(params.costcenter);

            params.roomID = params.room.cla_roomId;
            delete params.room;

            params.seatingID = params.seating;
            delete params.seating;

            delete params.serviceSelected;

            //fixe Equipments sind für Reservierungsformular irrelevant (da Raum schon angegeben)
            if (params.hasOwnProperty("fixedEquipment")) {
                delete params.fixedEquipment;
            }

            // POST REQUEST

            $http.post("/api/booking/create", {
                responseType: 'json',
                data: params
            }).then(function success(response) {
                scope.reservationLoading = false;
                appService.addAlert({
                    type: 'success',
                    msg: 'Ihre Reservierungsanfrage wurde erfolgreich erstellt.',
                    timeout: 5000
                });
                uibModalInstance.close();
            }, function failure(response) {
                scope.reservationLoading = false;
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    timeout: 5000
                });
            });

        };


        /*
         ********************
         */

        var bookingData = [];

        this.fetchPendingRequests = function (successFn) {
            this.fetchBookingData(successFn);
            return this.getPendingRequests();
        };
        this.fetchBookingData = function (successFn) {
            $http.get('/api/booking/getallforuser', {
                params: {nocache: new Date().getTime()}
            }).then(function success(response) {

                var bookingList = [];
                response.data.forEach(function (booking) {
                    var bookingItem = {};

                    bookingItem.bookingID = booking.cla_bookingId;
                    bookingItem.description = booking.cla_description;
                    bookingItem.memberCount = booking.cla_memberCount;
                    bookingItem.status = booking.cla_status.id;
                    bookingItem.costcenter = booking.costCenter;

                    bookingItem.date = {
                        begin: booking.cla_beginDate,
                        end: booking.cla_endDate,
                        created_at: booking.cla_createdAt
                    };

                    bookingItem.room = {
                        roomID: booking.cla_room.cla_roomId,
                        name: booking.cla_room.cla_name,
                        description: booking.cla_room.cla_description,
                        capacity: booking.cla_room.cla_capacity
                    };

                    bookingItem.seatingID = booking.cla_seating.cla_seatingId;

                    bookingItem.requester = {
                        userID: booking.cla_user.cla_userId,
                        firstname: booking.cla_user.cla_firstname,
                        surname: booking.cla_user.cla_surname,
                        gender: booking.cla_user.cla_gender,
                        mail: booking.cla_user.cla_mail,
                        phone: booking.cla_user.cla_phone,
                        department: {
                            departmentID: booking.cla_user.cla_department.cla_departmentId,
                            name: booking.cla_user.cla_department.cla_name
                        }
                    };

                    if (booking.cla_extern === true)
                        bookingItem.extern = true;

                    if (booking.cla_service === true)
                        bookingItem.service = true;

                    bookingList.push(bookingItem);
                });
                bookingData = bookingList;
                successFn(response);

            }, function failure(response) {
                if (response.status == 401) {
                    bookingData = [];
                }
            });

            return bookingData;
        };

        this.getPendingRequests = function () {
            var pendingRequests = [];

            bookingData.forEach(function (item) {
                if (item.status === 1) {
                    pendingRequests.push(item);
                }
            });

            return pendingRequests;
        };

        this.fetchValidBookingsAndRequests = function (successFn) {
            this.fetchBookingData(successFn);
            return this.getValidBookingsAndRequests();
        };

        this.getValidBookingsAndRequests = function () {
            var requests = [];

            bookingData.forEach(function (item) {
                if (item.status === 1 || item.status === 2 || item.status === 3 || item.status === 4) {
                    requests.push(item);
                }
            });

            return requests;
        };

        this.getRequests = function (status) {
            if (typeof  status === 'undefined')
                return bookingData;

            var requests = [];


            if (angular.isArray(status) && status.length > 0) {
                bookingData.forEach(function (item) {
                    for (var i = 0; i < status.length; i++) {
                        if (item.status === status[i]) {
                            requests.push(item);
                            break;
                        }
                    }
                });
            } else {
                if (angular.isNumber) {
                    bookingData.forEach(function (item) {
                        if (item.status === status) {
                            requests.push(item);
                        }
                    });
                }
            }
        };

        this.cancelOwnBooking = function (bookingID, successFn, failureFn) {
            $http.post('/api/booking/cancel/' + bookingID, {}).then(successFn, failureFn);
        }

    }]);

    function getObjectSize(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    }


})();