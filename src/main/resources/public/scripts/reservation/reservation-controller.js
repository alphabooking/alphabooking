/**
 * Created by Jan on 10.01.2016.
 */

(function () {

    var reservationModule = angular.module('app.reservation');

    reservationModule.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.onEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    });

    reservationModule.controller('RoomFilterController', ['$scope', '$rootScope', 'filterService', '$filter', 'appService', '$uibModal',
    function ($scope, $rootScope, filterService, $filter, appService, $uibModal) {

        //datepicker
        $scope.format = 'dd.MM.yyyy';
        $scope.minDate = today();
        $scope.openedDatePickerBeginTime = false;
        $scope.openedDatePickerEndTime = false;
        $scope.buttonBar = false;

        function today() {
            var _today = new Date();
            _today.setHours(0);
            _today.setMinutes(0);
            _today.setSeconds(0);
            _today.setMilliseconds(0);
            return _today;
        }

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };

        $scope.openDatePickerBeginTime = function () {
            $scope.openedDatePickerBeginTime = true;
        };

        $scope.openDatePickerEndTime = function () {
            $scope.openedDatePickerEndTime = true;
        };

        //timepicker
        $scope.hstep = 1;
        $scope.mstep = 1;

        $scope.customizeBeginTime = function () {
            var tempMinutes = $scope.selectedFilter.beginTime.getMinutes();
            if (tempMinutes % 15 !== 0) {
                tempMinutes = (15*Math.round(tempMinutes/15));
                var tempDateBegin = new Date();
                tempDateBegin.setMinutes(tempMinutes);
                tempDateBegin.setHours($scope.selectedFilter.beginTime.getHours());
                $scope.selectedFilter.beginTime = tempDateBegin;
            }
            if ($scope.selectedFilter.beginTime.getHours() < 7) {
                var tempDateBegin = new Date();
                tempDateBegin.setMinutes($scope.selectedFilter.beginTime.getMinutes());
                tempDateBegin.setHours(7);
                $scope.selectedFilter.beginTime = tempDateBegin;
            }
            if ($scope.selectedFilter.beginTime.getHours() > 19) {
                if ($scope.selectedFilter.beginTime.getMinutes() > 0) {
                    var tempDateBegin = new Date();
                    tempDateBegin.setMinutes($scope.selectedFilter.beginTime.getMinutes());
                    tempDateBegin.setHours(19);
                    $scope.selectedFilter.beginTime = tempDateBegin;
                } else {
                    var tempDateBegin = new Date();
                    tempDateBegin.setMinutes($scope.selectedFilter.beginTime.getMinutes());
                    if ($scope.selectedFilter.beginTime.getHours() === 19) {
                        tempDateBegin.setHours(19);
                    } else {
                        tempDateBegin.setHours(20);
                    }
                    $scope.selectedFilter.beginTime = tempDateBegin;
                }
            }
        };

        $scope.customizeEndTime = function () {
            var tempMinutes = $scope.selectedFilter.endTime.getMinutes();
            if (tempMinutes % 15 !== 0) {
                tempMinutes = (15*Math.round(tempMinutes/15));
                var tempDateEnd = new Date();
                tempDateEnd.setMinutes(tempMinutes);
                tempDateEnd.setHours($scope.selectedFilter.endTime.getHours());
                $scope.selectedFilter.endTime = tempDateEnd;
            }
            if ($scope.selectedFilter.endTime.getHours() < 7) {
                var tempDateEnd = new Date();
                tempDateEnd.setMinutes($scope.selectedFilter.endTime.getMinutes());
                tempDateEnd.setHours(7);
                $scope.selectedFilter.endTime = tempDateEnd;
            }
            if ($scope.selectedFilter.endTime.getHours() > 19) {
                if ($scope.selectedFilter.endTime.getMinutes() > 0) {
                    var tempDateEnd = new Date();
                    tempDateEnd.setMinutes($scope.selectedFilter.endTime.getMinutes());
                    tempDateEnd.setHours(19);
                    $scope.selectedFilter.endTime = tempDateEnd;
                } else {
                    var tempDateEnd = new Date();
                    tempDateEnd.setMinutes($scope.selectedFilter.endTime.getMinutes());
                    if ($scope.selectedFilter.endTime.getHours() === 19) {
                        tempDateEnd.setHours(19);
                    } else {
                        tempDateEnd.setHours(20);
                    }
                    $scope.selectedFilter.endTime = tempDateEnd;
                }
            }
        };

        $scope.pagination = {
            activated: false,
            pages: []
        };

        $scope.activePage = 0;
        $scope.currentDateInTable = "-";

        $scope.noResultsFound = false;

        $scope.getPaginationClass = function (index) {
            if (index === $scope.activePage) {
                return "active";
            }
            return "";
        };

        $scope.selectDatePage = function (index) {
            $scope.activePage = index;
            $scope.currentDateInTable = $filter('date')($scope.pagination.pages[index], "dd.MM.yyyy");
            filterService.fetchTimeTables($scope.availableRooms, $scope.currentDateInTable);
        };

        $scope.selectableTimes = [];

        for(var i = 7; i < 20; i++) {
            $scope.selectableTimes.push("h" + i + "0");
            $scope.selectableTimes.push("h" + i + "15");
            $scope.selectableTimes.push("h" + i + "30");
            $scope.selectableTimes.push("h" + i + "45");
        }

        //Filterwerte
        $scope.selectedFilter = {
            beginDate: new Date(),
            beginTime: null,
            endDate: null,
            endTime: null,
            memberCount: 2,
            seating: 0,
            fixedEquipment: []
        };

        $scope.bookingDataCreated = false;

        $scope.possibleSeatings = appService.getAllSeatings();
        $scope.fixedEquipment = appService.getAllFixedEquipments();

        $rootScope.$on('CONFIG_LOADED', function(evt){
            $scope.possibleSeatings = appService.getAllSeatings();
            $scope.fixedEquipment = appService.getAllFixedEquipments();
            $scope.additionalEquipment = appService.getAllAdditionalEquipments();
        });

        $scope.availableRooms = [];

        $scope.currentTimetables = [];

        $scope.searchLoading = false;

        $scope.getAvailableRooms = function () {

            //Zeit anpassen
            if ($scope.selectedFilter.beginTime !== null) {
                $scope.customizeBeginTime();
            }
            if ($scope.selectedFilter.endTime !== null) {
                $scope.customizeEndTime();
            }

            if ($scope.selectedFilter.memberCount < 2 || $scope.selectedFilter.memberCount > 40) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Die Anzahl der Teilnehmer muss zwischen 2 und 40 liegen.',
                    'timeout': 3000
                });
                $scope.searchLoading = false;
                return;
            }

            if ($scope.selectedFilter.beginDate === null) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Bitte geben Sie das Datum an, an dem Sie einen Raum reservieren möchten.',
                    'timeout': 3000
                });
                $scope.searchLoading = false;
                return;
            }

            $scope.searchLoading = true; //Ladeicon anzeigen und Animation starten

            //Zeittafel für Startdatum als erstes anzeigen (wichtig für Suche über mehrere Tage)
            $scope.currentDateInTable = $filter('date')($scope.selectedFilter.beginDate, "dd.MM.yyyy");

            //Gewählte Equipment-IDs bekommen
            var selectedEquipments = [];

            $scope.fixedEquipment.forEach(function (equipment) {
                if (equipment.selected) {
                    selectedEquipments.push(equipment.id);
                }
            });

            $scope.selectedFilter.fixedEquipment = selectedEquipments;

            //Navigation für Suche über mehrere Tage zurücksetzen
            $scope.pagination.activated = false;
            $scope.pagination.pages = [];

            //Suche über mehrere Tage ?
            if ($scope.selectedFilter.endDate > $scope.selectedFilter.beginDate) {

                //Alle Pflichtfelder gefüllt ?
                if ($scope.selectedFilter.beginTime === null || $scope.selectedFilter.endTime === null) {
                    appService.addAlert({
                        type: 'danger',
                        msg: 'Bitte geben Sie Anfangs- und Endzeit an.',
                        'timeout': 3000
                    });
                    $scope.searchLoading = false;
                    return;
                }

                //Navigation erstellen um zwischen Tagen wechseln zu können
                $scope.pagination.activated = true;
                var selDates = getDatesBetween(new Date($scope.selectedFilter.beginDate), new Date($scope.selectedFilter.endDate));
                selDates.forEach(function (dat) {
                    $scope.pagination.pages.push(dat);
                });
            } else if ($filter('date')($scope.selectedFilter.endDate, "dd.MM.yyyy") < $filter('date')($scope.selectedFilter.beginDate, "dd.MM.yyyy")
                        && $scope.selectedFilter.endDate !== null) {
                //Startdatum > Enddatum ?
                appService.addAlert({
                    type: 'danger',
                    msg: 'Das Startdatum muss vor dem Enddatum liegen.',
                    'timeout': 3000
                });
                $scope.searchLoading = false;
                return;
            } else if ($filter('date')($scope.selectedFilter.beginTime, "HH:mm") > $filter('date')($scope.selectedFilter.endTime, "HH:mm")) {
                //Startzeit > Endzeit?
                appService.addAlert({
                    type: 'danger',
                    msg: 'Die Startzeit muss vor der Endzeit liegen.',
                    'timeout': 3000
                });
                $scope.searchLoading = false;
                return;
            }

            //Zeitvergleichswerte generieren
            var tempTimeBegin = new Date();
            tempTimeBegin.setHours(7);
            tempTimeBegin.setMinutes(0);
            var tempTimeEnd = new Date();
            tempTimeEnd.setHours(20);
            tempTimeEnd.setMinutes(0);

            //Startzeit < 7:00 Uhr oder Endzeit > 20:00 Uhr?
            if (($filter('date')($scope.selectedFilter.beginTime, "HH:mm") < $filter('date')(tempTimeBegin, "HH:mm"))
                || ($filter('date')($scope.selectedFilter.endTime, "HH:mm") > $filter('date')(tempTimeEnd, "HH:mm"))) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Der Zeitraum muss zwischen 7:00 und 20:00 Uhr liegen.',
                    'timeout': 3000
                });
                $scope.searchLoading = false;
                return;
            }

            //freie Räume finden
            $scope.availableRooms = filterService.fetchAvailableRooms(function success(response) {
                $scope.availableRooms = filterService.getAvailableRooms();

                if ($scope.availableRooms.length === 0) {
                    $scope.noResultsFound = true;
                } else {
                    $scope.noResultsFound = false;
                    filterService.fetchTimeTables($scope.availableRooms, $scope.currentDateInTable);
                }

                $scope.searchLoading = false;
            }, $scope.selectedFilter);
        };


        /**
         * MODAL
         */
        var animationsEnabled = true;

        //Öffnet Reservierungsformular
        $scope.openBookingModal = function (index) {

            var modalInstance = $uibModal.open({
                animation: animationsEnabled,
                templateUrl: 'bookReservation.html',
                controller: 'BookingModalInstanceCtrl',
                resolve: {
                    filterValues: $scope.selectedFilter,
                    selectedRoom: $scope.availableRooms[index]
                }
            });

            modalInstance.result.then(function () {
                //Update
            }, function () {
                //Dissmiss
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        //Erhöht Datum um einen Tag
        function addDays (days) {
            var dat = new Date(this.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        //Gibt alle Tage zwischen zwei Daten zurück (inklusive min und max)
        function getDatesBetween (startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push( new Date (currentDate) )
                currentDate = currentDate.addDays(1);
            }
            dateArray.push( new Date (currentDate));
            return dateArray;
        }

    }]);

    reservationModule.controller('BookingModalInstanceCtrl', ['$scope', '$rootScope', 'filterService', '$uibModalInstance',
    'filterValues', 'selectedRoom', 'appService', 'bookingService', '$http', function ($scope, $rootScope, filterService,
                                                                              $uibModalInstance, filterValues,
                                                                              selectedRoom, appService, bookingService, $http) {

        //Benutzerinformationen bekommen
        $scope.currentUserInformation = {};

        $http.get('/api/user/info', {}).then(function (response) {
            $scope.currentUserInformation = response.data;
            console.log(response.data);
        });

        //Ladeicon für Reservierung
        $scope.reservationLoading = false;

        //Werte aus Filter übergeben
        $scope.selectedBookingData = {};
        angular.copy(filterValues, $scope.selectedBookingData);
        $scope.selectedBookingData.room = selectedRoom;

        $scope.threeDaysLeft = true;

        function daysBetweenDates(date1, date2) {
            if (!date1 || !date2) return null;
            var check1 = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var check2 = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            return Math.round(Math.abs(check1 - check2) / (1000 * 60 * 60 * 24 ));
        }

        if (daysBetweenDates(new Date(), $scope.selectedBookingData.beginDate) < 3) {
            $scope.threeDaysLeft = true;
        } else {
            $scope.threeDaysLeft = false;
        }

        //Bestuhlungsobjekt zu BestuhlungsID konvertieren
        if (typeof $scope.selectedBookingData.seating === 'object') {
            if ($scope.selectedBookingData.seating !== null) {
                $scope.selectedBookingData.seating = $scope.selectedBookingData.seating.id;
            }
        }

        //Mögliche Bestuhlungen für Raum abfragen
        $scope.possibleSeatingsForRoom = [];
        filterService.fetchPossibleSeatingsForRoom(selectedRoom, $scope);

        //Zusätzliches Equipment und Catering erhalten
        $scope.additionalEquipment = appService.getAllAdditionalEquipments();
        $scope.selectableServices = appService.getAllServiceTypes();
        $rootScope.$on('CONFIG_LOADED', function(evt){
            $scope.additionalEquipment = appService.getAllAdditionalEquipments();
            $scope.selectableServices = appService.getAllServiceTypes();
        });

        //Platzhalter für zusätzliche Felder gegenüber Filter in Formular generieren
        $scope.selectedBookingData.extern = false;
        $scope.selectedBookingData.serviceSelected = false;
        $scope.selectedBookingData.externalEmails = {};
        $scope.selectedBookingData.description = "";
        $scope.selectedBookingData.costcenter = "";
        $scope.selectedBookingData.comment = "";
        $scope.selectedBookingData.service = {};

        //handle external mail adresses
        $scope.currentExternalMail = "";

        $scope.addExternalMail = function () {
            //Prüfen, ob Emailadresse gültig ist (enthält @, weniger als 50 Zeichen und ist nicht leer)
            if ($scope.currentExternalMail.indexOf("@") === -1) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Bitte geben Sie eine gültige E-Mail Adresse ein.',
                    'timeout': 3000
                });
            } else if ($scope.currentExternalMail.length > 50) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Die E-Mail Adresse darf eine Länge von 50 Zeichen nicht überschreiten.',
                    'timeout': 3000
                });
            } else {
                //E-Mail hinzufügen
                var mailCount = getObjectSize($scope.selectedBookingData.externalEmails);

                if (mailCount === 0) {
                    $scope.selectedBookingData.externalEmails["email" + (mailCount + 1)] = $scope.currentExternalMail;
                } else {
                    var mailKeys = Object.keys($scope.selectedBookingData.externalEmails);
                    var lastId = parseInt(mailKeys[mailKeys.length - 1].replace("email", ""));
                    $scope.selectedBookingData.externalEmails["email" + (lastId + 1)] = $scope.currentExternalMail;
                }

                $scope.currentExternalMail = ""; //Feld für E-Maileingabe leeren
            }
        };

        //E-Mail adresse aus Liste entfernen
        $scope.removeExternalMail = function (mail) {
            var currKey = getKeyByValue($scope.selectedBookingData.externalEmails, mail);
            delete $scope.selectedBookingData.externalEmails[currKey];
        };

        //Reservierung abschicken
        $scope.ok = function () {
            $scope.reservationLoading = true;

            if ($scope.selectedBookingData.beginTime) {
                $scope.customizeBeginTime();
            }
            if ($scope.selectedBookingData.endTime) {
                $scope.customizeEndTime();
            }


            bookingService.createBooking($scope.selectedBookingData, $scope.additionalEquipment, $scope.selectableServices, $uibModalInstance, $scope);

            //$uibModalInstance.close();
        };

        //Reservierung abbrechen
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        //Anzahl der Parameter eines Objekts erhalten
        function getObjectSize (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        }

        //Schlüssel eines Objekts über den Wert erhalten
        function getKeyByValue (obj, value) {
            for( var prop in obj ) {
                if( obj.hasOwnProperty(prop) ) {
                    if( obj[prop] === value )
                        return prop;
                }
            }
        }

        //Datepicker
        $scope.format = 'dd.MM.yyyy';
        $scope.minDate = today();
        $scope.openedDatePickerEndTime = false;
        $scope.buttonBar = false;

        function today() {
            var _today = new Date();
            _today.setHours(0);
            _today.setMinutes(0);
            _today.setSeconds(0);
            _today.setMilliseconds(0);
            return _today;
        }

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };

        $scope.openDatePickerEndTime = function () {
            $scope.openedDatePickerEndTime = true;
        };

        //timepicker
        $scope.hstep = 1;
        $scope.mstep = 15;

        $scope.customizeBeginTime = function () {
            var tempMinutes = $scope.selectedBookingData.beginTime.getMinutes();
            if (tempMinutes % 15 !== 0) {
                tempMinutes = (15*Math.round(tempMinutes/15));
                var tempDateBegin = new Date();
                tempDateBegin.setMinutes(tempMinutes);
                tempDateBegin.setHours($scope.selectedBookingData.beginTime.getHours());
                $scope.selectedBookingData.beginTime = tempDateBegin;
            }
            if ($scope.selectedBookingData.beginTime.getHours() < 7) {
                var tempDateBegin = new Date();
                tempDateBegin.setMinutes($scope.selectedBookingData.beginTime.getMinutes());
                tempDateBegin.setHours(7);
                $scope.selectedBookingData.beginTime = tempDateBegin;
            }
            if ($scope.selectedBookingData.beginTime.getHours() > 19) {
                if ($scope.selectedBookingData.beginTime.getMinutes() > 0) {
                    var tempDateBegin = new Date();
                    tempDateBegin.setMinutes($scope.selectedBookingData.beginTime.getMinutes());
                    tempDateBegin.setHours(19);
                    $scope.selectedBookingData.beginTime = tempDateBegin;
                } else {
                    var tempDateBegin = new Date();
                    tempDateBegin.setMinutes($scope.selectedBookingData.beginTime.getMinutes());
                    if ($scope.selectedBookingData.beginTime.getHours() === 19) {
                        tempDateBegin.setHours(19);
                    } else {
                        tempDateBegin.setHours(20);
                    }
                    $scope.selectedBookingData.beginTime = tempDateBegin;
                }
            }
        };

        $scope.customizeEndTime = function () {
            var tempMinutes = $scope.selectedBookingData.endTime.getMinutes();
            if (tempMinutes % 15 !== 0) {
                tempMinutes = (15*Math.round(tempMinutes/15));
                var tempDateEnd = new Date();
                tempDateEnd.setMinutes(tempMinutes);
                tempDateEnd.setHours($scope.selectedBookingData.endTime.getHours());
                $scope.selectedBookingData.endTime = tempDateEnd;
            }
            if ($scope.selectedBookingData.endTime.getHours() < 7) {
                var tempDateEnd = new Date();
                tempDateEnd.setMinutes($scope.selectedBookingData.endTime.getMinutes());
                tempDateEnd.setHours(7);
                $scope.selectedBookingData.endTime = tempDateEnd;
            }
            if ($scope.selectedBookingData.endTime.getHours() > 19) {
                if ($scope.selectedBookingData.endTime.getMinutes() > 0) {
                    var tempDateEnd = new Date();
                    tempDateEnd.setMinutes($scope.selectedBookingData.endTime.getMinutes());
                    tempDateEnd.setHours(19);
                    $scope.selectedBookingData.endTime = tempDateEnd;
                } else {
                    var tempDateEnd = new Date();
                    tempDateEnd.setMinutes($scope.selectedBookingData.endTime.getMinutes());
                    if ($scope.selectedBookingData.endTime.getHours() === 19) {
                        tempDateEnd.setHours(19);
                    } else {
                        tempDateEnd.setHours(20);
                    }
                    $scope.selectedBookingData.endTime = tempDateEnd;
                }
            }
        };
    }]);

})();