/**
 * Created by Jan on 22.01.2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('NotFoundController', ['$scope', '$state', 'appService', function ($scope, $state, appService) {

        $scope.previousState = appService.previousState;
        $scope.goToPreviousState = function () {
            $state.go($scope.previousState);
        }

    }]);
})();