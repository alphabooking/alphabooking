/**
 * Created by Jan on 26.11.2015.
 */
(function () {
    'use strict';
    var app = angular.module('app', ['ui.router', 'ngResource', 'ngMaterial', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'app.auth', 'app.menu', 'app.user', 'app.reservation', 'app.overview', 'app.admin']);

    app.controller('MainController', ['$rootScope', '$scope', function ($rootScope, $scope) {
    }]);

    app.controller('AppController', ['$rootScope', '$scope', function ($rootScope, $scope) {
    }]);

    app.controller('TitleController', ['$scope', function ($scope) {
        $scope.title = "AlphaBooking";
    }]);


    app.config(['$httpProvider', '$stateProvider', '$logProvider', '$locationProvider', 'authenticationProvider',
        function ($httpProvider, $stateProvider, $logProvider, $locationProvider, authenticationProvider) {

            $logProvider.debugEnabled(true);

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            $httpProvider.interceptors.push(['$q', 'authentication', '$location', function ($q, authentication, $location) {
                return {
                    // optional method
                    'responseError': function (rejection) {
                        console.debug(rejection);
                        // do something on error
                        if (rejection.status == 401) {
                            // Zur Login-Seite
                            authentication.logout();
                            //$state.go('login');
                            $location.path('/login');
                        }
                        if (rejection.status == 403) {
                            $location.path('/pagenotfound');
                        }
                        return $q.reject(rejection);
                    }
                }
            }]);

        }]);

    app.run(['$http', 'authenticationService', '$rootScope', '$state', '$log', 'userService', '$location', 'appService', 'dashboardNewsService', 'bookingService', 'reservationManagerService', 'bookingManagerService', '$anchorScroll',
        function ($http, authenticationService, $rootScope, $state, $log, userService, $location, appService, dashboardNewsService, bookingService, reservationManagerService, bookingManagerService, $anchorScroll) {
            $anchorScroll.yOffset = -50;

            var authToken = authenticationService.getAuthToken();
            if (authToken != null) {
                $http.defaults.headers.common = {'X-AUTH-TOKEN': authenticationService.getAuthToken()}; // Setzte initial einen Token beim Start, wenn einer vorhanden ist!
                authenticationService.validateLoginOnStartUp(function () {
                    $rootScope.$emit('LOGGED_IN');

                    appService.startUpHasFinished();
                });
            }

            $rootScope.$on('LOGGED_IN', function (evt, to, params) {
                $log.debug('LOGGED_IN Event raised');

                $http.defaults.headers.common = {'X-AUTH-TOKEN': authenticationService.getAuthToken()}; // Setzte einen Token beim Login, wenn einer vorhanden ist!

                userService.fetchUserData(function (response) {

                });

                userService.fetchUserRoles(function (response) {
                    if (userService.isVerwalter()) {
                        reservationManagerService.fetchBookings(function () {

                        });
                        bookingManagerService.fetchBookings(function () {
                        });
                    }
                    $rootScope.$emit('USERROLES_FETCHED');
                });

                dashboardNewsService.fetchNews(function () {
                });
                bookingService.fetchBookingData(function () {

                });

            });

            $rootScope.$on('$stateChangeStart', function (evt, to, params, from) {

            });

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                $log.debug('Emit', '$stateChangeSuccess', appService.isStartUpFinished());
                if (!appService.isStartUpFinished()) {
                    handleBootUpAuthorizationRedirect(event, $state, authenticationService, userService);
                } else {
                    handleAuthorizationRedirect(event, toState, toParams, authenticationService, $state, appService);
                    if (toState.redirectTo) {
                        return redirect(event, toState.redirectTo, toParams, $state);
                    }
                }
                appService.previousState(fromState);
            });


            $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
                $log.debug('Emit', '$stateNotFound');
                event.preventDefault();
                appService.previousState(fromState);
                $state.go('notfound');
            });


            $rootScope.$on('STARTUP_FINISHED', function (evt) {
                $log.debug('Startup finished');
            });

            appService.loadConfig();

        }]);

    function handleBootUpAuthorizationRedirect(evt, $state, authenticationService, userService) {
        var loggedIn = authenticationService.isLoggedIn();
        var isAdmin = userService.isVerwalter();
        if (!$state.current.auth.needLogin) {
            return;
        }
        if (loggedIn) {
            if ($state.current.auth.needLogin === false) {
                redirect(evt, 'home', {}, $state);
                return;
            }
            if ($state.current.auth.needAdmin === true) {
                if (!isAdmin) {
                    redirect(evt, 'home', {}, $state);
                    return;
                }
            }
        } else {
            if ($state.current.auth.needLogin === true) {
                redirect(evt, 'login', {}, $state);
                return
            }
        }
    }

    function handleAuthorizationRedirect(evt, to, params, authenticationService, $state, appService) {
        var $log = angular.injector(['ng']).get('$log');
        var loggedIn = authenticationService.isLoggedIn();

        if (to.name !== "login" && to.name !== "passwordreset") {

            //not Login AND not Passwordreset
            if (loggedIn) {

            } else { //Kein Auth Token
                redirect(evt, 'login', params, $state);
            }
        } else { //Route to Lgoin || Password Reset
            if (loggedIn) {
                redirect(evt, 'home', params, $state);
            } else {
            }
        }
    }

    function redirect(evt, destination, params, $state) {
        evt.preventDefault();
        $state.go(destination, params);
    }


})();

