/**
 * Created by Jan on 19.01.2016.
 */
(function () {

    var reportsModule = angular.module('app.admin.reports');

    reportsModule.service('reportDataService', ['$http', 'requestDateFactory', function ($http, requestDateFactory) {

        /**
         * Anzahl der Buchungen (alle Räume/ein Raum)
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         * @param params Parameter für die Auswertung
         */
        this.fetchBookingCount = function (successFn, params) {
            var urls = [
                '/api/report/getcountallrooms',
                '/api/report/getcountsingleroom'
            ];

            var urlParams = parseToUrlParams(params);

            var url = urls[0];
            if (urlParams.roomId) {
                url = urls[1];
            }

            fetchData(successFn, url, urlParams)

        };

        /**
         * Durchschnittliche Anzahl an Buchungen eines Raums / aller Räume
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         * @param beginDate Datum des Beginns ab dem die Auswertung stattfinden soll.
         * @param endDate Datum des Endes bis zu dem die Auswertung stattfinden soll.
         * @param status Status der einzubeziehenden Buchungen
         * @param extern ?
         * @param roomId Bei Angabe: Auswertung nur für angegebenen Raum, sonst für alle existierende
         */
        this.fetchAverageBookingCount = function (successFn, params) {

            var urls = [
                '/api/report/getavgcountallrooms',
                '/api/report/getavgcountsingleroom'
            ];

            var urlParams = parseToUrlParams(params);

            var url = urls[0];
            if (urlParams.roomId) {
                url = urls[1];
            }

            fetchData(successFn, url, urlParams);

        };

        /**
         * Auslastung aller Räume / eines Raums
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         * @param beginDate Datum des Beginns ab dem die Auswertung stattfinden soll.
         * @param endDate Datum des Endes bis zu dem die Auswertung stattfinden soll.
         * @param status Status der einzubeziehenden Buchungen
         * @param extern ?
         * @param roomId Bei Angabe: Auswertung nur für angegebenen Raum, sonst für alle existierende
         */
        this.fetchRoomUtilization = function (successFn, params) {

            var urls = [
                '/api/report/gethoursallrooms',
                '/api/report/gethourssingleroom'
            ];

            var urlParams = parseToUrlParams(params);

            var url = urls[0];
            if (urlParams.roomId) {
                url = urls[1];
            }

            fetchData(successFn, url, urlParams);

        };

        /**
         * Auslastung Durchschnittlich aller Räume / eines Raums in Stunden
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         * @param beginDate Datum des Beginns ab dem die Auswertung stattfinden soll.
         * @param endDate Datum des Endes bis zu dem die Auswertung stattfinden soll.
         * @param status Status der einzubeziehenden Buchungen
         * @param extern ?
         * @param roomId Bei Angabe: Auswertung nur für angegebenen Raum, sonst für alle existierende
         */
        this.fetchAverageRoomUtilizationHours = function (successFn, params) {

            var urls = [
                '/api/report/getavgcountallrooms',
                '/api/report/getavgcountsingleroom'
            ];

            var urlParams = parseToUrlParams(params);

            var url = urls[0];
            if (urlParams.roomId) {
                url = urls[1];
            }
            fetchData(successFn, url, urlParams);

        };

        /**
         * Monatliche Nutzung eines Raums in Stunden
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         * @param beginDate Datum des Beginns ab dem die Auswertung stattfinden soll.
         * @param endDate Datum des Endes bis zu dem die Auswertung stattfinden soll.
         * @param status Status der einzubeziehenden Buchungen
         * @param extern ?
         * @param roomId Raum für den die Stunden zurück gegeben werden sollen.
         */
        this.fetchMonthlyUsageSingleRoom = function (successFn, params) {

            var urlParams = parseToUrlParams(params);
            var url = '/api/report/getmonthhourssingleroom';

            fetchData(successFn, url, urlParams);
        };

        /**
         * Auslastung in Prozent eines Raums
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         * @param beginDate Datum des Beginns ab dem die Auswertung stattfinden soll.
         * @param endDate Datum des Endes bis zu dem die Auswertung stattfinden soll.
         * @param status Status der einzubeziehenden Buchungen
         * @param extern ?
         * @param roomId Raum für den die Stunden zurück gegeben werden sollen.
         */
        this.fetchUtilizationPercentage = function (successFn, params) {

            var urlParams = parseToUrlParams(params);
            var url = '/api/report/getdailyutilizationsingleroom';

            fetchData(successFn, url, urlParams);

        };

        function fetchData(successFn, url, data) {

            data.nocache = new Date().getTime();

            $http.get(url, {
                params: data
            }).then(function success(response) {
                //console.debug(response.data);

                if (successFn)
                    successFn(response);
            }, function failure(response) {

            });
        }

        function parseToUrlParams(params) {
            var urlParams = {};

            console.debug(params);
            urlParams.beginDate = requestDateFactory.beginDate(params.beginDate).getTime();
            urlParams.endDate = requestDateFactory.endDate(params.endDate).getTime();

            if (params.status !== null) {
                urlParams.status = params.status;
            }
            if (params.extern !== null) {
                urlParams.extern = params.extern;
            }
            if (params.roomId !== null) {
                urlParams.roomId = params.roomId;
            }
            console.debug(urlParams);
            return urlParams;
        }

        /**
         * Top 3 Räume
         * @param successFn Callback-Funktion die nach Erhalt des Response aufgerufen wird
         */
        this.fetchUTopThree = function (successFn) {

            var url = '/api/report/getcounttopthreerooms';

            $http.get(url, {
                params: {nocache: new Date().getTime()}
            }).then(function success(response) {
                console.debug(response.data);

                if (successFn)
                    successFn(response);
            }, function failure(response) {

            });

        };


    }]);

    reportsModule.factory('requestDateFactory', [function () {
        return {
            beginDate: function (date) {
                var begin_Date = new Date(date.getTime());
                begin_Date.setHours(0);
                begin_Date.setMinutes(0);
                begin_Date.setSeconds(0);
                begin_Date.setMilliseconds(0);
                return begin_Date;
            },
            endDate: function (date) {
                var begin_Date = new Date(date.getTime());
                begin_Date.setHours(23);
                begin_Date.setMinutes(59);
                begin_Date.setSeconds(59);
                begin_Date.setMilliseconds(999);
                return begin_Date;
            }
        }
    }]);

})();