/**
 * Created by Jan on 10.01.2016.
 */
(function () {

    var reportsModule = angular.module('app.admin.reports');

    reportsModule.controller('ReportsController', ['$scope', '$anchorScroll', '$location', 'reportDataService', 'appService', 'roomManagerService', '$filter', function ($scope, $anchorScroll, $location, reportDataService, appService, roomManagerService, $filter) {


        //<editor-fold desc="Initialisierung">

        $scope.reportLoading = false;

        $scope.reportChart = {};
        $scope.selectors = {};
        $scope.selectors.art = [
            {id: 1, text: 'Alle Räume'},
            {id: 2, text: 'Einzelner Raum'}
        ];
        $scope.selectors.criteria = [
            {id: 1, text: 'Anzahl der Buchungen', roomOnly: false, apicall: reportDataService.fetchBookingCount},
            {
                id: 2,
                text: 'Durchschnittliche Anzahl an Buchungen',
                roomOnly: false,
                apicall: reportDataService.fetchAverageBookingCount
            },
            {id: 3, text: 'Auslastung in Stunden', roomOnly: false, apicall: reportDataService.fetchRoomUtilization},
            {
                id: 4,
                text: 'Durchschnittliche Auslastung in h',
                roomOnly: false,
                apicall: reportDataService.fetchAverageRoomUtilizationHours
            },
            {id: 5, text: 'Monatliche Stunden', roomOnly: true, apicall: reportDataService.fetchMonthlyUsageSingleRoom},
            {id: 6, text: 'Auslastung in %', roomOnly: true, apicall: reportDataService.fetchUtilizationPercentage}
        ];
        $scope.selectors.status = appService.getAllBookingStatus();
        $scope.selectors.status.push({id: 0, description: 'Alle'});

        $scope.selectors.extern = [
            {id: 1, text: 'Intern + Extern', value: null},
            {id: 2, text: 'Intern', value: 0},
            {id: 3, text: 'Extern', value: 1}
        ];

        $scope.selectors.types = [
            {
                id: 1, text: 'Liniendiagramm', charttype: 'Line'
            },
            {
                id: 2, text: 'Balkendiagramm', charttype: 'Bar'
            }
        ];

        $scope.selectors.years = [
            {
                id: 1,
                value: 2015,
                begin: new Date(2015, 0, 1),
                end: new Date(2015, 11, 31)
            }, {
                id: 2, value: 2016, begin: new Date(2016, 0, 1),
                end: new Date(2016, 11, 31)
            },
            {
                id: 3, value: 2017, begin: new Date(2017, 0, 1),
                end: new Date(2017, 11, 31)
            }];

        $scope.selectors.quartals = [
            {id: 1, text: '1. Quartal', begin: new Date(2016, 0, 1), end: new Date(2016, 2, 31)},
            {id: 2, text: '2. Quartal', begin: new Date(2016, 3, 1), end: new Date(2016, 5, 30)},
            {id: 3, text: '3. Quartal', begin: new Date(2016, 6, 1), end: new Date(2016, 8, 30)},
            {id: 4, text: '4. Quartal', begin: new Date(2016, 9, 1), end: new Date(2016, 11, 31)}];

        $scope.selectors.periods = [
            {id: 1, text: 'Jahresauswertung'},
            {id: 2, text: 'Quartalsauswertung'},
            {id: 3, text: 'Benutzerdefiniert'}
        ];

        $scope.activeSelectors = {};
        $scope.activeSelectors.art = $scope.selectors.art[0];
        $scope.activeSelectors.criteria = $scope.selectors.criteria[0];
        $scope.activeSelectors.status = $filter('orderBy')($scope.selectors.status, 'id', false)[0];
        $scope.activeSelectors.extern = $scope.selectors.extern[0];
        $scope.activeSelectors.type = $scope.selectors.types[0];
        $scope.activeSelectors.period = $scope.selectors.periods[0];
        $scope.activeSelectors.periodSubSelection = {};
        $scope.activeSelectors.periodSubSelection.year = $scope.selectors.years[1];
        $scope.activeSelectors.periodSubSelection.quartal = $scope.selectors.quartals[0];
        $scope.activeSelectors.periodSubSelection.endDate = today();
        $scope.activeSelectors.periodSubSelection.beginDate = today();


        $scope.format = 'dd.MM.yyyy';
        $scope.minDate = today();
        $scope.openedDatePickerBeginTime = false;
        $scope.openedDatePickerEndTime = false;
        $scope.buttonBar = false;

        function today() {
            var _today = new Date();
            _today.setHours(0);
            _today.setMinutes(0);
            _today.setSeconds(0);
            _today.setMilliseconds(0);
            return _today;
        }

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };

        $scope.openDatePickerBeginTime = function () {
            $scope.openedDatePickerBeginTime = true;
        };

        $scope.openDatePickerEndTime = function () {
            $scope.openedDatePickerEndTime = true;
        };

        $scope.chartLabels = [];
        $scope.chartData = [];
        $scope.chartType = 'line';

        //</editor-fold>

        $scope.isCriteriaDisabled = function (crit) {
            return !!(crit.roomOnly && $scope.activeSelectors.art.id == 1);
        };

        $scope.isQuartal = function () {
            return $scope.activeSelectors.period.id == $scope.selectors.periods[1].id;
        };

        $scope.isUserDefined = function () {
            return $scope.activeSelectors.period.id == $scope.selectors.periods[2].id;
        };

        $scope.changed = function () {
            if ($scope.activeSelectors.art.id === 2) {
                $scope.selectors.rooms = roomManagerService.fetchRoomData(function (response) {
                    $scope.selectors.rooms = roomManagerService.getRoomData();
                    $scope.activeSelectors.room = $filter('orderBy')($scope.selectors.rooms, 'name', false)[0];
                });
                $scope.activeSelectors.type = $scope.selectors.types
                    [1];
            }
            if ($scope.activeSelectors.criteria.roomOnly) {
                $scope.activeSelectors.criteria = $scope.selectors.criteria[0];
            }
        };

        // Optionen definieren für das Chart
        $scope.options = {
            scaleShowGridLines: true,
            responsive: true,
            scaleGridLineColor: "rgba(102, 102, 102, 0.3)",
            // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4
        };

        // Farben definieren für das Chart
        Chart.defaults.global.colours = [
            {
                fillColor: "rgba(51, 51, 51, 0.20)",
                strokeColor: "#B11A3B",
                pointColor: "#fff",
                pointStrokeColor: "#B11A3B"
            }
        ];

        $scope.evaluateReport = function () {

            //laden
            $scope.reportLoading = true;

            var activeSelectors = $scope.activeSelectors;

            //$scope.chartType = 'chart-base';

            var params = {};
            //Set Period
            switch ($scope.activeSelectors.period.id) {
                case 1:
                    params.beginDate = $scope.activeSelectors.periodSubSelection.year.begin;
                    params.endDate = $scope.activeSelectors.periodSubSelection.year.end;
                    break;
                case 2:
                    params.beginDate = $scope.activeSelectors.periodSubSelection.quartal.begin;
                    params.endDate = $scope.activeSelectors.periodSubSelection.quartal.end;

                    params.beginDate.setYear($scope.activeSelectors.periodSubSelection.year.value);
                    params.endDate.setYear($scope.activeSelectors.periodSubSelection.year.value);
                    break;
                case 3:
                    params.beginDate = $scope.activeSelectors.periodSubSelection.beginDate;
                    params.endDate = $scope.activeSelectors.periodSubSelection.endDate;
                    break;
                default:
                    params.beginDate = new Date(2016, 0, 1);
                    params.endDate = new Date(2016, 11, 31);
            }

            params.extern = $scope.activeSelectors.extern.value;
            params.status = ($scope.activeSelectors.status.id === 0) ? null : $scope.activeSelectors.status.id;
            params.roomId = ($scope.activeSelectors.art.id == 2) ? $scope.activeSelectors.room.roomID : null;

            console.debug(params);

            activeSelectors.criteria.apicall(function successResponse(response) {
                var responseData = response.data;

                var labels = [];
                var diagramData = [];
                responseData.forEach(function (item) {
                    labels.push(item.label);
                    diagramData.push(item.value);
                });

                $scope.chartLabels = labels;
                $scope.chartData = [diagramData];

                $scope.chartType = activeSelectors.type.charttype;

                $location.hash('reportChart');
                $anchorScroll();
                $scope.reportLoading = false;

            }, params);

            $scope.$watch('activeSelectors.type.charttype', function () {
                $scope.chartType = $scope.activeSelectors.type.charttype;
            })

        };
    }]);


})();