/**
 * Created by Jan on 08.01.2016.
 */


(function () {
    'use strict';

    var adminModule = angular.module('app.admin');

    adminModule.service('reservationManagerService', ['$http', '$log', 'bookingFactory', function ($http, $log, bookingFactory) {

        var temporalySavedBookings = [];


        function fetchBookings(successCallback) {

            $http.get('/api/booking/getbystatus/1', {
                params: {
                    nocache: new Date().getTime()
                }
            }).then(function success(response) {
                var bookingList = [];
                response.data.forEach(function (booking) {
                    bookingList.push(bookingFactory.getByResponse(booking));
                });

                temporalySavedBookings = bookingList;

                successCallback(response);
            }, function failureCallback() {
            });
            return temporalySavedBookings;
        }

        function approveBooking(bookingID, successCallback, failureCallback) {
            if (isBookingInList(bookingID)) {
                $http.post('/api/booking/approve/' + bookingID, {}).then(function (response) {

                    removeFromBookings(bookingID);

                    successCallback(response);
                }, failureCallback);
            }
        }

        function declineBooking(bookingID, successCallback, failureCallback) {
            if (isBookingInList(bookingID)) {

                $http.post('/api/booking/decline/' + bookingID, {}).then(function success(response) {

                    removeFromBookings(bookingID);

                    successCallback(response);
                }, failureCallback);
            }
        }

        function getTempSavedBookings() {
            return temporalySavedBookings;
        }

        function removeFromBookings(bookingID) {
            temporalySavedBookings.forEach(function (item, index) {
                if (item.bookingID === bookingID) {
                    temporalySavedBookings.splice(index, 1);
                }
            });
        }

        function isBookingInList(bookingId) {
            var erg = false;
            temporalySavedBookings.forEach(function (item) {
                if (item.bookingID === bookingId) {
                    erg = true;
                }
            });

            return erg;
        }

        function getBooking(bookingId) {
            var erg = false;
            temporalySavedBookings.forEach(function (item) {
                if (item.bookingID === bookingId) {
                    erg = item;
                }
            });
            return erg;
        }

        function _changeBooking(bookingID, roomID, successFn, failureFn) {
            $http.post('/api/booking/update', {
                data: {
                    bookingId: bookingID,
                    roomId: roomID
                }
            }).then(successFn, failureFn);
        }

        return {
            fetchBookings: fetchBookings,
            approveBooking: approveBooking,
            declineBooking: declineBooking,
            getBookings: getTempSavedBookings,
            getBooking: getBooking,
            changeBooking: _changeBooking
        }
    }]);

    adminModule.factory('bookingFactory', [function () {
        return {
            getByResponse: function (booking) {
                var clientBooking = {};

                clientBooking.bookingID = booking.cla_bookingId;
                clientBooking.description = booking.cla_description;
                clientBooking.memberCount = booking.cla_memberCount;
                clientBooking.status = booking.cla_status.id;
                clientBooking.costcenter = booking.costCenter;

                clientBooking.date = {
                    begin: booking.cla_beginDate,
                    end: booking.cla_endDate
                };


                clientBooking.room = {
                    roomID: booking.cla_room.cla_roomId,
                    name: booking.cla_room.cla_name,
                    description: booking.cla_room.cla_description,
                    capacity: booking.cla_room.cla_capacity
                };

                clientBooking.seatingID = booking.cla_seating.cla_seatingId;

                clientBooking.requester = {
                    userID: booking.cla_user.cla_userId,
                    firstname: booking.cla_user.cla_firstname,
                    surname: booking.cla_user.cla_surname,
                    gender: booking.cla_user.cla_gender,
                    mail: booking.cla_user.cla_mail,
                    phone: booking.cla_user.cla_phone,
                    department: {
                        departmentID: booking.cla_user.cla_department.cla_departmentId,
                        name: booking.cla_user.cla_department.cla_name
                    }
                };


                if (booking.cla_extern) {
                    clientBooking.extern = true;
                }

                if (booking.cla_service) {
                    clientBooking.service = true;
                }
                return clientBooking;
            }

        }
    }]);

    adminModule.service('roomManagerService', ['$http', '$log', 'roomFactory', function ($http, $log, roomFactory) {

        var roomList = [];

        this.fetchRoomData = function (successFn) {
            $http.get('/api/room/list', {
                params: {nocache: new Date().getTime()}
            }).then(function success(response) {
                roomList = [];
                response.data.forEach(function (item) {
                    roomList.push(roomFactory.getRoomByResponse(item));
                });

                if (successFn)
                    successFn(response);

            }, function failure(response) {

            });

            return roomList;
        };

        this.fetchPossibleRoomData = function (bookingID, successFn) {
            $http.get('/api/room/getpossiblerooms/' + bookingID, {
                params: {
                    nocache: new Date().getTime()
                }
            }).then(function success(response) {
                var possibleRooms = [];

                response.data.forEach(function (item) {
                    possibleRooms.push({
                        roomID: item.cla_roomId,
                        name: item.cla_name,
                        description: item.cla_description,
                        capacity: item.cla_capacity
                    });
                });

                successFn(possibleRooms);

            }, function failure(resonse) {

            });
        };

        this.getRoomData = function () {
            return roomList;
        };

        this.fetchFixEquipmentForRoom = function (roomID, successFn) {
            $http.get('/api/room/fixedequipments/' + roomID, {
                params: {nocache: new Date().getTime()}
            }).then(function success(response) {

                var fixEquips = [];
                response.data.forEach(function (item) {
                    fixEquips.push(item.cla_idFixEquipment);
                });


                if (successFn)
                    successFn(response, fixEquips);
            }, function failure(response) {

            });
        };

        this.fetchPossibleSeatingsForRoom = function (roomID, successFn) {
            $http.get('/api/room/possibleseatings/' + roomID, {
                params: {nocache: new Date().getTime()}
            }).then(function success(response) {

                var seatings = [];
                response.data.forEach(function (item) {
                    seatings.push(item.cla_seatingId);
                });


                if (successFn)
                    successFn(response, seatings);
            }, function failure(response) {

            });
        };

        this.addRoom = function (roomName, roomDesc, roomCapacity, roomPossibleSeatings, roomFixEquipment, successFn, failureFn) {

            $http.post('/api/room/create', {
                data: {
                    roomName: roomName,
                    description: roomDesc,
                    capacity: roomCapacity,
                    seatings: roomFactory.getPossibleSeatingsByObject(roomPossibleSeatings),
                    fixedEquipment: roomFactory.getFixedEquipmentByObject(roomFixEquipment)
                }
            }).then(function success(response) {
                if (successFn)
                    successFn(response);
            }, function failue(response) {
                if (failureFn)
                    failureFn(response);
            })
        };

        this.updateRoom = function (roomID, roomName, roomDesc, roomCapacity, roomPossibleSeatings, roomFixEquipment, successFn, failureFn) {
            $http.post('/api/room/edit/' + roomID, {
                data: {
                    roomName: roomName,
                    description: roomDesc,
                    capacity: roomCapacity,
                    seatings: roomFactory.getPossibleSeatingsByObject(roomPossibleSeatings),
                    fixedEquipments: roomFactory.getFixedEquipmentByObject(roomFixEquipment)
                }
            }).then(function success(response) {
                if (successFn)
                    successFn(response);
            }, function failue(response) {
                if (failureFn)
                    failureFn(response);
            });
        };

        this.deleteRoom = function (roomID, successFn, failureFn) {
            $http.post('/api/room/deactivate/' + roomID, {}).then(successFn, failureFn);
        }

    }]);

    adminModule.factory('roomFactory', [function () {
        return {
            getRoomByResponse: function (room) {
                return {
                    roomID: room.cla_roomId,
                    name: room.cla_name,
                    description: room.cla_description,
                    capacity: room.cla_capacity
                };
            },
            getPossibleSeatingsByObject: function (seatingsObject) {
                var erg = {};
                var counter = 1;
                seatingsObject.forEach(function (item) {
                    erg['id' + counter] = item.id;
                    counter++;
                });
                return erg;
            },
            getFixedEquipmentByObject: function (fixedEquipment) {
                var erg = {};
                var counter = 1;
                fixedEquipment.forEach(function (item) {
                    erg['id' + counter] = item.id;
                    counter++;
                });
                return erg;
            }
        }
    }]);

    adminModule.service('bookingManagerService', ['$http', '$log', 'bookingFactory', function ($http, $log, bookingFactory) {

        var bookings = {
            approved: [],
            declined: [],
            changed: []
        };

        function _fetchApprovedBookings(successFn) {
            $http.get('/api/booking/getbystatus/2', {
                params: {nocache: new Date().getTime()}
            }).then(function success(response1) {
                var bookingList = [];
                response1.data.forEach(function (booking) {
                    bookingList.push(bookingFactory.getByResponse(booking));
                });
                bookings.approved = bookingList;
                if (successFn)
                    successFn();

            }, function failureCallback() {
            });
        }

        function _fetchDeclinedBookings(successFn) {
            $http.get('/api/booking/getbystatus/3', {
                params: {nocache: new Date().getTime()}
            }).then(function success(response2) {
                var bookingList = [];
                response2.data.forEach(function (booking) {
                    bookingList.push(bookingFactory.getByResponse(booking));
                });
                bookings.declined = bookingList;
                if (successFn)
                    successFn();

            }, function failure() {
            });
        }

        function _fetchChangedBookings(successFn) {
            $http.get('/api/booking/getbystatus/4', {
                params: {nocache: new Date().getTime()}
            }).then(function success(response3) {
                var bookingList = [];
                response3.data.forEach(function (booking) {
                    bookingList.push(bookingFactory.getByResponse(booking));
                });

                bookings.changed = bookingList;
                if (successFn)
                    successFn();
            }, function failure() {

            });
        }


        function _fetchBookings(successCallback) {
            _fetchApprovedBookings(successCallback);
            _fetchChangedBookings(successCallback);
            _fetchDeclinedBookings(successCallback);

            return _getTempSavedBookings();
        }

        function _getTempSavedBookings() {
            var bookingList = [];

            bookings.changed.forEach(function (item) {
                bookingList.push(item);
            });
            bookings.approved.forEach(function (item) {
                bookingList.push(item);
            });
            bookings.declined.forEach(function (item) {
                bookingList.push(item);
            });

            return bookingList;
        }

        function isBookingInList(bookingId) {
            var erg = false;
            _getTempSavedBookings().forEach(function (item) {
                if (item.bookingID === bookingId) {
                    erg = true;
                }
            });

            return erg;
        }

        function _getBooking(bookingId) {
            var erg = false;
            _getTempSavedBookings().forEach(function (item) {
                if (item.bookingID === bookingId) {
                    erg = item;
                }
            });
            return erg;
        }


        this.fetchBookings = function (successFn) {
            return _fetchBookings(successFn);
        };

        this.getBookings = function () {
            return _getTempSavedBookings();
        };

        this.getBooking = function (bookingID) {
            return _getBooking(bookingID);
        };

    }]);

})();