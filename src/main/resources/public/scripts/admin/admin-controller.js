/**
 * Created by Jan on 08.01.2016.
 */


(function () {
    'use strict';

    var adminModule = angular.module('app.admin');


    adminModule.controller('ReservationAdminController', ['$scope', 'reservationManagerService', 'appService', '$uibModal', function ($scope, reservationManagerService, appService, $uibModal) {

        $scope.approve = function (bookingId) {
            reservationManagerService.approveBooking(bookingId, function success(response) {
                $scope.bookings = reservationManagerService.getBookings();
                appService.addAlert({
                    type: 'info',
                    msg: 'Buchung wurde angenommen.',
                    'timeout': 5000
                });
            }, function failure(response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    'timeout': 5000
                });
            })
        };

        $scope.decline = function (bookingId) {
            reservationManagerService.declineBooking(bookingId, function success(response) {
                $scope.bookings = reservationManagerService.getBookings();
                appService.addAlert({
                    type: 'info',
                    msg: 'Buchung wurde abgelehnt.',
                    'timeout': 5000
                });

            }, function failure(response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    'timeout': 5000
                });
            });
        };

        $scope.openBookingDetailModal = function (bookingID) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'detailReservationView.html',
                controller: 'DetailModalInstanceCtrl',
                resolve: {
                    reservation: reservationManagerService.getBooking(bookingID)
                }
            });
            modalInstance.result.then(function () {
                //Update
            }, function () {
                //Dissmiss
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openBookingEditModal = function (booking) {
            var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'ChangeBooking.html',
                    controller: 'EditBookingModalInstanceCtrl',
                    resolve: {
                        reservation: booking
                    }
                })
                ;
            modalInstance.result.then(function () {
                //Update
                $scope.bookings = reservationManagerService.fetchBookings(function successCallback(response) {
                    $scope.bookings = reservationManagerService.getBookings();
                });
            }, function () {
                //Dissmiss
            });
        };


        $scope.bookings = reservationManagerService.fetchBookings(function successCallback(response) {
            $scope.bookings = reservationManagerService.getBookings();
        });

    }]);

    adminModule.controller('DetailModalInstanceCtrl', ['$scope', '$uibModalInstance', 'reservation', function ($scope, $uibModalInstance, reservation) {

        $scope.reservation = reservation;

        $scope.ok = function () {


            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

    adminModule.controller('EditBookingModalInstanceCtrl', ['$scope', '$uibModalInstance', '$filter', 'roomManagerService', 'reservation', 'appService', 'reservationManagerService', function ($scope, $uibModalInstance, $filter, roomManagerService, reservation, appService, reservationManagerService) {

        $scope.reservation = reservation;
        updateRoomList();

        function updateRoomList() {
            $scope.rooms = roomManagerService.fetchRoomData(function () {
                roomManagerService.fetchPossibleRoomData(reservation.bookingID, function (rooms) {
                    $scope.rooms = rooms;
                    $scope.selectedRoom = $filter('orderBy')(rooms, 'name', false)[0];
                });
            });
        }

        $scope.submit = function () {
            var oldRoomID = $scope.reservation.room.roomID;
            var newRoomID = $scope.selectedRoom.roomID;

            if (oldRoomID === newRoomID) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Es kann nicht der gleiche Raum gewählt werden.',
                    timeout: 5000
                });
                return;
            }

            reservationManagerService.changeBooking($scope.reservation.bookingID, newRoomID,
                function success(response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Raum wurde erfolgreich geändert',
                        timeout: 5000
                    });
                    $uibModalInstance.close();
                }, function failure(response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });

                });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    ]);

    adminModule.controller('AdminMenuController', ['$scope', '$state', function ($scope, $state) {
        $scope.menu = [
            {
                status: 'admin.reservationmanager',
                description: 'Reservierungen',
                symbol: 'file'
            }, {
                status: 'admin.bookingmanager',
                description: 'Buchungen',
                symbol: 'book'
            }, {
                status: 'admin.roommanager',
                description: 'Räume verwalten',
                symbol: 'home'
            }, {
                status: 'admin.equipmentmanager',
                description: 'Equipment verwalten',
                symbol: 'blackboard'
            }, {
                status: 'admin.reports',
                description: 'Berichte',
                symbol: 'stats'
            }
        ];

        $scope.isSelected = function (id) {
            return $state.current.name === $scope.menu[id].status;
        }

    }]);

    adminModule.controller('RoomManagerController', ['$scope', '$filter', 'roomManagerService', '$uibModal', function ($scope, $filter, roomManagerService, $uibModal) {

        updateRoomList();
        $scope.selectedRoomChanged = fetchAdditionalRoomData;


        function fetchAdditionalRoomData() {
            roomManagerService.fetchFixEquipmentForRoom($scope.selectedRoom.roomID, function (response, fixEquip) {
                $scope.selectedRoom.fixedEquipments = fixEquip;
            });
            roomManagerService.fetchPossibleSeatingsForRoom($scope.selectedRoom.roomID, function (response, seatings) {
                $scope.selectedRoom.possibleSeatings = seatings;
            });
        }

        $scope.openAddRoomModal = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'roomAdd.html',
                controller: 'AddRoomModalInstanceCtrl'
            });

            modalInstance.result.then(function () {
                //Create Room
                updateRoomList();
            }, function () {
                //Dissmiss
            });
        };

        $scope.openEditRoomModal = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'roomAdd.html',
                controller: 'EditRoomModalInstanceCtrl',
                resolve: {
                    room: $scope.selectedRoom
                }
            });

            modalInstance.result.then(function () {
                //Update Room
                updateRoomList();
            }, function () {
                //Dissmiss
            });
        };

        $scope.openDeleteRoomModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'roomDelete.html',
                controller: 'DeleteRoomModalInstanceCtrl',
                resolve: {
                    room: $scope.selectedRoom
                }
            });

            modalInstance.result.then(function () {
                //Deleted Room
                updateRoomList();
            }, function () {
                //Dissmiss
            });
        };

        function updateRoomList() {
            $scope.rooms = roomManagerService.fetchRoomData(function () {
                var rooms = roomManagerService.getRoomData();
                $scope.rooms = rooms;
                $scope.selectedRoom = $filter('orderBy')(rooms, 'name', false)[0];
                fetchAdditionalRoomData();
            });
        }

    }]);

    adminModule.controller('BookingManagerController', ['$scope', '$filter', '$uibModal', 'dashboardNewsService', 'bookingManagerService', function ($scope, $filter, $uibModal, dashboardNewsService, bookingManagerService) {

        $scope.bookings = bookingManagerService.fetchBookings(function success(response) {
            $scope.bookings = bookingManagerService.getBookings();
        });

        $scope.filterDate = today();
        $scope.format = 'dd.MM.yyyy';
        $scope.minDate = today();
        $scope.opened = false;
        $scope.buttonBar = false;

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };

        $scope.open = function () {
            if ($scope.filter) {
                $scope.opened = true;
            }
        };

        $scope.$watch('filter', function () {
            if (!$scope.filter)
                $scope.bookings = bookingManagerService.getBookings();
            if ($scope.filter) {
                $scope.filterDate = today();
            }
        });

        $scope.filterBookings = function () {
            if ($scope.filter) {
                $scope.bookings = $filter('atDate')(bookingManagerService.getBookings(), $scope.filterDate);
            } else {
                $scope.bookings = bookingManagerService.getBookings();
            }
        };

        $scope.isDisabled = function () {
            return !$scope.filter;
        };

        $scope.isCanceled = function (booking) {
            return booking.status === 3;
        };

        $scope.openInfoModal = function (bookingitem) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'seeBookingInfo.html',
                controller: 'InfoModalBookingInstanceCtrl',
                resolve: {
                    booking: bookingitem
                }
            });

            modalInstance.result.then(function () {
            }, function () {
                //Dismiss
            });
        };

        $scope.openEditModal = function (bookingitem) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ChangeBooking.html',
                controller: 'EditBookingModalInstanceCtrl',
                resolve: {
                    reservation: bookingitem
                }
            });

            modalInstance.result.then(function () {
                //Update
                $scope.bookings = bookingManagerService.fetchBookings(function success(response) {
                    $scope.bookings = bookingManagerService.getBookings();
                });

            }, function () {
                //Dissmiss
            });
        };

        $scope.setBufferTime = function () {

        };


        function today() {
            var _today = new Date();
            _today.setHours(0);
            _today.setMinutes(0);
            _today.setSeconds(0);
            _today.setMilliseconds(0);
            return _today;
        }

    }]);

    adminModule.controller('InfoModalBookingInstanceCtrl', ['$scope', '$uibModalInstance', 'booking', function ($scope, $uibModalInstance, booking) {

        $scope.booking = booking;

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

    adminModule.controller('AddRoomModalInstanceCtrl', ['$scope', '$uibModalInstance', 'roomManagerService', 'appService', function ($scope, $uibModalInstance, roomManagerService, appService) {
        $scope.seatings = {};
        $scope.seatings.$error = false;
        $scope.function = 'hinzufügen';
        $scope.room = {};
        $scope.possibleSeatings = appService.getAllSeatings();
        $scope.fixedEquipments = appService.getAllFixedEquipments();
        $scope.fieldsDisabled = false;


        $scope.seatingChanged = function (seating) {
            if (seating.selected) {
                $scope.seatings.$error = false;
            } else {
                var minOneSelected = false;
                $scope.possibleSeatings.forEach(function (item) {
                    minOneSelected = item.selected == true ? true : minOneSelected;
                });

                $scope.seatings.$error = !minOneSelected;
            }
        };

        $scope.submit = function () {

            if (isFormularValid()) {
                disableAllFields();
                roomManagerService.addRoom($scope.room.name, $scope.room.description, $scope.room.capacity, selectedSeatings(), selectedFixEquipment(), function success(response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Der Raum \"' + $scope.room.name + '\" wurde erfolgreich erstellt.',
                        timeout: 5000
                    });
                    enableAllFields();
                    $uibModalInstance.close();
                }, function failure(response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });
                    enableAllFields();
                });

            } else {
                appService.addAlert({
                    type: 'warning',
                    msg: 'Das Formular wurde nicht richtig ausgefüllt!',
                    timeout: 4000
                });
            }


        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function disableAllFields() {
            $scope.fieldsDisabled = true;
        }

        function enableAllFields() {
            $scope.fieldsDisabled = false;
        }

        function isFormularValid() {
            if (angular.equals({}, $scope.roomFormular.$error)) {
                if ($scope.room.capacity >= 2 && $scope.room.capacity <= 40) {
                    if (selectedSeatings().length > 0) {
                        return true;
                    }
                }
            }
            return false;
        }

        function selectedSeatings() {
            var pSeatings = [];
            $scope.possibleSeatings.forEach(function (item) {
                if (item.selected) {
                    pSeatings.push(item);
                }
            });
            return pSeatings;
        }

        function selectedFixEquipment() {
            var tFixEquipment = [];
            $scope.fixedEquipments.forEach(function (item) {
                if (item.selected) {
                    tFixEquipment.push(item);
                }
            });
            return tFixEquipment;
        }
    }]);

    adminModule.controller('EditRoomModalInstanceCtrl', ['$scope', '$uibModalInstance', 'roomManagerService', 'appService', 'room', function ($scope, $uibModalInstance, roomManagerService, appService, room) {
        $scope.seatings = {};
        $scope.seatings.$error = false;
        $scope.function = 'bearbeiten';
        $scope.room = angular.copy(room);
        $scope.possibleSeatings = appService.getAllSeatings();
        $scope.fixedEquipments = appService.getAllFixedEquipments();
        $scope.fieldsDisabled = false;
        initializeSeatingAndEquipmentSelection();


        $scope.seatingChanged = function (seating) {
            if (seating.selected) {
                $scope.seatings.$error = false;
            } else {
                var minOneSelected = false;
                $scope.possibleSeatings.forEach(function (item) {
                    minOneSelected = item.selected == true ? true : minOneSelected;
                });

                $scope.seatings.$error = !minOneSelected;
            }
        };

        console.debug(room);

        $scope.submit = function () {

            if (isFormularValid()) {
                disableAllFields();
                roomManagerService.updateRoom($scope.room.roomID, $scope.room.name, $scope.room.description, $scope.room.capacity, selectedSeatings(), selectedFixEquipment(), function success(response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Der Raum \"' + $scope.room.name + '\" wurde erfolgreich bearbeitet.',
                        timeout: 5000
                    });
                    enableAllFields();
                    $uibModalInstance.close();
                }, function failure(response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });
                    enableAllFields();
                });

            } else {
                appService.addAlert({
                    type: 'warning',
                    msg: 'Das Formular wurde nicht richtig ausgefüllt!',
                    timeout: 4000
                });
            }


        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function disableAllFields() {
            $scope.fieldsDisabled = true;
        }

        function enableAllFields() {
            $scope.fieldsDisabled = false;
        }

        function isFormularValid() {
            if (angular.equals({}, $scope.roomFormular.$error)) {
                if ($scope.room.capacity >= 2 && $scope.room.capacity <= 40) {
                    if (selectedSeatings().length > 0) {
                        return true;
                    }
                }
            }
            return false;
        }

        function selectedSeatings() {
            var pSeatings = [];
            $scope.possibleSeatings.forEach(function (item) {
                if (item.selected) {
                    pSeatings.push(item);
                }
            });
            return pSeatings;
        }

        function selectedFixEquipment() {
            var tFixEquipment = [];
            $scope.fixedEquipments.forEach(function (item) {
                if (item.selected) {
                    tFixEquipment.push(item);
                }
            });
            return tFixEquipment;
        }

        function initializeSeatingAndEquipmentSelection() {
            $scope.room.possibleSeatings.forEach(function (item) {
                $scope.possibleSeatings.forEach(function (checkBoxItem) {
                    if (item === checkBoxItem.id) {
                        checkBoxItem.selected = true;
                    }
                })
            });

            $scope.room.fixedEquipments.forEach(function (item) {
                $scope.fixedEquipments.forEach(function (checkBoxItem) {
                    if (item === checkBoxItem.id) {
                        checkBoxItem.selected = true;
                    }
                })
            });
        }
    }]);

    adminModule.controller('DeleteRoomModalInstanceCtrl', ['$scope', '$uibModalInstance', 'roomManagerService', 'appService', 'room', function ($scope, $uibModalInstance, roomManagerService, appService, room) {
        $scope.function = 'löschen';
        $scope.room = angular.copy(room);

        $scope.submit = function () {

            if ($scope.accept) {

                roomManagerService.deleteRoom($scope.room.roomID, function success(response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Der Raum \"' + $scope.room.name + '\" wurde erfolgreich gelöscht.',
                        timeout: 5000
                    });
                    $uibModalInstance.close();
                }, function failure(response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });
                });

            } else {
                appService.addAlert({
                    type: 'warning',
                    msg: 'Sie müssen bestätigen dass der Raum gelöscht werden soll.',
                    timeout: 4000
                });
            }


        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);


})();