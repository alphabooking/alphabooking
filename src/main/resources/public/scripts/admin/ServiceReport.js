/**
 * Created by fbrec on 18.01.2016.
 */

// Globale Variabeln
var globalstartdatum;
var globalenddatum;

// Formular generieren------------------------------------
window.createformular = function(e){

    var outputdropdown;


    switch(e.value) {

        case "01":
            outputdropdown = '<b>Jahr</b>'+'</br><select id="year" class="form-control"></select>';
            document.getElementById("variableAuswahl").innerHTML = outputdropdown;
            createyearlist();
            break;

        case "02":
            outputdropdown ='<b>Jahr</b>'+'</br><select id="year" class="form-control"></select>' +'</br>'+ '<b>Quartal</b>'+'</br><select name="quartal" id="quartal" onchange="" size="1" class="form-control"> <option value="01">1. Quartal</option> <option value="02">2. Quartal</option> <option value="03">3. Quartal</option> <option value="04">4. Quartal</option> </select>'

            document.getElementById("variableAuswahl").innerHTML = outputdropdown;
            createyearlist();
            break;

        case "03":
            outputdropdown ='<b>Startdatum:</b>'+' <input type="date" name="startdate" id = "startdate" min="2015-01-01" max="2099-01-01" class="form-control">'+'</br>'+'<b>Enddatum</b>'+'<input type="date" name="enddate" id = "enddate" min="2015-01-01" max="2099-01-01" class="form-control">';

           // outputdropdown ='<table><tr> <td>benutzerdefinierter Zeitraum <p>Beginn:</p> </td><td><input type="text" name="startdatum1" id="startdatum1" maxlength="10" value="01.02.2016" style="width: 100px; float: left; text-align: center;" class="form-control"></td></tr><tr><td><p>Ende:</p> </td> <td> <input type="text" name="enddatum1" id="enddatum1" maxlength="10" value="31.02.2016"  style="width: 100px; float: left; text-align: center;" class="form-control"> </td>  </tr>    </table>'
            document.getElementById("variableAuswahl").innerHTML = outputdropdown;
            break;

        default:
            outputdropdown = '<b>Jahr</b>'+'</br><select id="year" class="form-control"></select>';
            document.getElementById("variableAuswahl").innerHTML = outputdropdown;
            createyearlist();
    }
}

// Auwahl Alle Räume oder einen einzelnen
window.raumart = function(e){

    var outputdropdown;


    switch(e.value) {

        case "0":
            document.getElementById("rooms").innerHTML = "";
            break;

        case "1":
           getallrooms();
            document.getElementById("diagramm").selectedIndex = 1;
            break;



        default:
            document.getElementById("rooms").innerHTML = "";

    }




}

// Fill dropdown list year
function createyearlist(){
    var start = 2015;
    var end = new Date().getFullYear();
    var options = "";
    for(var year = end ; year >=start; year--){
        options += "<option>"+ year +"</option>";
    }
    document.getElementById("year").innerHTML = options;
}

// Generiert Abhängig der Raumauswahl : Alle oder einzelnen Raum unterschiedliche Auswahlen an Abfragen
function createauswertungen(e){

/*
    switch(e.value) {

        case "0":
            outputdropdown ='<b>Auswertung auswählen</b>'+'</br>'+'<select name="criteria" id="criteria" onchange="" size="1"> <option value="01">Anzahl der Buchungen</option><option value="02">Durchschnittliche Anzahl an Buchungen</option> <option value="03">Auslastung in Stunden</option> <option value="04">Durchschnittliche Auslastung in h</option> </select>'+'</br>'+'</br>'
            document.getElementById("criteriaauswahl").innerHTML = outputdropdown;
            break;

        case "1":

            outputdropdown ='<b>Auswertung auswählen</b>'+'</br>'+'<select name="criteria" id="criteria" onchange="" size="1"> <option value="01">Anzahl der Buchungen</option><option value="02">Durchschnittliche Anzahl an Buchungen</option> <option value="03">Auslastung in Stunden</option> <option value="04">Durchschnittliche Auslastung in h</option> <option value="05">Monatliche Stunden</option><option value="06">Auslastung in %</option></select>'+'</br>'+'</br>'
            document.getElementById("criteriaauswahl").innerHTML = outputdropdown;
            break;


        default:
            outputdropdown ='<b>Auswertung auswählen</b>'+'</br>'+'<select name="criteria" id="criteria" onchange="" size="1"> <option value="01">Anzahl der Buchungen</option><option value="02">Durchschnittliche Anzahl an Buchungen</option> <option value="03">Auslastung in Stunden</option> <option value="04">Durchschnittliche Auslastung in h</option> </select>'+'</br>'+'</br>'
            document.getElementById("criteriaauswahl").innerHTML = outputdropdown;
            break;
    }



*/


}

//--------------------------------------------------------

// Top gebuchte Räume anzeigen
function dashboardchart(){

    var returnvalue = api("98");

    createDiagramm(3,"Diagramm2",returnvalue);

}

// Start und Ende Datum auslesen und speichern in der globalen Variable
function builddate(){

    // Definiton Variabeln und Auslesen des Formulares / des Filters



    var e = document.getElementById("zeitraum");
    var zeitraum = e.options[e.selectedIndex].value;

    if(zeitraum=="01"){
        var e = document.getElementById("year");
        var year = e.options[e.selectedIndex].value;
        globalstartdatum = "01.01." + year;
        globalenddatum = "31.12." + year;
    }


else if(zeitraum=="02") {
        var e = document.getElementById("year");
        var year = e.options[e.selectedIndex].value;

        var e = document.getElementById("quartal");
        var quartal = e.options[e.selectedIndex].value;

        if (quartal == "01") {
            globalstartdatum = "01.01." + year;
            globalenddatum = "31.03." + year;
        }


        else if (quartal == "02") {
            globalstartdatum = "01.04." + year;
            globalenddatum = "30.06." + year;
        }

        else if (quartal == "03") {
            globalstartdatum = "01.07." + year;
            globalenddatum = "30.09." + year;
        }


        else if (quartal == "04") {
            globalstartdatum = "01.10." + year;
            globalenddatum = "31.12." + year;
        }

    }

    else{

        var startdate = document.getElementById("startdate").value;
        var enddate = document.getElementById("enddate").value;

        var startdate2=   Date.parse(startdate).toString("dd.MM.yyyy");
        var enddate2=   Date.parse(enddate).toString("dd.MM.yyyy");

        globalstartdatum = startdate2;
        globalenddatum= enddate2;

    }


}

//Funktion um alle Namen der Räume abzufragen um diese im Filter anzuzeigen
function getallrooms() {

    var result =  api("99");


    var outpucheckbox = "";
    var outputdropdown = '</br><b>Raum auswählen:</b>'+'</br><select name="Rooms" id ="Rooms" size="1">';

    var i;
    for(i = 0; i < result.length; i++) {

        outputdropdown +=   '<option value="'+result[i].cla_roomId+'">'+result[i].cla_name+'</option>'


        outpucheckbox += '<label for="' + result[i].cla_roomId + '">' +
            '<input type="checkbox" name="'+ result[i].cla_name +'" value="'+ result[i].cla_name +'" id="' +result[i].cla_roomId +'">' +
            result[i].cla_name +'</label>';

    }
    outputdropdown+='</select>'
   // document.getElementById("id01").innerHTML = outpucheckbox;
    document.getElementById("rooms").innerHTML = outputdropdown;
}

// Funktion für API Call an das Backend
function api(id,startdatum,enddatum,roomid,status,extern) {

    var url;
    var xmlhttp = new XMLHttpRequest();

    if(roomid==0){
    roomid=null;
    }

    if(status==0){
        status=null;
    }

    if(extern==2){
        extern=null;
    }


    switch(id) {
        // Anzahl Buchungen
        case "01":

            if(roomid==null){
                url= "http://localhost:4567/api/report/getcountallrooms?beginDate=" + startdatum + "&endDate=" + enddatum + "&status=" + status+ "&extern=" + extern;
            }
            else {
                url = "http://localhost:4567/api/report/getcountsingleroom?beginDate=" + startdatum + "&endDate=" + enddatum + "&roomId=" + roomid + "&status=" + status+ "&extern=" + extern;
            }
                break;
        // Durchschnittliche Anzahl an Buchungen
        case "02":

            if(roomid==null){
                url= "http://localhost:4567/api/report/getavgcountallrooms?beginDate=" + startdatum + "&endDate=" + enddatum+ "&status=" + status+ "&extern=" + extern;
            }
            else {
                url = "http://localhost:4567/api/report/getavgcountsingleroom?beginDate=" + startdatum + "&endDate=" + enddatum + "&roomId=" + roomid+ "&status=" + status+ "&extern=" + extern;
            }
            break;

        // Auslastung
        case "03":
            if(roomid==null) {
                url = "http://localhost:4567/api/report/gethoursallrooms?beginDate=" + startdatum + "&endDate=" + enddatum+ "&status=" + status+ "&extern=" + extern;
            }
            else {
                url = "http://localhost:4567/api/report/gethourssingleroom?beginDate=" + startdatum + "&endDate=" + enddatum+ "&roomId=" + roomid+ "&status=" + status+ "&extern=" + extern;
            }
            break;

        // Durchschnittliche Anzahl an Buchungen
        case "04":
            if(roomid==null){
                url= "http://localhost:4567/api/report/getavgcountallrooms?beginDate=" + startdatum + "&endDate=" + enddatum+ "&status=" + status+ "&extern=" + extern;
            }
            else {
                url = "http://localhost:4567/api/report/getavgcountsingleroom?beginDate=" + startdatum + "&endDate=" + enddatum + "&roomId=" + roomid+ "&status=" + status+ "&extern=" + extern;
            }
            break;

        // Monatliche Stunden
        case "05":
            url= "http://localhost:4567/api/report/getmonthhourssingleroom?beginDate=" + startdatum + "&endDate=" + enddatum + "&roomId="+ roomid+ "&status=" + status+ "&extern=" + extern;

            break;

        // Auslastung in %
        case "06":
            url= "http://localhost:4567/api/report/getdailyutilizationsingleroom?beginDate=" + startdatum + "&endDate=" + enddatum + "&roomId="+ roomid+ "&status=" + status+ "&extern=" + extern;

            break;

        // Dashboard - TOP 3 meistgebuchten Räume
        case "98":
            url=  url= "http://localhost:4567/api/report/getcounttopthreerooms";

            break;


        case "99":
            url= "http://localhost:4567/api/room/getallrooms";
            break;

        default:
       alert("Fehler Switch");
    }


    xmlhttp.open("GET", url, false);
    xmlhttp.send();

    return JSON.parse(xmlhttp.responseText);

}

// Report ausführen
function report(){

    var e = document.getElementById("criteria");
    var criteria = e.options[e.selectedIndex].value;

    var e = document.getElementById("diagramm");
    var diagramm = e.options[e.selectedIndex].value;

    var e = document.getElementById("art");
    var art = e.options[e.selectedIndex].value;

    var e = document.getElementById("status");
    var status = e.options[e.selectedIndex].value;

    var e = document.getElementById("extern");
    var extern = e.options[e.selectedIndex].value;

    if(art==0){
        var roomid=0;

    }
    else{

        var e = document.getElementById("Rooms");
        var roomid = e.options[e.selectedIndex].value;
    }



    builddate();

    var returnvalue = api(criteria,globalstartdatum,globalenddatum,roomid,status,extern);

    createDiagramm(diagramm,"Diagramm2",returnvalue);

}

// Diagrammart wählen, Name vergeben, Diagramm mit Daten befüllen, Canvas in HTML erzeugen, Diagramm erzeugen, Diagramm darstellen
function createDiagramm(art,name,arr){
    var label = new Array();
    var data = new Array();


    for(i = 0; i < arr.length; i++) {
        label.push(arr[i].label);
        data.push(arr[i].value);
    }


    var diagrammname = name;
    outputdiagramm = '<canvas id="'+diagrammname.toString()+'" width="500" height="500"></canvas>';


    if(art==01){


        var diagrammdata = {
            labels : label,
            datasets :[
                {
                    fillColor :   "rgba(51, 51, 51, 0.20)",
                    strokeColor : "#B11A3B",
                    pointColor : "#fff",
                    pointStrokeColor : "#B11A3B",
                    data : data
                }
]


    };
        var chartoptions = {
            scaleGridLineColor : "rgba(102, 102, 102, 0.3)",
            // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,


        }


        document.getElementById("outputdiagramm").innerHTML = outputdiagramm;
        var diagramm1 = document.getElementById(diagrammname).getContext('2d');
        Chart.defaults.global.responsive = true;
        new Chart(diagramm1).Line(diagrammdata,chartoptions);
         Chart.defaults.global.responsive = true;

    }

    else if(art==02) {

        var diagrammdata = {
            labels: label,
            datasets: [
                {
                    label: "My First dataset",
                    fillColor : "#B11A3B",
                    strokeColor : "#B11A3B",
                    data: data
                },


            ]

        }

        var chartoptions = {
            scaleGridLineColor : "rgba(102, 102, 102, 0.5)",
            // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,


        }

        document.getElementById("outputdiagramm").innerHTML = outputdiagramm;
        var diagramm1 = document.getElementById(diagrammname).getContext('2d');
        Chart.defaults.global.responsive = true;
        new Chart(diagramm1).Bar(diagrammdata,chartoptions);
         Chart.defaults.global.responsive = true;


    }


    else if(art==03){


        var diagrammdata = [
            {
                value: arr[0].value,
                color:"#F7464A",
                highlight: "#FF5A5E",
                label: arr[0].label
            },
            {
                value: arr[1].value,
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: arr[1].label
            },
            {
                value: arr[2].value,
                color: "#FDB45C",
                highlight: "#FFC870",
                label: arr[2].label
            }
        ]


        var chartoptions = {
            segmentShowStroke : false,
            animateScale : true,
            //Number - Amount of animation steps
            animationSteps : 75,

            //String - Animation easing effect
            animationEasing : "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate : true,


        }

        document.getElementById("outputdiagramm").innerHTML = outputdiagramm;
        var diagramm1 = document.getElementById(diagrammname).getContext('2d');
        //Chart.defaults.global.responsive = true;
        var myPieChart = new Chart(diagramm1).Pie(diagrammdata,chartoptions);
        // Chart.defaults.global.responsive = true;

       // mychart.addData([40], "August");




    }


}

//----------------------------------------------- ALT

// Diagramm zeichnen - alte Funktion
function drawDiagrammBar(arr){

    var description = new Array();
    var myvalues = new Array();
    description.length = 0;
    myvalues.length = 0;

    // Diagramm zerstören um Darstellungsfehler und Überlagerungen zu vermeiden
    if( typeof myLineChart  !== 'undefined'){

        description.length = 0;
        myvalues.length = 0;

        //for(i = 0; i < arr.length; i++) {
        description.push(arr[0].cla_roomName)
        myvalues.push(arr[0].cla_countBooking);

        myLineChart.addData([myvalues], description);
        myLineChart.removeData();
        myLineChart.update();
    }
    else{

        //for(i = 0; i < arr.length; i++) {
        description.push(arr[0].cla_roomName)
        myvalues.push(arr[0].cla_countBooking);
        //  }

        var barData = {
            labels : description,
            datasets : [
                {
                    fillColor : "#48A497",
                    strokeColor : "#48A4D1",
                    data : myvalues
                }

            ]
        }

        var income = document.getElementById("income").getContext("2d");
        myLineChart = new Chart(income).Bar(barData);
        myLineChart.defaults.global.responsive = true;
    }
}

// Report Single Room
function reportsingleroom(){

    // Definiton Variabeln und Auslesen des Formulares / des Filters
    var e = document.getElementById("Rooms");
    var roomid = e.options[e.selectedIndex].value;

    var e = document.getElementById("criteria");
    var criteria = e.options[e.selectedIndex].value;

    var e = document.getElementById("year");
    var year = e.options[e.selectedIndex].value;

    var e = document.getElementById("status");
    var status = e.options[e.selectedIndex].value;






    // Auswertung Stundendurchschnitt
    if(criteria=="03"){

        if(quartal == "0") {
            startdatum = "01.01." + year;
            enddatum = "31.12." + year;
        }


        else if(quartal == "01") {
            startdatum = "01.01." + year;
            enddatum = "31.03." + year;
        }


        else if (quartal == "02") {
            startdatum="01.04."+year;
            enddatum="30.06."+year;
        }

        else if (quartal == "03") {
            startdatum="01.07."+year;
            enddatum="30.09."+year;
        }


        else if (quartal == "04") {
            startdatum="01.10."+year;
            enddatum="31.12."+year;
        }


        // startdatum = document.getElementById('startdatum1').value;
        // enddatum = document.getElementById('enddatum1').value;



        var returnvalue =  api("http://localhost:4567/api/report/getpall?beginDate=" + startdatum + "&endDate=" + enddatum+"&roomId="+ roomid);

    }

    // Auswertung Anzahl der Buchungen
    else {

        if (quartal == "0") {
            startdatum = "01.01." + year;
            enddatum = "31.12." + year;
        }


        else if (quartal == "01") {
            startdatum = "01.01." + year;
            enddatum = "31.03." + year;
        }


        else if (quartal == "02") {
            startdatum = "01.04." + year;
            enddatum = "30.06." + year;
        }

        else if (quartal == "03") {
            startdatum = "01.07." + year;
            enddatum = "30.09." + year;
        }


        else if (quartal == "04") {
            startdatum = "01.10." + year;
            enddatum = "31.12." + year;
        }


        // startdatum = document.getElementById('startdatum1').value;
        // enddatum = document.getElementById('enddatum1').value;


        // Wenn als Raum ALLE ausgewählt ist
        if (roomid == 0) {

            var returnvalue = api("http://localhost:4567/api/report/getcountallrooms?beginDate=" + startdatum + "&endDate=" + enddatum);

        }

        // Wenn ein bestimmter Raum ausgewählt ist
        else {
            var returnvalue = api("http://localhost:4567/api/report/getrsr?beginDate=" + startdatum + "&endDate=" + enddatum + "&roomId=" + roomid);

        }
    }

    // Diagramm zeichnen
    createDiagramm(2,"Diagramm2",returnvalue);

}