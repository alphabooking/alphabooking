/**
 * Created by Jan on 21.12.2015.
 */
'use strict';

(function () {

    var usermodule = angular.module('app.user');

    usermodule.controller('DashboardController', ['$scope', '$timeout', 'dashboardNewsService', 'bookingService', function ($scope, $timeout, dashboardNewsService, bookingService) {

        $scope.pendingRequests = bookingService.fetchPendingRequests(function success(response) {
            $scope.pendingRequests = bookingService.getPendingRequests();
        });

        dashboardNewsService.fetchNews(function success(response) {
            $scope.news = dashboardNewsService.getNews();
        });

        $scope.markNewsAsRead = function () {
            $scope.news = dashboardNewsService.setNewsAsRead();
        };

    }]);

    usermodule.controller('UserController', ['$scope', 'userService', '$uibModal', function ($scope, userService, $uibModal) {

        var animationsEnabled = true;

        $scope.user = userService.getUserData();

        $scope.user.roles = ['Besteller'];
        if (userService.isVerwalter())
            $scope.user.roles.push('Verwalter');

        $scope.openEditModal = function () {

            var modalInstance = $uibModal.open({
                animation: animationsEnabled,
                templateUrl: 'userEdit.html',
                controller: 'EditModalInstanceCtrl',
                resolve: {
                    user: $scope.user
                }
            });

            modalInstance.result.then(function () {
                //Update
            }, function () {
                //Dissmiss
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

    usermodule.controller('EditModalInstanceCtrl', ['$scope', '$uibModalInstance', 'user', 'appService', 'userService', function ($scope, $uibModalInstance, user, appService, userService) {

        $scope.user = user;

        $scope.newPasswordsMatch = function () {
            var pw1 = $scope.user.password_new;
            var pw2 = $scope.user.password_new2;

            return angular.equals(pw1, pw2);
        };

        $scope.submit = function () {

            var pw_old = $scope.user.password_old;
            var pw_new = $scope.user.password_new;

            if (angular.equals(pw_new, pw_old)) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Sie können nicht das selbe Passwort verwenden.',
                    timeout: 5000
                });
                return;
            }

            if (pw_new.length < 6 && pw_old.length < 6) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Das Passwort muss mindestens 6 Zeichen lang sein.',
                    timeout: 5000
                });
                return;
            }
            if (!$scope.newPasswordsMatch())
                return;

            userService.changePassword(pw_old, pw_new, function success() {
                appService.addAlert({
                    type: 'success',
                    msg: 'Passwort erfolgreich geändert',
                    timeout: 5000
                });
                $uibModalInstance.close();
            }, function failure(response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    timeout: 5000
                });
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})();