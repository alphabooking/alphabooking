/**
 * Created by Jan on 30.12.2015.
 */
'use strict';
(function () {
    var auth = angular.module('app.user');

    auth.service('dashboardNewsService', ['$http', '$state', function ($http, $state) {

        var newsData = [];

        this.fetchNews = function (successFn) {
            $http.get('/api/booking/news', {
                params: {nocache: new Date().getTime()}
            }).then(function success(response) {

                var news = [];
                response.data.forEach(function (item) {
                    var newsItem = {};

                    newsItem.id = item.id;
                    newsItem.status_new = item.status_new;
                    newsItem.status_old = item.status_old;
                    newsItem.booking = {
                        id: item.booking.cla_bookingId,
                        date: {
                            begin: item.booking.cla_beginDate,
                            end: item.booking.cla_endDate
                        },
                        createdAt: item.booking.cla_createdAt,
                        description: item.booking.cla_description,
                        room: {
                            id: item.booking.cla_room.cla_roomId,
                            description: item.booking.cla_room.cla_descrtiption,
                            name: item.booking.cla_room.cla_name
                        }
                    };

                    news.push(newsItem);
                });
                newsData = news;

                successFn(response);

            }, function failure(response) {
                if (response.status == 401) {
                    newsData = [];
                }
            });

            return newsData;
        };

        this.getNews = function () {
            return newsData;
        };

        this.setNewsAsRead = function () {
            $http.get('/api/booking/marknewsasseen', {
                params: {nocache: new Date().getTime()}
            });
            newsData = [];
            return newsData;
        };

    }]);

    auth.service('userService', ['$http', '$state', '$rootScope', function ($http, $state, $rootScope) {
        var userRoles = {
            besteller: false,
            verwalter: false
        };

        var userData = {
            userID: -1,
            surname: '',
            firstname: '',
            gender: '',
            email: '',
            phone: '',
            department: {
                departmentID: -1,
                departmentName: ''
            }
        };

        $rootScope.$on('LOGGED_OUT', function () {
            userRoles.besteller = false;
            userRoles.verwalter = false;
        });

        this.fetchUserData = function (successFn) {
            $http.get('/api/user/info', {
                    params: {nocache: new Date().getTime()}
                })
                .then(function success(response) {

                    userData = {
                        firstname: response.data.cla_firstname,
                        surname: response.data.cla_surname,
                        gender: response.data.cla_gender,
                        email: response.data.cla_mail,
                        phone: response.data.cla_phone,
                        department: {
                            departmentID: response.data.cla_department.cla_departmentId,
                            departmentName: response.data.cla_department.cla_name
                        }
                    };

                    if (typeof successFn !== 'undefined')
                        successFn(response);

                }, function failure(response) {
                    if (response.status == 401) {
                        $state.go('login');
                    }
                });
            return userData;
        };

        this.getUserData = function () {
            return userData;
        };

        this.fetchUserRoles = function (successFn) {
            $http.get('/api/user/getroles', {
                    params: {nocache: new Date().getTime()}
                })
                .then(function success(response) {

                    response.data.forEach(function (role) {
                        if (role.cla_roleId === 1)
                            userRoles.besteller = true;

                        if (role.cla_roleId === 2)
                            userRoles.verwalter = true;
                    });
                    successFn(response);

                }, function failure(response) {
                    if (response.status == 401) {
                        $state.go('login');
                    }
                });
            return userRoles;
        };

        this.isVerwalter = function () {
            return userRoles.verwalter;
        };

        this.isBesteller = function () {
            return userRoles.besteller;
        };

        this.userRoles = function () {
            return userRoles;
        };

        this.changePassword = function (oldPassword, newPassword, successFn, failureFn) {
            $http.post('/api/user/changepassword', {
                data: {
                    "password_old": sha256_digest(oldPassword),
                    "password_new": sha256_digest(newPassword)
                }

            }).then(successFn, failureFn);

        }
    }]);

})();