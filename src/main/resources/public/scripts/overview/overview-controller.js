/**
 * Created by Jan on 14.01.2016.
 */
(function () {
    'use strict';

    var usermodule = angular.module('app.overview');

    usermodule.controller('OverviewController', ['$scope', '$filter', '$uibModal', 'dashboardNewsService', 'bookingService', function ($scope, $filter, $uibModal, dashboardNewsService, bookingService) {

        $scope.bookings = bookingService.fetchValidBookingsAndRequests(function success(response) {
            $scope.bookings = bookingService.getValidBookingsAndRequests();
        });

        $scope.filterDate = today();
        $scope.format = 'dd.MM.yyyy';
        $scope.minDate = null;
        $scope.opened = false;
        $scope.buttonBar = false;

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };

        $scope.open = function () {
            if ($scope.filter) {
                $scope.opened = true;
            }
        };

        $scope.$watch('filter', function () {
            if (!$scope.filter)
                $scope.bookings = bookingService.getValidBookingsAndRequests();
            if ($scope.filter) {
                $scope.filterDate = today();
            }
        });

        $scope.filterBookings = function () {
            if ($scope.filter) {
                $scope.bookings = $filter('atDate')(bookingService.getValidBookingsAndRequests(), $scope.filterDate);
            } else {
                $scope.bookings = bookingService.getValidBookingsAndRequests();
            }
        };

        $scope.isDisabled = function () {
            return !$scope.filter;
        };

        $scope.openEditModal = function (bookingitem) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'seeInfo.html',
                controller: 'InfoModalInstanceCtrl',
                resolve: {
                    booking: bookingitem
                }
            });

            modalInstance.result.then(function () {
                //Update
                bookingService.fetchValidBookingsAndRequests(function success(response) {
                    $scope.filterBookings();
                });
            }, function () {
                //Dissmiss
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openDeleteModal = function (bookingitem) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ownBookingDelete.html',
                controller: 'DeleteOwnBookingModalInstanceCtrl',
                resolve: {
                    booking: bookingitem
                }
            });

            modalInstance.result.then(function () {
                //Update
            }, function () {
                //Dissmiss
            });
        };

        function today() {
            var _today = new Date();
            _today.setHours(0);
            _today.setMinutes(0);
            _today.setSeconds(0);
            _today.setMilliseconds(0);
            return _today;
        }

    }]);

    usermodule.controller('InfoModalInstanceCtrl', ['$scope', '$uibModalInstance', 'booking', function ($scope, $uibModalInstance, booking) {

        $scope.booking = booking;

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

    usermodule.controller('DeleteOwnBookingModalInstanceCtrl', ['$scope', '$uibModalInstance', 'bookingService', 'appService', 'booking', function ($scope, $uibModalInstance, bookingService, appService, booking) {
        $scope.function = 'löschen';
        $scope.booking = angular.copy(booking);

        $scope.submit = function () {

            if ($scope.accept) {

                bookingService.cancelOwnBooking($scope.booking.bookingID, function success(response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Die Buchung \"' + $scope.booking.description + '\" wurde erfolgreich gelöscht.',
                        timeout: 5000
                    });
                    $uibModalInstance.close();
                }, function failure(response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });
                });

            } else {
                appService.addAlert({
                    type: 'warning',
                    msg: 'Sie müssen bestätigen dass der Raum gelöscht werden soll.',
                    timeout: 4000
                });
            }


        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);


})();