/**
 * Created by Jan on 01.12.2015.
 */
'use strict';

angular.module('app').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        var $log = angular.injector(['ng']).get('$log');

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/user/dashboard.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: false
                }
            })
            .state('notfound', {
                url: '/pagenotfound',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/notfound/notfound.html'
                    }
                },
                auth: {
                    needLogin: false,
                    needAdmin: false
                }
            })
            .state('login', {
                url: '/login',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/auth/login.html'
                    }
                },
                auth: {
                    needLogin: false,
                    needAdmin: false
                }
            })
            .state('passwordreset', {
                url: '/passwordreset',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/auth/passwordreset.html'
                    }
                },
                auth: {
                    needLogin: false,
                    needAdmin: false
                }
            })
            .state('reservation', {
                url: '/reservation',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/reservation/reservation.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: false
                }
            })
            .state('user', {
                url: '/user',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/overview/overview.html'
                    }
                },
                redirectTo: 'user.profil',
                auth: {
                    needLogin: true,
                    needAdmin: false
                }
            })
            .state('user.bookings', {
                url: '/mybookings',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/overview/overview.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: false
                }
            })
            .state('user.profil', {
                url: '/profil',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/user/profil.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: false
                }
            })
            .state('admin', {
                url: '/admin',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/admin/admin.html'
                    },
                    'admin@admin.reservationmanager': {
                        templateUrl: '/public/scripts/admin/reservation_manager.html'
                    },
                    'admin@admin.bookingmanager': {
                        templateUrl: '/public/scripts/admin/booking_manager.html'
                    },
                    'admin@admin.roommanager': {
                        templateUrl: '/public/scripts/admin/room_manager.html'
                    },
                    'admin@admin.reports': {
                        templateUrl: '/public/scripts/reports/reports.html'
                    },
                    'admin@admin.equipmentmanager': {
                        templateUrl: '/public/scripts/equipmentmanager/equipment_manager.html'
                    }

                },
                redirectTo: 'admin.reservationmanager',
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            })
            .state('admin.backup', {
                url: '/',
                redirectTo: 'admin.reservationmanager',
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            })
            .state('admin.reservationmanager', {
                url: '/reservationmanager',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/admin/admin.html'
                    },
                    'admin@': {
                        templateUrl: '/public/scripts/admin/reservation_manager.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            })
            .state('admin.bookingmanager', {
                url: '/bookingmanager',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/admin/admin.html'
                    },
                    'admin@': {
                        templateUrl: '/public/scripts/admin/booking_manager.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            })
            .state('admin.roommanager', {
                url: '/roommanagement',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/admin/admin.html'
                    },
                    'admin@': {
                        templateUrl: '/public/scripts/admin/room_manager.html'
                    }
                },
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            })
            .state('admin.reports', {
                url: '/reports',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/admin/admin.html'
                    },
                    'admin@': {
                        templateUrl: '/public/scripts/reports/reports.html'
                    }
                }
                ,
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            })
            .state('admin.equipmentmanager', {
                url: '/equipmentmanager',
                views: {
                    'main@': {
                        templateUrl: '/public/scripts/admin/admin.html'
                    },
                    'admin@': {
                        templateUrl: '/public/scripts/equipmentmanager/equipment_manager.html'
                    }
                }
                ,
                auth: {
                    needLogin: true,
                    needAdmin: true
                }
            });
    }]
);