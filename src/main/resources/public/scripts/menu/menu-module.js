/**
 * Created by Jan on 01.12.2015.
 */
var menu = angular.module('app.menu', []);

menu.controller('MenuController', ['$scope', 'authenticationService', 'userService', '$rootScope', function ($scope, authenticationService, userService, $rootScope) {

    $scope.tabs = [];//validMenuTabs();

    $scope.isLoggedIn = function () {
        return authenticationService.isLoggedIn();
    };

    $rootScope.$on('$stateChangeSuccess', function (evt, to, params) {
        initMenu();
    });

    $rootScope.$on('USERROLES_FETCHED', function (evt, to, params) {
        initMenu();
    });


    function initMenu() {

        var isVerwalter = userService.isVerwalter();

        var tabs = [];

        var loggedIn = authenticationService.isLoggedIn();

        if (loggedIn) {
            tabs = setupLoggedInMenu();
        } else {
            tabs = setupGuestMenu()
        }


        function setupGuestMenu() {
            var validTabs = validMenuTabs();
            var ergTabs = [];

            validTabs.forEach(function (tab) {
                if (tab.visibility.guest == true)
                    ergTabs.push(tab);
            });

            return ergTabs;
        }

        function setupLoggedInMenu() {
            var validTabs = validMenuTabs();
            var ergTabs = [];

            validTabs.forEach(function (tab) {
                if (tab.visibility.guest == false) {
                    if (tab.visibility.admin) {
                        if (isVerwalter)
                            ergTabs.push(tab);
                    } else {
                        ergTabs.push(tab);
                    }
                }

            });

            return ergTabs;
        }

        $scope.tabs = tabs;

    }

    function validMenuTabs() {
        return [
            {
                title: "Home",
                url: "home",
                visibility: {
                    guest: false
                }
            },
            {
                title: "Login",
                url: "login",
                visibility: {
                    guest: true
                }
            },
            {
                title: "PW-Reset",
                url: "passwordreset",
                visibility: {
                    guest: true
                }
            },
            {
                title: "Raum finden",
                url: "reservation",
                visibility: {
                    guest: false
                }
            },
            {
                title: "Buchungsübersicht",
                url: "user.bookings",
                visibility: {
                    guest: false
                }
            },
            {
                title: "Benutzerprofil",
                url: "user.profil",
                visibility: {
                    guest: false
                }
            },
            {
                title: "Administration",
                url: "admin",
                visibility: {
                    guest: false,
                    admin: true
                }
            }
        ];
    }

    $scope.checkActive = function (url) {
        if (url == "#" + $scope.newLocation) {
            return "active";
        } else {
            return "";
        }
    };

    $scope.logout = function () {
        authenticationService.logout();
    };
}])
;