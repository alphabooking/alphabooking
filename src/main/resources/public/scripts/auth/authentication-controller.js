/**
 * Created by Jan on 08.12.2015.
 */
'use strict';

(function () {
    var auth = angular.module('app.auth');

    auth.controller('AuthenticationController', ['$scope', function ($scope) {

    }]);

    auth.controller('LoginController', ['$rootScope', '$scope', 'authenticationService', '$http', '$state', 'userService', function ($rootScope, $scope, authenticationService, $http, $state, userService) {

        $scope.email = "";
        $scope.password = "";

        $scope.loginLoading = false;

        $scope.login = function () {
            $scope.loginLoading = true;
            authenticationService.login($scope.email, $scope.password, successfulLogin, failureLogin, $scope);
        };

        function successfulLogin(data, status, headers, config) {
            $rootScope.$emit('LOGGED_IN');
            setErrorMessage();
            $state.go('home');
        }


        function failureLogin() {
            setErrorMessage("Login fehlgeschlagen.");
        }

        function setErrorMessage(message) {
            if (!message) {
                $scope.errorMessage = message;
                angular.element('.errorMessage').addClass('hidden');
            } else {
                $scope.errorMessage = message;
                angular.element('.errorMessage').removeClass('hidden');
            }
        }


    }]);

    auth.controller('PasswordresetController', ['$scope', function ($scope) {

        $scope.email = "";
        $scope.change = function () {
            $scope.$parent.$parent.username = $scope.email;
        };

    }]);


    auth.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });


})();