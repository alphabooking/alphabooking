/**
 * Created by Jan on 15.12.2015.
 */
'use strict';
(function () {
    var auth = angular.module('app.auth');

    auth.provider('authentication', ['lsAuthKey', '$httpProvider', function (lsAuthKey, $httpProvider) {
        var loggedIn = false;

        function _removeToken() {
            console.debug('removed');
            localStorage.removeItem(lsAuthKey);
            $httpProvider.defaults.headers.common["X-AUTH-TOKEN"] = undefined;
            loggedIn = false;
        }

        this.$get = function () {


            function _setToken(token) {
                localStorage.setItem(lsAuthKey, token);
            }

            function _getAuthToken() {
                return localStorage.getItem(lsAuthKey);
            }


            return {
                login: function () {
                    loggedIn = true;
                },
                logout: function () {
                    loggedIn = false;
                    _removeToken();
                },
                removeAuthToken: function () {
                    _removeToken();
                },
                getAuthToken: function () {
                    return _getAuthToken();
                },
                setAuthToken: function (token) {
                    _setToken(token);
                },
                isLoggedIn: function () {
                    var authToken = _getAuthToken();
                    if (authToken && authToken != null)
                        return loggedIn;
                    return false;
                }
            };
        };
    }]);

    auth.service('authenticationService', ['lsAuthKey', '$http', '$state', '$rootScope', '$log', 'authentication', function (lsAuthKey, $http, $state, $rootScope, $log, authentication) {


        this.getAuthToken = function () {
            return authentication.getAuthToken();
        };

        this.setAuthToken = function (token) {
            authentication.setAuthToken(token);
        };

        this.removeAuthToken = function () {
            authentication.removeAuthToken();
        };

        this.login = function (email, password, successFn, failFn, scope) {

            var authToken = this.getAuthToken();
            if (!(!angular.isDefined(authToken) || authToken == null)) {
                validateAuthToken(successFn);
            }


            if (!angular.isDefined(authToken) || authToken == null) {
                $http.post('/api/user/login', {
                    responseType: 'json',
                    data: {
                        email: email,
                        password: sha256_digest(password)
                    }
                }).success(function (data, status, headers, config) {
                    _setLoggedIn(true);
                    scope.loginLoading = false;
                    authentication.setAuthToken(headers('X-Auth-Token'));
                    successFn(data, status, headers, config);
                }).error(function (data, status, headers, config) {
                    _setLoggedIn(false);
                    scope.loginLoading = false;
                    failFn(data, status, headers, config);
                });
            }
        };

        this.validateLoginOnStartUp = function (successFn) {
            var authToken = this.getAuthToken();
            if (!(!angular.isDefined(authToken) || authToken == null)) {
                validateAuthToken(successFn);
            }
        };

        this.logout = function () {
            $log.debug('Logout');

            $http.get('/api/user/logout', {params: {nocache: new Date().getTime()}});
            this.removeAuthToken(lsAuthKey);
            _setLoggedIn(false);
            $http.defaults.headers.common["X-AUTH-TOKEN"] = undefined;
            $rootScope.$emit('LOGGED_OUT');
            $state.go('login');
        };


        this.isLoggedIn = function () {
            return authentication.isLoggedIn();
        };

        function validateAuthToken(successFn) {
            $http({
                method: 'GET',
                url: '/api/user/isloggedin',
                "cache": false,
                "responseType": "json"

            }).success(authCallSuccessCallback).error(authCallFailCallback);

            function authCallSuccessCallback(data, status, headers, config) {
                _setLoggedIn(true);
                successFn(data, status, headers, config);
            }

            function authCallFailCallback() {
                _setLoggedIn(false);
                authentication.removeAuthToken();
            }
        }

        function _setLoggedIn(value) {
            if (value) {
                authentication.login();
            } else {
                authentication.logout();
            }
        }

    }]);


})();