/**
 * Created by Jan on 30.11.2015.
 */
'use strict';
(function () {
    var auth = angular.module('app.auth', []);
    auth.constant('lsAuthKey', 'ab-auth');


    auth.run(function () {
        auth.value('moduleName', 'Authentication');
    });
})();
