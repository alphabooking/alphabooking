/**
 * Created by Jan on 25.01.2016.
 */
(function () {
    'use strict';

    var equipModule = angular.module('app.admin.equipmentmanager');

    equipModule.service('equipmentService', ['$http', 'equipmentFactory', function ($http, equipmentFactory) {

        var activeFixEquipment = [];
        var activeVarEquipment = [];

        this.fetchActiveEquipments = function (fixEquipSuccessFn, varEquipSuccessFn) {
            $http.get('/api/equipment/listroomequipment', {
                params: {
                    nocache: new Date().getTime()
                }
            }).then(function success(response) {
                var activeFixEquip = equipmentFactory.getFixEquipmentsFromResponse(response.data);
                activeFixEquipment = activeFixEquip;
                fixEquipSuccessFn(activeFixEquip);
            }, function failure(response) {

            });


            $http.get('/api/equipment/listadditionalequipment', {
                params: {
                    nocache: new Date().getTime()
                }
            }).then(function success(response) {
                var activeVarEquip = equipmentFactory.getVarEquipmentsFromResponse(response.data);
                activeVarEquipment = activeVarEquip;
                varEquipSuccessFn(activeVarEquip)
            }, function failure(response) {

            });
        };

        this.getFixEquipment = function () {
            return activeFixEquipment;
        };

        this.getVarEquipment = function () {
            return activeVarEquipment;
        };

        this.updateFixEquipment = function (fixEquipID, description, successFn, failureFn) {
            $http.post('/api/equipment/editroomequipment/' + fixEquipID, {
                data: {
                    description: description
                }
            }).then(successFn, failureFn);
        };

        this.updateVarEquipment = function (fixEquipID, description, stock, successFn, failureFn) {
            $http.post('/api/equipment/editadditionalequipment/' + fixEquipID, {
                data: {
                    description: description,
                    stock: stock
                }
            }).then(successFn, failureFn);
        };

        this.deleteFixEquipment = function (fixEquipID, successFn, failureFn) {
            $http.post('/api/equipment/deactivateroomequipment/' + fixEquipID, {}).then(successFn, failureFn);
        };

        this.deleteVarEquipment = function (fixEquipID, successFn, failureFn) {
            $http.post('/api/equipment/deactivateadditionalequipment/' + fixEquipID, {}).then(successFn, failureFn);
        };

        this.addFixEquipment = function (description, successFn, failureFn) {
            $http.post('/api/equipment/createroomequipment', {
                data: {
                    description: description
                }
            }).then(successFn, failureFn);
        };

        this.addVarEquipment = function (description, stock, successFn, failureFn) {
            $http.post('/api/equipment/createadditionalequipment', {
                data: {
                    description: description,
                    stock: stock
                }
            }).then(successFn, failureFn);
        }

    }]);

    equipModule.factory('equipmentFactory', [function () {
        function _fixEquipmentsFromResponse(data) {
            var items = [];
            data.forEach(function (item) {
                items.push({
                    id: item.cla_idFixEquipment,
                    description: item.cla_description
                });
            });
            return items;
        }

        function _varEquipmentsFromResponse(data) {
            var items = [];
            data.forEach(function (item) {
                items.push({
                    id: item.cla_additionalEquipmentId,
                    description: item.cla_description,
                    stock: item.cla_stock
                });
            });
            return items;
        }


        return {
            getFixEquipmentsFromResponse: _fixEquipmentsFromResponse,
            getVarEquipmentsFromResponse: _varEquipmentsFromResponse
        }

    }])


})();
