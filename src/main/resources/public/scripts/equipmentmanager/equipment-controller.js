/**
 * Created by Jan on 25.01.2016.
 */
(function () {
    'use strict';

    var equipModule = angular.module('app.admin.equipmentmanager');

    equipModule.controller('AdminEquipmentManagerController', ['$scope', '$uibModal', 'equipmentService', 'appService', function ($scope, $uibModal, equipmentService, appService) {

        $scope.activeFixEquipments = equipmentService.getFixEquipment();
        $scope.activeVarEquipments = equipmentService.getVarEquipment();

        equipmentService.fetchActiveEquipments(function (fix) {
            $scope.activeFixEquipments = fix;
        }, function (variable) {
            $scope.activeVarEquipments = variable;
        });

        $scope.addFixEquipment = function () {

            var input = $scope.newFixEquipment;

            if (input.length < 3) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Die Beschreibung für das neue Equipment ist zu kurz',
                    timeout: 5000
                });
                return;
            }

            equipmentService.addFixEquipment(input, function (response) {
                appService.addAlert({
                    type: 'success',
                    msg: 'Raumausstattung erfolgreich hinzugefügt',
                    timeout: 5000
                });
                $scope.newFixEquipment = "";
                updateFixEquipmentList();
            }, function (response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    timeout: 5000
                });
            });

        };

        $scope.addVarEquipment = function () {
            var input = $scope.newVarEquipment;
            var stock = $scope.newVarEquipmentStock;

            if (input.length < 3) {
                appService.addAlert({
                    type: 'danger',
                    msg: 'Die Beschreibung für das neue Equipment ist zu kurz',
                    timeout: 5000
                });
                return;
            }

            equipmentService.addVarEquipment(input, stock, function (response) {
                appService.addAlert({
                    type: 'success',
                    msg: 'Raumausstattung erfolgreich hinzugefügt',
                    timeout: 5000
                });
                $scope.newVarEquipment = "";
                $scope.newVarEquipmentStock = 1;
                updateVarEquipmentList();
            }, function (response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    timeout: 5000
                });
            });

        };


        $scope.openEditVarEquipmentModal = function (variableEquip) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ChangeVarEquipment.html',
                controller: 'EditVarEquipModalCtrl',
                resolve: {
                    equipment: variableEquip
                }
            });

            modalInstance.result.then(function () {
                updateVarEquipmentList();
            }, function () {
                //Dissmiss
            });
        };


        $scope.openEditFixEquipmentModal = function (fixEquip) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ChangeFixEquipment.html',
                controller: 'EditFixEquipModalCtrl',
                resolve: {
                    equipment: fixEquip
                }
            });

            modalInstance.result.then(function () {
                updateFixEquipmentList();
            }, function () {
                //Dissmiss
            });
        };

        $scope.openDeleteFixEquipModal = function (equipment) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'deleteEquipment.html',
                controller: 'DeleteFixEquipModalInstanceCtrl',
                resolve: {
                    equipment: equipment
                }
            });

            modalInstance.result.then(function () {
                updateFixEquipmentList();
                updateVarEquipmentList();
            }, function () {
                //Dissmiss
            });
        };

        $scope.openDeleteVarEquipModal = function (equipment) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'deleteEquipment.html',
                controller: 'DeleteVarEquipModalInstanceCtrl',
                resolve: {
                    equipment: equipment
                }
            });

            modalInstance.result.then(function () {
                updateFixEquipmentList();
                updateVarEquipmentList();
            }, function () {
                //Dissmiss
            });
        };

        function updateFixEquipmentList() {
            equipmentService.fetchActiveEquipments(function (fix) {
                $scope.activeFixEquipments = fix;
            }, function (variable) {
            });
        }

        function updateVarEquipmentList() {
            equipmentService.fetchActiveEquipments(function (fix) {
            }, function (variable) {
                $scope.activeVarEquipments = variable;
            });
        }

    }]);

    equipModule.controller('EditFixEquipModalCtrl', ['$scope', '$uibModalInstance', 'equipment', 'equipmentService', 'appService', function ($scope, $uibModalInstance, equipment, equipmentService, appService) {

        $scope.equipment = angular.copy(equipment);

        $scope.submit = function () {
            equipmentService.updateFixEquipment($scope.equipment.id, $scope.equipment.description, function (response) {
                appService.addAlert({
                    type: 'success',
                    msg: 'Raumausstattung erfolgreich aktualisiert.',
                    timeout: 5000
                });
                $uibModalInstance.close();
            }, function (response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    timeout: 5000
                });
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('');
        }

    }]);

    equipModule.controller('EditVarEquipModalCtrl', ['$scope', '$uibModalInstance', 'equipment', 'equipmentService', 'appService', function ($scope, $uibModalInstance, equipment, equipmentService, appService) {

        $scope.equipment = angular.copy(equipment);

        $scope.submit = function () {
            equipmentService.updateVarEquipment($scope.equipment.id, $scope.equipment.description, $scope.equipment.stock, function (response) {
                appService.addAlert({
                    type: 'success',
                    msg: 'Zusatzausstattung erfolgreich aktualisiert.',
                    timeout: 5000
                });
                $uibModalInstance.close();
            }, function (response) {
                appService.addAlert({
                    type: 'danger',
                    msg: response.data.message,
                    timeout: 5000
                });
            });

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('');
        }

    }]);

    equipModule.controller('DeleteFixEquipModalInstanceCtrl', ['$scope', '$uibModalInstance', 'equipment', 'equipmentService', 'appService', function ($scope, $uibModalInstance, equipment, equipmentService, appService) {

        $scope.equipment = angular.copy(equipment);

        $scope.submit = function () {
            if ($scope.accept) {
                equipmentService.deleteFixEquipment($scope.equipment.id, function (response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Raumausstattung erfolgreich gelöscht.',
                        timeout: 5000
                    });
                    $uibModalInstance.close();
                }, function (response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('');
        }

    }]);

    equipModule.controller('DeleteVarEquipModalInstanceCtrl', ['$scope', '$uibModalInstance', 'equipment', 'equipmentService', function ($scope, $uibModalInstance, equipment, equipmentService) {

        $scope.equipment = angular.copy(equipment);

        $scope.submit = function () {
            if ($scope.accept) {
                equipmentService.deleteVarEquipment($scope.equipment.id, $scope.equipment.description, function (response) {
                    appService.addAlert({
                        type: 'success',
                        msg: 'Zusatzausstattung erfolgreich gelöscht.',
                        timeout: 5000
                    });
                    $uibModalInstance.close();
                }, function (response) {
                    appService.addAlert({
                        type: 'danger',
                        msg: response.data.message,
                        timeout: 5000
                    });
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('');
        }

    }]);


})();
